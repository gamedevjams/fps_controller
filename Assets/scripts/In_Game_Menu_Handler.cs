﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;
using UnityEngine.InputSystem;

namespace tg
{
	public class In_Game_Menu_Handler : MonoBehaviour
	{
		public GameObject menu;
		#region private
		#endregion

		#region Unity
		private void Awake()
		{
			menu.SetActive( false );
		}
		private void Start()
		{
		}
		private void Update()
		{
			bool got_esc =// Keyboard.current.escapeKey.wasPressedThisFrame;
			   Game_Input.instance.merged_keyboards_per_frame.key_front_down( Game_Input.Index.escape );
			//foreach ( Game_Input.Keyboard_Status ks in Game_Input.instance.keyboards_per_frame.Values )
			//{
			//	got_esc = ks.keys_front_down[(int)Game_Input.Index.escape] == 1;
			//	if ( got_esc )
			//	{
			//		break;
			//	}
			//}
			if ( got_esc )
			{
				if ( menu.activeSelf )
				{
					menu.SetActive( false );
				} else
				{
					menu.SetActive( true );
				}
			}
		}
		#endregion
	}
}
