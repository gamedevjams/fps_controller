﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;
using System;

namespace tg
{
	public class Mapping_GUI_Entry : MonoBehaviour
	{
		[Foldout("UI",true)]
		public TMP_Text label_text;
		public TMP_Text button_text;
		[Foldout("UI",false)]
		public Button   button;

		public Mapping_GUI mapping_gui;

		// TODO(theGiallo): this should be readonly in inspector
		public FP_Controller.Action_Index action_index;

		public
		void
		set_action_index( FP_Controller.Action_Index action_index )
		{
			if ( ever_set && this.action_index == action_index )
			{
				return;
			}
			ever_set = true;

			this.action_index = action_index;
			refresh();
		}
		public
		void
		refresh()
		{
			label_text .text = mapping_gui.action_text( action_index );
			button_text.text = mapping_gui.mapping_text( action_index );
		}

		#region private
		private bool ever_set = false;
		private
		void
		click_handler()
		{
			mapping_gui.request_remap( action_index );
		}
		#endregion

		#region Unity
		private void Awake()
		{
		}
		private void Start()
		{
			button.onClick.AddListener( click_handler );
		}
		private void Update()
		{
		}
		#endregion
	}
}
