﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;

namespace tg
{
	[RequireComponent(typeof(Plot_CRT_Ranges))]
	public class Plot_Mouse_Events_Count : MonoBehaviour
	{
		public Game_Input.Device_Path device_path;

		#region private
		private Plot_CRT_Ranges plot_crt;
		private const float max_count = 64;
		private
		float
		count_to_01( int count )
		{
			float ret = count / max_count;
			return ret;
		}
		#endregion

		#region Unity
		private void Awake()
		{
			plot_crt = GetComponent<Plot_CRT_Ranges>();
			plot_crt.ranges_max[0] = count_to_01(   0 );
			plot_crt.ranges_max[1] = count_to_01(   2 );
			plot_crt.ranges_max[2] = count_to_01(   4 );
			plot_crt.ranges_max[3] = count_to_01(   8 );
			plot_crt.ranges_max[4] = count_to_01(  16 );
			plot_crt.ranges_max[5] = count_to_01(  32 );
			plot_crt.ranges_max[6] = count_to_01( 128 );
			plot_crt.fg_colors[0] = new Color(0.2588235f,0.5623817f,0.7058824f,0.8f);
			plot_crt.fg_colors[1] = new Color(0.2862745f,0.7058824f,0.2588235f,0.8f);
			plot_crt.fg_colors[2] = new Color(1f,0.9294118f,0.3098039f,0.8f);
			plot_crt.fg_colors[3] = new Color(1f,0.4666667f,0f,0.8f);
			plot_crt.fg_colors[4] = new Color(1f,0f,1f,1f);
			plot_crt.fg_colors[5] = new Color(1f,0f,0f,1f);
		}
		private void Start()
		{
		}
		private void Update()
		{
			if ( Game_Input.instance.mouses_per_frame.TryGetValue( device_path, out Game_Input.Mouse_Status m ) )
			{
				int c = m.step_events_counter;
				plot_crt.plot01( count_to_01( c ) );
			}
		}
		#endregion
	}
}
