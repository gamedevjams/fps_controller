﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.HID;
using UnityEngine.InputSystem.Controls;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;

namespace tg
{
	public class MultiMouseTest : MonoBehaviour
	{
		public
		enum
		Update_Method
		{
			UNITY,
			RAW
		}
		public Transform     pointer_prefab;
		public Transform     father;
		[Min(0.00001f)]
		public float         scale = 0.001f;
		public Update_Method update_method;
		public Text          update_method_text;
		public Color         pressed_emittance;
		public Color         unpressed_emittance;

		#region private
		private Game_Input.Device_Path mouse_path;
		private bool                   got_mouse_path;
		private Game_Input.Device_Path keyboard_path;
		private bool                   got_keyboard_path;

		private List<MeshRenderer[]>          buttons      = new List<MeshRenderer[]>();
		private List<MaterialPropertyBlock[]> mpbs         = new List<MaterialPropertyBlock[]>();
		private List<TextMesh>                texts        = new List<TextMesh>();
		private List<Transform>               pointers     = new List<Transform>();
		private List<Game_Input.Device_Path>  mouses_paths = new List<Game_Input.Device_Path>();

		private int shader_property_emittance_name = Shader.PropertyToID( "_EmissionColor" );

		private Transform add_pointer( Game_Input.Device_Path mouse_path )
		{
			Transform ret = Instantiate( pointer_prefab, father );
			TextMesh text = ret.gameObject.GetComponentInChildren<TextMesh>();
			Transform buttons_container = ret.Find( "buttons" );
			MeshRenderer[]          mrs   = new MeshRenderer         [buttons_container.childCount];
			MaterialPropertyBlock[] bmpbs = new MaterialPropertyBlock[buttons_container.childCount];
			for ( int c = 0, c_count = buttons_container.childCount; c != c_count; ++c )
			{
				MeshRenderer mr = buttons_container.GetChild( c ).GetComponent<MeshRenderer>();
				mrs[c] = mr;
				bmpbs[c] = new MaterialPropertyBlock();
			}
			mpbs.Add( bmpbs );
			buttons.Add( mrs );

			pointers    .Add( ret );
			mouses_paths.Add( mouse_path );
			texts       .Add( text );

			string str = mouse_path.ToString();
			log( $"text = '{str}' length = {str.Length} mouse_path.chars_count = {mouse_path.chars_length}" );
			text.text = str;

			{
				MeshRenderer mr = ret.GetComponent<MeshRenderer>();
				Color c = mr.material.GetColor( "_Color" );
				Color.RGBToHSV( c, out float h, out float s, out float v );
				h = Random.value;
				c = Color.HSVToRGB( h, s, v );
				MaterialPropertyBlock mpb = new MaterialPropertyBlock();
				mpb.SetColor( "_Color", c );
				mr.SetPropertyBlock( mpb );
			}

			return ret;
		}
		private void add_existing_mouses()
		{
			foreach ( Game_Input.Device_Path mouse_path in Game_Input.instance.mouses.Keys )
			{
				add_pointer( mouse_path );
			}
		}

		private
		void
		new_mouse_h( object caller, Game_Input.Device_Path path )
		{
			add_pointer( path );
		}
		#endregion

		#region Unity
		private void Awake()
		{
			add_existing_mouses();
		}
		private void Start()
		{
			Game_Input.instance.on_new_mouse += new_mouse_h;

			if ( update_method_text != null )
			{
				update_method_text.text = update_method.ToString();
			}
		}
		private void FixedUpdate()
		{
			if ( update_method == Update_Method.RAW )
			{
				for ( int i = 0; i != pointers.Count; ++i )
				{
					Game_Input.Device_Path path = mouses_paths[i];
					Transform t = pointers[i];
					Game_Input.Mouse_Status m = Game_Input.instance.mouses[path];
					Vector3 p = t.localPosition;
					p.x = m.pos_x * scale;
					p.y = m.pos_y * scale;
					t.localPosition = p;
				}
			} else
			if ( update_method == Update_Method.UNITY )
			{
				if ( pointers.Count > 0 )
				{
					Transform t = pointers[0];
					Vector3 p = t.localPosition;
					p.x = Mouse.current.position.x.ReadValue() * scale;
					p.y = Mouse.current.position.y.ReadValue() * scale;
					t.localPosition = p;
				}
			}
		}
		private void Update()
		{
			for ( int i = 0; i != texts.Count; ++i )
			{
				Game_Input.Device_Path path = mouses_paths[i];
				Game_Input.Mouse_Status m = Game_Input.instance.mouses[path];
				float maxHz  = m.max_step_events_counter / Time.fixedDeltaTime;
				float lastHx = m.step_events_counter     / Time.fixedDeltaTime;
				texts[i].text = $"{path}\nmax: {m.max_step_events_counter.ToString("0000")}/fixu {maxHz.ToString("0000.00")} last: {m.step_events_counter.ToString("0000")} {lastHx.ToString("0000.00")}Hz";
			}

			for ( int i = 0; i != mouses_paths.Count; ++i )
			{
				Game_Input.Mouse_Status m = Game_Input.instance.mouses_per_frame[mouses_paths[i]];
				texts[i].text += $"\nbuttons status down {m.buttons_status_down}";
				texts[i].text += $"\nwheel h {m.wheel_h.ToString("d6").PadLeft(7,' ')} wheel v {m.wheel_v.ToString("d6").PadLeft(7,' ')}";
				texts[i].text += $"\ndelta wheel h {m.last_delta_wheel_h.ToString("d4").PadLeft(5,' ')} delta wheel v {m.last_delta_wheel_v.ToString("d4").PadLeft(5,' ')}";
				for ( int b = 0; b != buttons[i].Length; ++b )
				{
					MeshRenderer mr = buttons[i][b];
					MaterialPropertyBlock mpb = mpbs[i][b];
					mpb.SetColor( shader_property_emittance_name, m.buttons_status_down[b] == 1 ? pressed_emittance : unpressed_emittance );
					mr.SetPropertyBlock( mpb );
				}
			}

			if ( Keyboard.current.digit1Key.wasPressedThisFrame )
			{
				update_method = Update_Method.UNITY;
				if ( update_method_text != null )
				{
					update_method_text.text = update_method.ToString();
				}
			} else
			if ( Keyboard.current.digit2Key.wasPressedThisFrame )
			{
				update_method = Update_Method.RAW;
				if ( update_method_text != null )
				{
					update_method_text.text = update_method.ToString();
				}
			}
		}
		#endregion
	}
}
