﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;
using System;
using System.Text;
using System.Collections.Concurrent;
using UnityEngine.InputSystem;

namespace tg
{
	public class FP_Controller : MonoBehaviour
	{
		public enum
		Action_Index
		{
			WALK,
			WALK_BACK,
			WALK_SIDEWAYS_LEFT,
			WALK_SIDEWAYS_RIGHT,
			AIM,
			USE,
			INTERACT,
			SPEED_UP,
			SPEED_DOWN,
			ZERO_SPEED
		}
		public enum
		Mouse_Hand
		{
			LEFT,
			RIGHT
		}

		[Serializable]
		public class
		Mapping : ISerializationCallbackReceiver
		{
			public const float MIN_SENSIBILITY = 0.0001f;
			public const float MAX_SENSIBILITY = 1000.0f;

			[SerializeField]
			public Game_Input.Mapping     mapping;
			public ref Game_Input.Device_Path left_mouse_path  => ref mouses_paths[(int)Mouse_Hand.LEFT];
			public ref Game_Input.Device_Path right_mouse_path => ref mouses_paths[(int)Mouse_Hand.RIGHT];
			[SerializeField]
			public Game_Input.Device_Path[] mouses_paths = new Game_Input.Device_Path[2];
			[SerializeField]
			public Vector2[] mouses_sensibilities = new Vector2[2];

			public EventHandler on_change;

			[NonSerialized]
			public Vector2[] mouses_deltas = new Vector2[2];
			[NonSerialized]
			public Vector2[] mouses_deltas_sensible = new Vector2[2];

			void ISerializationCallbackReceiver.OnBeforeSerialize()
			{
			}
			void ISerializationCallbackReceiver.OnAfterDeserialize()
			{
				setup_mapping( on_deserialized: true );
			}

			public
			void
			copy_from( Mapping m )
			{
				for ( int i = 0; i != mouses_paths.Length; ++i )
				{
					mouses_paths[i] = m.mouses_paths[i];
				}
				for ( int i = 0; i != mouses_sensibilities.Length; ++i )
				{
					mouses_sensibilities[i] = m.mouses_sensibilities[i];
				}
				mapping.copy_from( m.mapping );
			}

			public
			void
			setup_mapping()
			{
				setup_mapping( false );
			}
			private
			void
			setup_mapping( bool on_deserialized )
			{
				Game_Input.Input_Capture_Flags default_flags =
				     Game_Input.Input_Capture_Flags.DEVICE_PATHS
				   | Game_Input.Input_Capture_Flags.DIGITIZED_WHEELS
				   | Game_Input.Input_Capture_Flags.INPUT_RAW
				   | Game_Input.Input_Capture_Flags.NO_GAMEPADS;

				Game_Input.Mapping.Action[] actions = new Game_Input.Mapping.Action[Enum.GetValues( typeof( Action_Index ) ).Length];
				for ( int i = 0; i != actions.Length; ++i )
				{
					actions[i] = new Game_Input.Mapping.Action();
					actions[i].flags = default_flags;
					actions[i].index = i;
				}
				bool ok = false;
				if ( on_deserialized )
				{
					ok = mapping.on_after_deserialized( actions );
				}
				if ( ! ok )
				{
					mapping = new Game_Input.Mapping( actions );
				}
			}


			public
			bool
			change_mouse_paths( Game_Input.Device_Path new_left_mouse_path,
			                    Game_Input.Device_Path new_right_mouse_path )
			{
				bool ret = new_left_mouse_path != left_mouse_path
				        || new_right_mouse_path != right_mouse_path;
				if ( ! ret )
				{
					return ret;
				}

				for ( int i = 0, ic = mapping.sources.occupancy; i != ic; ++i )
				{
					ref Game_Input.Input_Source s = ref mapping.sources[i];
					change_mouse_paths( ref s, new_left_mouse_path, new_right_mouse_path );
				}
				left_mouse_path  = new_left_mouse_path;
				right_mouse_path = new_right_mouse_path;

				on_change?.Invoke( this, null );

				return ret;
			}

			public
			void
			change_mouse_capture( Mouse_Hand           hand,
			                      Action<bool>         callback,
			                      Action<Mouse_Hand,
			                             Game_Input.Device_Path,
			                             Action<bool>> confirmation_required )
			{
				tg_Manager.start_coroutine(
				Game_Input.instance.capture_input_source_co(
				   Game_Input.Input_Capture_Flags.NO_GAMEPADS
				 | Game_Input.Input_Capture_Flags.NO_KEYBOARDS
				 | Game_Input.Input_Capture_Flags.INPUT_RAW
				 | Game_Input.Input_Capture_Flags.DIGITIZED_WHEELS
				 | Game_Input.Input_Capture_Flags.DEVICE_PATHS
				   ,
				   ( r ) => change_mouse_capture_result_handler( hand, r, callback, confirmation_required )
				) );
			}

			public
			float
			get_sensibility( Mouse_Hand hand, int axis )
			{
				float ret = mouses_sensibilities[(int)hand][axis];
				log( $"get sensibility {hand} {axis} => {ret}" );
				return ret;
			}
			public
			float
			set_sensibility( Mouse_Hand hand, int axis, float v )
			{
				float ret = v;
				v = Mathf.Clamp( v, MIN_SENSIBILITY, MAX_SENSIBILITY );
				if ( mouses_sensibilities[(int)hand][axis] != v )
				{
					mouses_sensibilities[(int)hand][axis] = v;
					on_change?.Invoke( this, null );
				}
				return ret;
			}

			public
			void
			remap_capture( Action_Index                    action,
			               Action<bool>                    callback,
			               Action<Action_Index,
			                      Game_Input.Input_Source,
			                      List<int>,
			                      Action<bool>>            confirmation_required )
			{
				var a = mapping.actions[(int)action];
				debug_assert( a.index == (int)action );
				tg_Manager.start_coroutine(
				Game_Input.instance.capture_input_source_co(
				   a.flags | Game_Input.Input_Capture_Flags.ABORT_ON_ESCAPE,
				   ( r ) => remap_capture_result_handler( action, r, callback, confirmation_required ),
				   new Game_Input.Capture_Advanced_Options(){ just_these_mouses = new Game_Input.Device_Path[]{ left_mouse_path, right_mouse_path } }
				) );
			}

			public
			Mouse_Hand
			other_hand( Mouse_Hand hand )
			{
				Mouse_Hand ret = hand == Mouse_Hand.LEFT ? Mouse_Hand.RIGHT : Mouse_Hand.LEFT;
				return ret;
			}

			private
			void
			change_mouse_capture_result_handler( Mouse_Hand                   hand,
			                                     Opt<Game_Input.Input_Source> result,
			                                     Action<bool>                 callback,
			                                     Action<Mouse_Hand,
			                                            Game_Input.Device_Path,
			                                            Action<bool>>         confirmation_required )
			{
				if ( ! result.has_value
				  || result.value.device_path == mouses_paths[(int)hand] )
				{
					callback?.Invoke( false );
					return;
				}

				if ( result.value.device_path == mouses_paths[(int)other_hand(hand)] )
				{
					if ( confirmation_required != null )
					{
						confirmation_required.Invoke( hand,
							result.value.device_path,
							( bool confirmed )=>
							{
								if ( confirmed )
								{
									Game_Input.Device_Path[] new_paths = new Game_Input.Device_Path[2];
									new_paths[(int)other_hand(hand)] = new Game_Input.Device_Path();
									new_paths[(int)hand            ] = result.value.device_path;
									change_mouse_paths( new_paths[(int)Mouse_Hand.LEFT], new_paths[(int)Mouse_Hand.RIGHT] );
								}
								callback?.Invoke( confirmed );
							} );
					} else
					{

						Game_Input.Device_Path[] new_paths = new Game_Input.Device_Path[2];
						new_paths[(int)other_hand(hand)] = new Game_Input.Device_Path();
						new_paths[(int)hand            ] = result.value.device_path;
						change_mouse_paths( new_paths[(int)Mouse_Hand.LEFT], new_paths[(int)Mouse_Hand.RIGHT] );
						callback?.Invoke( true );
					}
				} else
				{
					Game_Input.Device_Path[] new_paths = new Game_Input.Device_Path[2];
					new_paths[(int)other_hand(hand)] = mouses_paths[(int)other_hand(hand)];
					new_paths[(int)hand            ] = result.value.device_path;
					change_mouse_paths( new_paths[(int)Mouse_Hand.LEFT], new_paths[(int)Mouse_Hand.RIGHT] );
					callback?.Invoke( true );
				}
			}


			private
			void
			remap_capture_result_handler( Action_Index                    action,
			                              Opt<Game_Input.Input_Source>    result,
			                              Action<bool>                    callback,
			                              Action<Action_Index,
			                                     Game_Input.Input_Source,
			                                     List<int>,
			                                     Action<bool>>            confirmation_required )
			{
				bool did_change = false;
				if ( ! result.has_value )
				{
					callback?.Invoke( did_change );
					return;
				}

				#if false
				// NOTE(theGiallo): remove other mouses
				if ( result.value.type == Game_Input.Input_Source.Type.COMBINED )
				{
					bool removed_some = false;
					Bit_Vector removed = new Bit_Vector( result.value.combined.Length );
					int removed_count = 0;
					for ( int i = 0; i != result.value.combined.Length; ++i )
					{
						if ( result.value.combined[i].input_system == Game_Input.Input_Source.Input_System.RAW
						  && result.value.combined[i].type == Game_Input.Input_Source.Type.MOUSE_BUTTON
						  && result.value.combined[i].device_path.chars_length > 0
						  && result.value.combined[i].device_path != left_mouse_path
						  && result.value.combined[i].device_path != right_mouse_path )
						{
							removed_some = true;
							removed[i] = 1;
							++removed_count;
						}
					}
					if ( removed_count == result.value.combined.Length )
					{
						callback?.Invoke( did_change );
						return;
					}
					if ( removed_some )
					{
						Game_Input.Input_Source[] new_comb = new Game_Input.Input_Source[result.value.combined.Length - removed_count];
						for ( int i = 0, j = 0; i != result.value.combined.Length; ++i )
						{
							if ( removed[i] == 0 )
							{
								new_comb[j++] = result.value.combined[i];
							}
						}
						var r = result.value;
						r._combined = new_comb;
						result = r;
					}
				} else
				{
					if ( result.value.input_system == Game_Input.Input_Source.Input_System.RAW
					  && result.value.type == Game_Input.Input_Source.Type.MOUSE_BUTTON
					  && result.value.device_path.chars_length > 0
					  && result.value.device_path != left_mouse_path
					  && result.value.device_path != right_mouse_path )
					{
						callback?.Invoke( did_change );
						return;
					}
				}
				#endif

				if ( mapping.already_mapped( (int)action, result, out List<int> colliding_actions ) )
				{
					if ( confirmation_required != null )
					{
						confirmation_required.Invoke( action, result, colliding_actions,
							( bool confirmed )=>
							{
								if ( confirmed )
								{
									remap_removing_collisions( action, result, colliding_actions );
								}
								callback?.Invoke( confirmed );
							} );
					} else
					{
						remap_removing_collisions( action, result, colliding_actions );
						did_change = true;
						callback?.Invoke( did_change );
					}
				} else
				{
					remap( action, result );
					did_change = true;
					callback?.Invoke( did_change );
				}
			}

			private
			void
			remap( Action_Index action, Game_Input.Input_Source source )
			{
				mapping.sources[(int)action] = source;
				on_change?.Invoke( this, null );
			}

			private
			void
			remap_removing_collisions( Action_Index action, Game_Input.Input_Source source, List<int> colliding_actions )
			{
				mapping.remove_collisions( (int)action, source, colliding_actions );
				remap( action, source );
			}

			private
			void
			change_mouse_paths( ref Game_Input.Input_Source s,
			                    Game_Input.Device_Path new_left_mouse_path,
			                    Game_Input.Device_Path new_right_mouse_path )
			{
				if ( s.type == Game_Input.Input_Source.Type.COMBINED )
				{
					for ( int i = 0; i != s.combined.Length; ++i )
					{
						change_mouse_paths( ref s.combined[i], new_left_mouse_path, new_right_mouse_path );
					}
				} else
				if ( s.type != Game_Input.Input_Source.Type.NONE
				  && s.input_system == Game_Input.Input_Source.Input_System.RAW )
				{
					if ( s.device_path == left_mouse_path )
					{
						s._device_path = new_left_mouse_path;
					} else
					if ( s._device_path == right_mouse_path )
					{
						s._device_path = new_right_mouse_path;
					} else
					{
						ILLEGAL_PATH();
					}
				}
			}

			public
			void
			fixed_update()
			{
				mapping.fixed_update();
				if ( mouses_paths != null )
				for ( int i = 0; i != 2; ++i )
				{
					if ( Game_Input.instance.mouses.TryGetValue( mouses_paths[i], out Game_Input.Mouse_Status value ) )
					{
						mouses_deltas[i] = new Vector2( value.last_delta_pos_x, value.last_delta_pos_y );
						mouses_deltas_sensible[i] = mouses_deltas[i] * mouses_sensibilities[i];
					}
				}
			}
		}

		[HideInInspector]
		public Mapping mapping;
		public EventHandler<Mapping> on_mapping_loaded;

		public Camera camera_main;
		public Camera camera_3D_ui;
		public Camera camera_3rd_person;
		public RawImage camera_main_raw_image;
		public RawImage camera_3D_ui_raw_image;
		public RawImage camera_3rd_person_raw_image;
		public Transform walk_dir_arrow;
		public Animator animator;
		public Transform ground_zero;

		[Space]
		public bool spawn_debug_arrows = false;
		public GameObject collision_marker_prefab;

		[Space]
		public Transform step_check_area_foot;
		public Step_Checker step_checker;
		public float char_height;
		public float step_max_height_1_height = 0.4f;
		public float step_max_height_0_height = 0.3f;
		public float step_max_height_neg1_height = 0.15f;
		public float curr_step_max_height => max_step( height );
		public float step_lenght = 0.35f;

		public enum Curve_Type
		{
			LERP,
			SMOOTHSTEP,
			SMOOTHERSTEP,
			SIN,
			ANIMATION_CURVE,
		}

		#region private
		#region save_load_settings
		private const string mapping_file_name = "fp_controller_mapping.json";
		private string default_profile_name = "default";
		private
		bool
		load_mapping( byte[] data )
		{
			bool ret = false;
			string str = Encoding.UTF8.GetString( data );
			try {
				Mapping m = JsonUtility.FromJson<Mapping>( str );
				log( "loaded mapping" );
				log( m.get_sensibility( Mouse_Hand.LEFT, 0 ).ToString() );
				mapping.copy_from( m );
				on_mapping_loaded?.Invoke( this, mapping );
				ret = true;
			} catch ( Exception e )
			{
				log_err( e.Message );
			}
			return ret;
		}
		private
		void
		request_load_mapping()
		{
			string profile_name = default_profile_name;
			Save_Load_Manager.load( mapping_file_name,
			                        profile_name,
			( Opt<byte[]> data )=>
			{
				if ( data.has_value )
				{
					log( "got file" );
					bool did_load = load_mapping( data );
					if ( ! did_load )
					{
						log_err( "failed to load mappings" );
					}
				} else
				{
					log( "did'd got mapping file" );
				}
			} );
		}
		private
		void
		request_save_mapping()
		{
			log( "request save" );
			string profile_name = default_profile_name;
			string s = JsonUtility.ToJson( mapping );
			byte[] data = Encoding.UTF8.GetBytes( s );
			Save_Load_Manager.save( mapping_file_name, profile_name, data );
		}
		#endregion save_load_settings

		private
		float
		max_step( float height )
		{
			float ret =
			   height >= 0
			   ? Mathf.Lerp( step_max_height_0_height, step_max_height_1_height, height )
			   : Mathf.Lerp( step_max_height_0_height, step_max_height_neg1_height, - height );
			return ret;
		}

		private
		void
		update_step_check_area( Vector3 vel, float step_height )
		{
			//Vector3 p = step_check_area_foot.localPosition;
			//p.y = step_max_height;
			//step_check_area_foot.localPosition = p;

			Vector3 v = vel;// body_rb.velocity;
			Vector3 loc_v_dir = transform.InverseTransformVector( v ).normalized;
			//log( $"step_check_area_foot.position {step_check_area_foot.position}" );
			step_check_area_foot.localPosition = ground_zero.localPosition + loc_v_dir * step_lenght + new Vector3( 0, step_height, 0 );
			//log( $"vel {vel} loc_v_dir {loc_v_dir} step_check_area_foot.position {step_check_area_foot.position} ground_zero.position {ground_zero.position}" );
			//step_checker.box.center = step_checker.box.center;
		}

		private
		bool
		action_just_on( Action_Index a )
		{
			bool ret = mapping.mapping.actions_just_activated[(int)a] == 1;
			return ret;
		}
		private
		bool
		action_just_off( Action_Index a )
		{
			bool ret = mapping.mapping.actions_just_deactivated[(int)a] == 1;
			return ret;
		}
		private
		bool
		action_on( Action_Index a )
		{
			bool ret = mapping.mapping.actions_active[(int)a] == 1;
			return ret;
		}

		private
		float
		delta_rotate_shoulders()
		{
			float ret = mapping.mouses_deltas_sensible[(int)Mouse_Hand.LEFT].x;
			return ret;
		}
		private
		float
		delta_height()
		{
			float ret = mapping.mouses_deltas_sensible[(int)Mouse_Hand.LEFT].y * mouse_height_factor;
			return ret;
		}
		private
		float
		delta_rotate_head_yaw()
		{
			float ret = mapping.mouses_deltas_sensible[(int)Mouse_Hand.RIGHT].x;
			return ret;
		}
		private
		float
		delta_rotate_head_pitch()
		{
			float ret = mapping.mouses_deltas_sensible[(int)Mouse_Hand.RIGHT].y;
			return ret;
		}

		#region controller variables
		[Header("Structure")]
		public Transform head_tr;
		public Transform head_pivot_tr;
		public Transform head_colliders_container;
		public Transform neck_collider_tr;
		public Transform body_tr;
		public Rigidbody body_rb;

		[Header("Movement")]
		[Range(1,2)]
		public float movement_speed_up_coefficient = 1.05f;
		[Range(0,6)]
		public float min_movement_speed = 0.1f;
		[Range(0,6)]
		public float movement_speed_threshold_abs = 1;
		[Tooltip("m/s")]
		public float normal_movement_speed= 4;
		[Tooltip("m/s")]
		public float max_movement_speed_pos = 8;
		[Tooltip("m/s")]
		public float max_movement_speed_neg = -4;
		[Range(1,50)]
		public float min_walking_static_vel;
		[Range(1,50)]
		public float initial_movement_speed;
		public float drag_if_stopped;
		public float default_drag;
		public float angular_grag_if_not_walking = 0.05f;
		public float vel_dir_up_coefficient = 0.1f;
		public float anti_gravity_coefficient = 2f / 3f;
		public bool  anti_gravity_as_grounded = true;
		public bool  anti_gravity = true;
		public float grounded_height = 0.3f;
		public float grounded_forgiveness_ms = 500f;
		public float walking_back_vel_coeff = 0.2f;
		public float walking_sideways_vel_coeff = 0.4f;
		public float mouse_height_factor = 1 / 128f;
		public bool  force_initial_velocity = false;
		public float prone_movement_speed_factor = 0.05f;
		public float height_walk_speed_factor = 0.8f;
		public float height_walk_speed_min    = 0.2f;

		[Space]
		[Range(0,2)]
		public float prev_walk_vel_attenuator = 0.9f;
		public bool  contrast_old_walk_vel = false;

		[Header("Friction")]
		public float max_friction_force = 4f;
		public float straight_friction_factor = 0.8f;
		public float max_straight_friction_force = 4f;

		[Header("Head")]
		[Range(0,179)]
		public float head_yaw_max_abs_deg;
		[Range(-180,180)]
		public float head_pitch_min_deg;
		[Range(-180,180)]
		public float head_pitch_max_deg;

		[Space]
		public bool body_yaw_manual = false;

		[Header("Feet")]
		public PhysicMaterial feet_physicMaterial;
		public float          static_friction_threshold_value = 10f;
		public float          slipping_velocity = 0.1f;

		[Header("Jump")]
		[Foldout("jump", true)]
		public float jump_dh_base    = 1f;
		public float jump_dh_factor  = 2f;
		public float jump_f_base     = 0f;
		public float jump_f_factor   = 0f;
		public float jump_f_factor_2 = 1f;
		public float min_base_height_to_jump  = 0.5f;
		public float min_jump_dtms   = 1000f;
		[Foldout("jump", false)]
		public float max_jump_force  = 1000f;

		[Header("Stance")]
		[Foldout("height", true)]
		public float height_standup  =  1f;
		public float height_crouched =  0f;
		public float height_prone    = -1f;
		public float height_retraction_if_blocked = 0.005f;
		public float stance_activation_prone_to_crouch = 1f;
		public float stance_activation_crouch_to_prone = 1f;
		public float air_prone_trigger_speed = 4;
		public float stance_activation_tolerance_ms = 100;
		public Curve_Type curve_type;
		public AnimationCurve height_curve;
		public float prone_to_crouch_timer_ms = 500;
		[Foldout("height", false )]
		public float crouch_to_prone_timer_ms = 500;

		public bool  is_grounded { get; private set; }
		public bool  as_grounded { get; private set; }
		public float last_ms_grounded { get; private set; }
		private bool was_walking;
		private Vector3 prev_walk_vel;
		public Vector3 curr_walk_velocity { get; private set; }
		public Vector3 adjusted_walk_velocity { get; private set; }
		public Vector3 contrasting_vel { get; private set; }
		public float movement_speed { get; private set; }
		public float head_yaw_deg { get; private set; }
		public float head_pitch_deg { get; private set; }
		public float last_head_yaw { get; private set; }
		public float last_head_pitch { get; private set; }
		public float abs_vel { get; private set; }  = 0;
		public float curr_vel_fw { get; private set; }
		public float movement_velocity { get; private set; }
		public float height { get; private set; } = 1f;
		public float height01_linear { get; private set; }
		public float stance_activation { get; private set; }
		public double last_time_stance_activation { get; private set; }
		public Stance stance { get; private set; }
		public enum Stance
		{
			STANDING     = 0,
			CROUCHED_MIN = 1,
			PRONE        = 2
		}

		public float  last_height_min { get; private set; }
		public double last_height_min_time_ms { get; private set; }
		public float  last_height_max { get; private set; }
		public double last_height_max_time_ms { get; private set; }
		public float  last_delta_height { get; private set; }
		public bool   height_going_up => last_delta_height >= 0;
		public float  max_height { get; private set; }

		public bool   top_blocked { get; private set; } = false;
		public bool   head_top_blocked { get; private set; } = false;

		public Vector3 prev_body_rot { get; private set; }
		//public Vector3 prev_head_rot { get; private set; }
		private
		void
		contrast_body_rot()
		{
			Vector3 body_rot = body_tr.localRotation.eulerAngles;

			float da = body_rot.y - prev_body_rot.y;
			if ( da > 180 )
			{
				da -= 360;
			} else
			if ( da < -180 )
			{
				da += 360;
			}
			head_yaw_deg -= da;
			apply_head_rot();

			prev_body_rot = body_rot;
		}

		private Fixed_Timer prone_to_crouch_timer;
		private Fixed_Timer crouch_to_prone_timer;
		private Fixed_Timer landing_timer;

		private int animator_id_height;
		private int animator_id_xz_speed;
		private int animator_id_walk_speed;
		private int animator_id_stance;
		private int animator_id_grounded;
		private int animator_id_really_grounded;

		private
		struct
		Friction
		{
			public float dyn;
			public float stc;
			public PhysicMaterialCombine combine;
			public Friction( PhysicMaterial pm )
			{
				dyn     = pm.dynamicFriction;
				stc     = pm.staticFriction;
				combine = pm.frictionCombine;
			}
			public
			void
			sum( PhysicMaterial colliding, PhysicMaterial to_avg )
			{
				// NOTE(theGiallo):  The priority order is as follows: Average < Minimum < Multiply < Maximum.
				// https://docs.unity3d.com/ScriptReference/PhysicMaterialCombine.html

				if ( colliding.frictionCombine == PhysicMaterialCombine.Maximum || to_avg.frictionCombine == PhysicMaterialCombine.Maximum )
				{
					dyn += Mathf.Max( colliding.dynamicFriction, to_avg.dynamicFriction );
					stc += Mathf.Max( colliding.staticFriction , to_avg.staticFriction  );
				} else
				if ( colliding.frictionCombine == PhysicMaterialCombine.Multiply || to_avg.frictionCombine == PhysicMaterialCombine.Multiply )
				{
					dyn += colliding.dynamicFriction * to_avg.dynamicFriction;
					stc += colliding.staticFriction  * to_avg.staticFriction;
				} else
				if ( colliding.frictionCombine == PhysicMaterialCombine.Minimum || to_avg.frictionCombine == PhysicMaterialCombine.Minimum )
				{
					dyn += Mathf.Min( colliding.dynamicFriction, to_avg.dynamicFriction );
					stc += Mathf.Min( colliding.staticFriction , to_avg.staticFriction  );
				} else
				if ( colliding.frictionCombine == PhysicMaterialCombine.Average || to_avg.frictionCombine == PhysicMaterialCombine.Average )
				{
					dyn += ( colliding.dynamicFriction + to_avg.dynamicFriction ) * 0.5f;
					stc += ( colliding.staticFriction  + to_avg.staticFriction  ) * 0.5f;
				}
			}
		}


		#endregion

		private Vector3 checkpoint_position = new Vector3( 32, 9, -15 );
		private Quaternion checkpoint_rotation = Quaternion.identity;
		private
		void
		fixed_update_controls()
		{
			if ( false )
			{
				Vector3 r = transform.localRotation.eulerAngles;
				r.x = r.z = 0;
				transform.localRotation = Quaternion.Euler( r );
			}

			if ( ! capturing_mouse )
			{
				return;
			}

			contrast_body_rot();

			bool anti_gravity_now = true;

			if ( Game_Input.instance.merged_keyboards_per_frame.key_front_down( Game_Input.Index.backspace ) )
			{
				checkpoint_position = transform.position;
				checkpoint_rotation = transform.rotation;
			}
			if ( Game_Input.instance.merged_keyboards_per_frame.key_front_down( Game_Input.Index.f5 ) )
			{
				transform.position = checkpoint_position;
				transform.rotation = checkpoint_rotation;
				body_rb.velocity = Vector3.zero;
			}
			if ( Game_Input.instance.merged_keyboards_per_frame.key_front_down( Game_Input.Index.enter ) )
			{
				transform.position = Vector3.zero;
				body_rb.velocity = Vector3.zero;
			}

			if ( action_on( Action_Index.SPEED_UP ) )
			{
				if ( movement_speed == 0 )
				{
					movement_speed = initial_movement_speed;
				}
				//log( "speed up" );
				//if ( movement_speed <= 0 && movement_speed >= -movement_speed_threshold_abs )
				//{
				//	movement_speed = -movement_speed;
				//}
				if ( movement_speed > 0 )
				{
					movement_speed *= movement_speed_up_coefficient;
				}
				//else
				//{
				//	movement_speed /= movement_speed_up_coefficient;
				//}
				//movement_speed = Mathf.Clamp( movement_speed, max_movement_speed_neg, max_movement_speed_pos );
				movement_speed = Mathf.Clamp( movement_speed, 0, max_movement_speed_pos );
			}
			if ( action_on( Action_Index.SPEED_DOWN ) )
			{
				//if ( movement_speed == 0 )
				//{
				//	movement_speed = -initial_movement_speed;
				//}
				log( "speed down" );
				//if ( movement_speed >= 0 && movement_speed <= movement_speed_threshold_abs )
				//{
				//	movement_speed = -movement_speed;
				//}
				if ( movement_speed > 0 )
				{
					movement_speed /= movement_speed_up_coefficient;
				}
				if ( movement_speed < min_movement_speed )
				{
					movement_speed = 0;
				}
				//else
				//{
				//	movement_speed *= movement_speed_up_coefficient;
				//}
				//movement_speed = Mathf.Clamp( movement_speed, max_movement_speed_neg, max_movement_speed_pos );
			}


			// NOTE(theGiallo): check for blocking collisions ontop
			top_blocked = false;
			head_top_blocked = false;
			{
				float top_h = step_checker.box.size.y * step_checker.box.transform.transform.lossyScale.y / body_tr.lossyScale.y;
				float eps_h = 0.1f;
				for ( int i = 0; i != colliding_points.Count; ++i )
				{
					Colliding_Point cp = colliding_points[i];
					if ( ! cp.has_grounded_local_pos )
					{
						continue;
					}
					if ( cp.grounded_local_pos.y >= top_h - eps_h )
					{
						top_blocked = true;
						if ( cp.contact_point.thisCollider.transform.parent == head_colliders_container )
						{
							head_top_blocked = true;
						}
					}
				}
			}


			if ( ! landing_timer.ended )
			{
				landing_timer.fixed_update();
			}
			float curr_ms = (float) FrameTimeManager.curr_fixu_end_time_ms;
			bool new_as_grounded = is_grounded || curr_ms - last_ms_grounded < grounded_forgiveness_ms;
			bool landed = new_as_grounded && ! as_grounded;
			if ( landed )
			{
				log( $"max_height = {max_height}" );
				float vel = Mathf.Abs( body_rb.velocity.y );//.magnitude;
				log( $"vel = {vel}" );
				float ms_in_air = curr_ms - last_ms_grounded;
				if ( landing_timer.ended )
				{
					landing_timer.set_duration( stun_after_land_ms( vel ) );
					landing_timer.restart();
				} else
				{
					float ms = stun_after_land_ms( vel );
					ms += landing_timer.remaining_ms;
					landing_timer.set_duration( ms );
					landing_timer.restart();
				}
			}
			as_grounded = new_as_grounded;
			if ( is_grounded )
			{
				last_ms_grounded = curr_ms;
			}


			if ( ! prone_to_crouch_timer.ended )
			{
				prone_to_crouch_timer.fixed_update();
			}
			if ( ! crouch_to_prone_timer.ended )
			{
				crouch_to_prone_timer.fixed_update();
			}

			float delta_h = delta_height();
			if ( stance == Stance.CROUCHED_MIN && delta_h > 0 && ! prone_to_crouch_timer.ended )
			{
				if ( ! prone_to_crouch_timer.ended )
				{
					delta_h = 0;
				}
			}
			if ( top_blocked && delta_h >= 0 && last_delta_height > 0 )
			{
				delta_h = -last_delta_height;
			} else
			if ( top_blocked && delta_h >= 0 )
			{
				delta_h = -height_retraction_if_blocked;
			}
			if ( delta_h < 0 || ! top_blocked )
			{
				if ( ( stance == Stance.CROUCHED_MIN && delta_h > 0 )
				  || ( stance == Stance.PRONE && delta_h < 0 ) )
				{
					stance_activation = 0;
				} else
				if ( ( stance == Stance.CROUCHED_MIN && delta_h == 0 )
				  || ( stance == Stance.PRONE && delta_h == 0 ) )
				{
					if ( FrameTimeManager.curr_fixu_end_time_ms - last_time_stance_activation > stance_activation_tolerance_ms )
					{
						stance_activation = 0;
					}
				}

				if ( as_grounded )
				{
					if ( ( stance == Stance.CROUCHED_MIN && delta_h < 0 )
					  || ( stance == Stance.PRONE && delta_h > 0 ) )
					{
						stance_activation += delta_h;
						last_time_stance_activation = (double)FrameTimeManager.curr_fixu_end_time_ms;
					} else
					if ( stance == Stance.STANDING
					  || ( stance == Stance.CROUCHED_MIN && stance_activation == 0 ) )
					{
						height01_linear = Mathf.Clamp01( height01_linear + delta_h );
						update_height();
					}
				} else
				{
					stance_activation = 0;
				}

				float stance_activation_abs = Mathf.Abs( stance_activation );

				if ( stance == Stance.STANDING && height == 0 )
				{
					stance = Stance.CROUCHED_MIN;
				} else
				if ( stance == Stance.CROUCHED_MIN && height > 0 )
				{
					stance = Stance.STANDING;
				} else
				if ( stance == Stance.CROUCHED_MIN && stance_activation_abs >= stance_activation_crouch_to_prone )
				{
					stance = Stance.PRONE;
					stance_activation = 0;
					#if UNITY_EDITOR
					crouch_to_prone_timer.set_duration( crouch_to_prone_timer_ms );
					#endif
					crouch_to_prone_timer.restart();
					//height01_linear = 0;
					//update_height();
				} else
				if ( stance == Stance.PRONE && stance_activation_abs >= stance_activation_prone_to_crouch )
				{
					stance = Stance.CROUCHED_MIN;
					stance_activation = 0;
					height01_linear = 0;//stance_activation_abs - stance_activation_prone_to_crouch;
					update_height();
					#if UNITY_EDITOR
					prone_to_crouch_timer.set_duration( prone_to_crouch_timer_ms );
					#endif
					prone_to_crouch_timer.restart();
				}
			} else
			{
				delta_h = 0;
			}

			//height = Mathf.Clamp01( height + delta_h );

			float delta_head_yaw_deg   = delta_rotate_head_yaw();
			float delta_head_pitch_deg = delta_rotate_head_pitch();
			float delta_body_yaw_deg   = delta_rotate_shoulders();
			if ( head_top_blocked )
			{
				delta_head_pitch_deg = Mathf.Min( 0, delta_head_pitch_deg );
				if ( delta_head_pitch_deg == 0 )
				{
					head_pitch_deg = last_head_pitch;
				}
			}
			last_head_pitch = head_pitch_deg;
			head_pitch_deg += delta_head_pitch_deg;

			last_head_yaw = head_yaw_deg;
			//if ( head_pivot_node.up.y >= 0 )
			//if ( head_pivot_node.localRotation.x >= 0 )
			//if ( head_pitch >= -145 )
			if ( false ) // NOTE(theGiallo): doing this in contrast_body_rot, because we use physics to rotate, and could rotate less than expected
			{
				head_yaw_deg   += delta_head_yaw_deg - delta_body_yaw_deg;
			} else
			{
				head_yaw_deg += delta_head_yaw_deg;
			}
			//else
			//{
			//	head_yaw   += -delta_head_yaw - delta_body_yaw;
			//	//head_pivot_node.localRotation = Quaternion.Euler( -head_pitch, head_yaw, 0 );
			//}
			apply_head_rot();

			//head_pivot_node.Rotate( Vector3.up,        delta_head_yaw,   Space.Self );
			//head_pivot_node.Rotate( -body_node.right,  delta_head_pitch, Space.World );

			//body_rb.angularDrag = angular_grag_if_not_walking;

			//body_rb.angularVelocity = Vector3.zero;
			if ( stance == Stance.PRONE && is_grounded )
			{
				body_rb.constraints = RigidbodyConstraints.None;
				body_rb.maxAngularVelocity = Mathf.PI / 4f;
			} else
			{
				body_rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
				body_rb.maxAngularVelocity = Mathf.PI * 2f / 0.1f;
			}
			body_rb.maxDepenetrationVelocity = 0.1f * 120f;

			if ( body_rb.constraints == RigidbodyConstraints.None )
			{
				Vector3 e, c;
				e = c = body_tr.localRotation.eulerAngles;
				float a = 30;

				if ( e.x > 180 ) e.x -= 360;
				if ( e.z > 180 ) e.z -= 360;

				c.x = Mathf.Clamp( e.x, -a, a );
				c.z = Mathf.Clamp( e.z, -a, a );
				#if false
				body_tr.localRotation = Quaternion.Euler( e );
				#else
				Vector3 d = c - e;
				// BAD CODE body_rb.angularVelocity += d / Time.fixedDeltaTime;
				d = body_tr.TransformVector( d );
				d *= Mathf.Deg2Rad;
				Vector3 v = d / Time.fixedDeltaTime;
				//if ( ! float.IsNaN( v.x )
				//  && ! float.IsNaN( v.y )
				//  && ! float.IsNaN( v.z )
				//  && ! float.IsInfinity( v.x )
				//  && ! float.IsInfinity( v.y )
				//  && ! float.IsInfinity( v.z )
				//  && ! float.IsPositiveInfinity( v.x )
				//  && ! float.IsPositiveInfinity( v.y )
				//  && ! float.IsPositiveInfinity( v.z )
				//  && ! float.IsNegativeInfinity( v.x )
				//  && ! float.IsNegativeInfinity( v.y )
				//  && ! float.IsNegativeInfinity( v.z )
				//  )
				{
					//log( $"body rb ang vel = {body_rb.angularVelocity} v = {v} e = {e} c = {c} d = {d}" );
					body_rb.angularVelocity += v;
				}
				#endif
			}

			if ( stance != Stance.PRONE && is_grounded )
			{
				Vector3 e = body_tr.localRotation.eulerAngles;
				e.x = e.z = 0f;
				body_tr.localRotation = Quaternion.Euler( e );
			}

			{
				Vector3 av = body_rb.angularVelocity;
				av = body_tr.InverseTransformVector( av );
				av.y = 0;
				av = body_tr.TransformVector( av );
				body_rb.angularVelocity = av;
			}
			if ( body_yaw_manual )
			{
				body_tr.Rotate( Vector3.up, delta_body_yaw_deg, Space.Self );
			} else
			{
				//body_rb.angularDrag = 0;
				body_rb.angularVelocity += body_tr.TransformVector( new Vector3( 0, Mathf.Deg2Rad * delta_body_yaw_deg / Time.fixedDeltaTime, 0 ) );
			}

			body_rb.drag = default_drag;

			bool is_walking_fwd        = false;
			bool is_walking_back       = false;
			bool is_walking_side_left  = false;
			bool is_walking_side_right = false;
			bool is_walking_straight   = false;
			bool is_walking_sideways   = false;
			bool is_walking            = false;
			bool jumped                = false;

			if ( as_grounded )
			{
				is_walking_fwd        = action_on( Action_Index.WALK );
				is_walking_back       = action_on( Action_Index.WALK_BACK );
				is_walking_side_left  = action_on( Action_Index.WALK_SIDEWAYS_LEFT  );
				is_walking_side_right = action_on( Action_Index.WALK_SIDEWAYS_RIGHT );
				is_walking_straight   = is_walking_fwd != is_walking_back;
				is_walking_sideways   = ! is_walking_straight && ( is_walking_side_left != is_walking_side_right );
				is_walking            = is_walking_straight || is_walking_sideways;

				if ( ( stance == Stance.CROUCHED_MIN && ! prone_to_crouch_timer.ended )
				  || ( stance == Stance.PRONE        && ! crouch_to_prone_timer.ended )
				  || ! landing_timer.ended )
				{
					is_walking_fwd        = false;
					is_walking_back       = false;
					is_walking_side_left  = false;
					is_walking_side_right = false;
					is_walking_straight   = false;
					is_walking_sideways   = false;
					is_walking            = false;
				}
			}

			if ( delta_h > 0 && height == height_standup && last_height_max != height )
			{
				last_height_max = height;
				last_height_max_time_ms = FrameTimeManager.curr_fixu_end_time_ms;

				// NOTE(theGiallo): jump
				if ( as_grounded && stance == Stance.STANDING )
				{
					float start_h = Mathf.Clamp( last_height_min, height_crouched, height_standup );
					float dh = height - start_h;
					float dtms = (float)( last_height_max_time_ms - last_height_min_time_ms );
					if ( dh > min_base_height_to_jump && dtms < min_jump_dtms )
					{
						jumped = true;
						float dts = dtms * 1e-4f;
						float dh01 = ( dh - min_base_height_to_jump ) / ( height_standup - min_base_height_to_jump );
						float dh_speed = dh01 / dts;
						float dhE = dh_speed / dts;
						float dhx = jump_dh_base + jump_dh_factor * dhE;
						float jump_force_m = jump_f_base + jump_f_factor * dhx + jump_f_factor_2 * dhx * dhx;
						jump_force_m = Mathf.Min( jump_force_m, max_jump_force );
						Vector3 jump_force = Vector3.up * jump_force_m;
						body_rb.AddForce( jump_force * ( ( 2f / 3f ) / Time.fixedDeltaTime ) );
						log( $"jump force {jump_force_m} dh01: {dh01} dhx: {dhx} dtms: {dtms} dts: {dts} dhE: {dhE} dh_speed: {dh_speed}" );
						max_height = 0;
					}
				}
			} else
			if ( delta_h != 0 )
			{
				if ( ( delta_h > 0 ) != ( last_delta_height > 0 ) )
				{
					if ( last_delta_height > 0 )
					{
						last_height_max = height;
						last_height_max_time_ms = FrameTimeManager.curr_fixu_end_time_ms;
					} else
					{
						last_height_min = height;
						last_height_min_time_ms = FrameTimeManager.curr_fixu_end_time_ms;
					}
				}
				last_delta_height = delta_h;
			}

			abs_vel = 0;
			if ( as_grounded )
			{
				Vector3 walk_dir = is_walking_sideways ? body_tr.right : body_tr.forward;
				float mov = 0f;
				if ( is_walking_straight && is_walking_fwd )
				{
					mov = 1f;
				} else
				if ( is_walking_straight && is_walking_back )
				{
					mov = walking_back_vel_coeff;
					walk_dir = - walk_dir;
				} else
				if ( is_walking_sideways && is_walking_side_left )
				{
					mov = walking_sideways_vel_coeff;
					walk_dir = - walk_dir;
				} else
				if ( is_walking_sideways && is_walking_side_right )
				{
					mov = walking_sideways_vel_coeff;
				}
				adjusted_walk_velocity = Vector3.zero;

				curr_walk_velocity = Vector3.zero;

				if ( action_just_on( Action_Index.ZERO_SPEED ) )
				{
					movement_speed = 0.0f;
					body_rb.drag = drag_if_stopped;
				}

				if ( stance == Stance.PRONE )
				{
					mov *= prone_movement_speed_factor;
				} else
				{
					mov *= standing_height_movement();
				}

				if ( is_walking )
				{
					abs_vel = movement_speed * mov;
					curr_walk_velocity = walk_dir * abs_vel;
					body_rb.drag = 0;
				}
				adjusted_walk_velocity += curr_walk_velocity;

				if ( was_walking && contrast_old_walk_vel )
				{
					Vector3 wn = adjusted_walk_velocity.normalized;
					Vector3 up = Vector3.Cross( wn, prev_walk_vel );
					Vector3 side = Vector3.Cross( up, wn );
					contrasting_vel = -side * prev_walk_vel_attenuator;
					adjusted_walk_velocity += contrasting_vel;
				}

				// NOTE(theGiallo): auto-stepping
				{
					bool has_to_step = false;
					bool can_step = true;
					float step_height = 0;
					float max_step_h = curr_step_max_height;
					for ( int i = 0; i != colliding_points.Count; ++i )
					{
						Colliding_Point cp = colliding_points[i];
						if ( cp.has_our_normal )
						{
							float cos_coll = Vector3.Dot( cp.our_normal, walk_dir );
							float cos_hpi = 0;
							bool is_obstacle = cos_coll > cos_hpi;
							debug_assert( cp.has_grounded_local_pos );
							float curr_step_height = cp.grounded_local_pos.y;
							bool this_can_step = curr_step_height <= max_step_h;
							bool this_has_to_step = is_obstacle && this_can_step;
							has_to_step = has_to_step || this_has_to_step;
							can_step    = can_step    || this_can_step;
							if ( this_has_to_step )
							{
								step_height = Mathf.Clamp( step_height, curr_step_height, max_step_h );
							}
						}
					}
					update_step_check_area( curr_walk_velocity, step_height );
					if ( is_walking && step_checker.check_now() )
					{
						if ( has_to_step && step_height < 0.05f )
						{
							//log( $"step_height {step_height}" );
							//transform.position += new Vector3( 0, step_height, 0 );
						} else
						if ( has_to_step && can_step )
						{
							float g = Physics.gravity.y;
							// step_height = 0.5f * g * t*t + ( v0 + v ) * t;
							//float step_time_per_1m = 1f / movement_speed;
							//float t = step_time_per_1m * step_height;//Time.fixedDeltaTime;
							//if ( t > 1e-3 )
							{
								float v0 = body_rb.velocity.y;
								// {
								//    step_height = 0.5f * g * t*t + ( v0 + sv ) * t
								//    sv = ( step_height - 0.5f * g * t*t ) / t - v0;
								//    sv + v0 = ( step_height - 0.5f * g * t*t ) / t;
								//
								//    sv = g * t - v0;
								//    sv + v0 = - g * t;
								//    t = ( sv + v0 ) / -g
								// }
								// sv + v0 = ( step_height - 0.5f * g * ( sv + v0 ) / -g * ( sv + v0 ) / -g ) / ( ( sv + v0 ) / -g );
								// sv + v0 = ( step_height - 0.5f * ( sv + v0 ) * ( sv + v0 ) / g ) * -g / ( sv + v0 );
								// sv + v0 = ( step_height - 0.5f / g * ( sv + v0 )^2 ) * -g / ( sv + v0 );
								// sv + v0 = ( step_height * g - 0.5f * ( sv + v0 )^2 ) / ( sv + v0 );
								// ( sv + v0 )^2 = - step_height * g + 0.5f * ( sv + v0 )^2;
								// 1.5 * ( sv + v0 )^2 = - step_height * g;
								// 1.5 * ( sv + v0 )^2 + step_height * g = 0;
								// sv * sv + 2 * v0 * sv + v0 * v0 - 2 / 3 * step_height * g = 0;
								#if true
								float V01 = Mathf.Sqrt( - step_height * g * 2f / 3f );
								float V = Mathf.Abs( V01 );
								float step_vel0 = V - v0;
								#else
								float b = 2 * v0;
								float hb = v0;
								float c = v0 * v0 - 2f / 3f * step_height * g;
								float sv0 = -hb + Mathf.Sqrt( hb * hb - c );
								float sv1 = -hb - Mathf.Sqrt( hb * hb - c );
								float step_vel0 = Mathf.Max( sv0, sv1 );
								#endif
								//float step_vel0 = ( step_height - 0.5f * g * t*t ) / t - v0;
								if ( step_vel0 > 0 )
								{
									float step_force_abs = step_vel0 / Time.fixedDeltaTime * 2f / 3f;
									Vector3 step_force = new Vector3( 0, step_force_abs, 0 );
									//body_rb.AddForce( step_force );
									body_rb.velocity = body_rb.velocity + new Vector3( 0, step_vel0, 0 );
									log( $"stepping {step_force} step_height: {step_height} v0: {v0} stepv0: {step_vel0} V: {V}" );
								} else
								{
									log( $"NOT stepping step_height: {step_height} v0: {v0} stepv0: {step_vel0} V: {V}" );
								}
							}// else
							{
								//log( $"not stepping t = {t}" );
							}
						}
					}
				}

				if ( force_initial_velocity && ! was_walking && is_walking && body_rb.velocity.sqrMagnitude < 1 )
				{
					body_rb.velocity += walk_dir * abs_vel;

					//abs_vel = Mathf.Max( abs_vel, min_walking_static_vel );
				}
				curr_vel_fw = Mathf.Max( 0f, Vector3.Dot( body_rb.velocity, walk_dir ) );
				movement_velocity = 0;
				if ( is_walking )
				{
					movement_velocity = Mathf.Max( 0f, abs_vel - curr_vel_fw );
				}
				Vector3 vel_dir = walk_dir;//( walk_dir + body_node.up * vel_dir_up_coefficient ).normalized;
				Vector3 movement_force = vel_dir * ( movement_velocity / Time.fixedDeltaTime );
				//body_rigidbody.velocity += walk_velocity;

				// NOTE(theGiallo): friction like force
				if ( is_grounded )
				{
					Friction friction = default;
					int ground_count = 0;
					for ( int i = 0; i != colliding_points.Count; ++i )
					{
						Colliding_Point cp = colliding_points[i];
						if ( ! cp.has_grounded_local_pos )
						{
							continue;
						}

						PhysicMaterial pm = cp.contact_point.otherCollider.material;
						friction.sum( feet_physicMaterial, pm );
						++ground_count;
					}
					friction.dyn /= (float)ground_count;
					friction.stc /= (float)ground_count;

						Vector3 rbv = body_rb.velocity;
						float rbv_m = body_rb.velocity.magnitude;
						Vector3 desired_v = rbv + movement_velocity * vel_dir;
						float desired_v_m = desired_v.magnitude;


					bool is_slipping =
					     static_friction_threshold( friction.stc, movement_velocity )
					  || static_friction_threshold( friction.stc, rbv_m )
					  || static_friction_threshold( friction.stc, desired_v_m );

					if ( is_slipping )
					{
						anti_gravity_now = false;
					}

					float fr = is_slipping ? friction.dyn : friction.stc;
					//log( $"fr = {fr} dyn = {friction.dyn} stc = {friction.stc}" );

					if ( is_walking )//&& is_grounded )
					{
						float ok_v_m = Vector3.Dot( walk_dir, rbv );
						ok_v_m = Mathf.Max( 0, ok_v_m );
						Vector3 ok_v = walk_dir * ok_v_m;

						Vector3 bad_v = rbv - ok_v;
						float bad_v_m = bad_v.magnitude;
						float friction_m = Mathf.Min( max_friction_force, bad_v_m );
						bad_v = bad_v_m == 0f ? Vector3.zero : bad_v * ( friction_m / bad_v_m );

						body_rb.AddForce( ( - fr * 2f / 3f / Time.fixedDeltaTime ) * bad_v );

						if ( ok_v_m > abs_vel )
						{
							float straight_friction = ( ok_v_m - abs_vel ) * straight_friction_factor;
							straight_friction = Mathf.Min( max_straight_friction_force, straight_friction );

							body_rb.AddForce( -walk_dir * straight_friction / Time.fixedDeltaTime * fr );
						}

						if ( is_slipping )
						{
							movement_velocity = slipping_velocity;
							movement_force = movement_force.normalized * movement_velocity / Time.fixedDeltaTime;
						}
					} else
					//if ( is_grounded )
					{
						Vector3 stopping_friction_vel;
						stopping_friction_vel = -rbv * fr;
						body_rb.AddForce( stopping_friction_vel / Time.fixedDeltaTime );
						//log( "stopping force" );
					}
				}

				if ( is_walking && as_grounded )
				{
					body_rb.AddForce( movement_force );
				}

				was_walking = is_walking;
				prev_walk_vel = curr_walk_velocity;
			}

			// NOTE(theGiallo): anti-gravity
			bool anti_gravity_grounded = anti_gravity_as_grounded ? as_grounded : is_grounded;
			if ( anti_gravity_now && anti_gravity && ( ( anti_gravity_grounded && is_walking ) || jumped ) )
			{
				body_rb.AddForce( - Physics.gravity * ( anti_gravity_coefficient / Time.fixedDeltaTime ) );
			}

			float xz_speed;
			{
				Vector3 rbv = body_rb.velocity;
				float ok_v_m = Vector3.Dot( body_tr.forward, rbv );
				Vector3 v = body_tr.forward * ok_v_m;
				Vector2 vxz = new Vector2( v.x, v.z );
				xz_speed = vxz.magnitude;
			}
			if ( jumped && xz_speed > air_prone_trigger_speed )//&& ! is_grounded )
			{
				stance = Stance.PRONE;
				height01_linear = 0;
				update_height();
				stance_activation = 0;
			}

			animator.SetBool( animator_id_grounded, jumped ? false : as_grounded );
			animator.SetBool( animator_id_really_grounded, jumped ? false : is_grounded );
			animator.SetFloat( animator_id_height, height );
			animator.SetInteger( animator_id_stance, (int)stance );
			animator.SetFloat( animator_id_walk_speed, abs_vel );
			animator.SetFloat( animator_id_xz_speed, xz_speed  );

			max_height = Mathf.Max( transform.localPosition.y, max_height );

			// NOTE(theGiallo): this will be updated by the OnCollision* that are called after the FixedUpdate
			is_grounded = false;
			colliding_points.Clear();
			//update_step_check_area( curr_walk_velocity );
		}

		private
		bool
		static_friction_threshold( float static_friction, float abs_vel )
		{
			bool ret = abs_vel / static_friction > static_friction_threshold_value;
			return ret;
		}

		private
		float
		standing_height_movement()
		{
			float ret = height * height_walk_speed_factor + height_walk_speed_min;
			return ret;
		}

		private
		float
		stun_after_land_ms( float vel )
		{
			float ret = Mathf.Max( 0, vel - 1f ) * 200f;
			ret = Mathf.Min( ret, 3000f );
			return ret;
		}
		private
		void
		apply_head_rot()
		{
			head_yaw_deg   = Mathf.Clamp( head_yaw_deg,  -head_yaw_max_abs_deg, head_yaw_max_abs_deg );
			head_pitch_deg = Mathf.Clamp( head_pitch_deg, head_pitch_min_deg,   head_pitch_max_deg );
			//head_pivot_node.localRotation = Quaternion.Euler( -head_pitch, head_yaw, 0 );
			head_pivot_tr.localRotation =
			   Quaternion.AngleAxis( head_yaw_deg, Vector3.up )
			 *
			   Quaternion.AngleAxis( -head_pitch_deg, Vector3.right )
			;
			neck_collider_tr.LookAt( head_tr );
		}
		private
		void
		update_height()
		{
			switch ( curve_type )
			{
				case Curve_Type.LERP:
					height = height01_linear;
					break;
				case Curve_Type.SMOOTHSTEP:
					height = smoothstep( height01_linear );
					break;
				case Curve_Type.SMOOTHERSTEP:
					height = smootherstep( height01_linear );
					break;
				case Curve_Type.SIN:
					height = Mathf.Sin( height01_linear * ( Mathf.PI * 0.5f ) );
					break;
				case Curve_Type.ANIMATION_CURVE:
					height = Mathf.Clamp( height_curve.Evaluate( height01_linear ), -1, 1 );
					break;
			}
		}
		private
		struct
		Colliding_Point
		{
			public Vector3        our_normal;
			public bool           has_our_normal;
			public Vector3        grounded_local_pos;
			public bool           has_grounded_local_pos;
			public ContactPoint   contact_point;
		}
		private ContactPoint[] collision_contact_points = new ContactPoint[64];
		private List<Colliding_Point> colliding_points = new List<Colliding_Point>();
		private
		void
		check_collision_for_grounding( Collision collision )
		{
			float gh = grounded_height + ground_zero.localPosition.y;
			int ccount = collision.contactCount;
			if ( ccount > collision_contact_points.Length )
			{
				collision_contact_points = new ContactPoint[ccount];
			}
			collision.GetContacts( collision_contact_points );
			for ( int i = 0; i != ccount; ++i )
			{
				ContactPoint cp = collision_contact_points[i];

				Colliding_Point colliding_point = new Colliding_Point();
				colliding_point.contact_point = cp;
				//log( $"{cp.normal} | {cp.normal.magnitude}" );
				Vector3 lp = transform.InverseTransformPoint( cp.point );
				colliding_point.grounded_local_pos = lp;
				colliding_point.has_grounded_local_pos = true;
				if ( cp.normal.y > 0 )
				{
					if ( lp.y < gh )
					{
						is_grounded = true;
					}
				}

				{
					Vector3 dir_towards_us = cp.normal;
					float abs_dp = Mathf.Max( 1e-3f, 16f * Mathf.Abs( cp.separation ) );
					Vector3 dp = -dir_towards_us * abs_dp;
					Vector3 p = cp.point + dp;
					Ray ray = new Ray( p, dir_towards_us );
					float max_d = Mathf.Max( 0.1f, abs_dp * 3f );
					bool did_hit = cp.thisCollider.Raycast( ray, out RaycastHit hit, max_d );
					if ( did_hit )
					{
						colliding_point.our_normal = hit.normal;
						colliding_point.has_our_normal = true;
					} else
					{
						log( $"abs_dp = {abs_dp} dirtus = ({dir_towards_us.x}, {dir_towards_us.y}, {dir_towards_us.z}) dp = ({dp.x}, {dp.y}, {dp.z}) p = ({p.x}, {p.y}, {p.z}) cp.separation = {cp.separation}" );
						log( $"pos: {transform.position.x}, {transform.position.y}, {transform.position.z}" );
						ILLEGAL_PATH( "collision normal ray didn't hit" );
					}
				}

				colliding_points.Add( colliding_point );

				if ( spawn_debug_arrows )
				{
					GameObject go = Instantiate( collision_marker_prefab, null, false );
					Quaternion r = Quaternion.FromToRotation( Vector3.up, cp.normal );
					go.transform.SetPositionAndRotation( cp.point, r );
				}
				//log( cp.point.ToString() );
			}
		}
		private void OnCollisionEnter( Collision collision )
		{
			check_collision_for_grounding( collision );
		}
		private void OnCollisionStay( Collision collision )
		{
			check_collision_for_grounding( collision );
		}
		private void OnCollisionExit( Collision collision )
		{
		}

		private bool capturing_mouse = true;
		private void update_lock_cursor()
		{
			Cursor.lockState = capturing_mouse ? CursorLockMode.Locked : CursorLockMode.None;
		}
		#endregion

		#region Unity
		private void Awake()
		{
			mapping = new Mapping();
			mapping.setup_mapping();

			animator_id_height     = Animator.StringToHash( "height"     );
			animator_id_xz_speed   = Animator.StringToHash( "xz_speed"   );
			animator_id_walk_speed = Animator.StringToHash( "walk_speed" );
			animator_id_stance     = Animator.StringToHash( "stance" );
			animator_id_grounded   = Animator.StringToHash( "grounded"   );
			animator_id_really_grounded = Animator.StringToHash( "really_grounded"   );
		}
		private void Start()
		{
			request_load_mapping();
			mapping.on_change += ( mapping, _ )=> request_save_mapping();

			movement_speed = normal_movement_speed;

			update_lock_cursor();
			Vector2 ref_res = new Vector2Int( 1920, 1080 );

			Resolution_Manager.register( new Resolution_Manager.Render_Camera_Target(){
				camera = camera_main,
				raw_image = camera_main_raw_image,
				rt = null,
				res_scale = Vector2.one
			} );
			Resolution_Manager.register( new Resolution_Manager.Render_Camera_Target(){
				camera = camera_3D_ui,
				raw_image = camera_3D_ui_raw_image,
				rt = null,
				res_scale = Vector2.one
			} );
			Vector2 raw_image_size = camera_3rd_person_raw_image.GetComponent<RectTransform>().rect.size;
			Resolution_Manager.register( new Resolution_Manager.Render_Camera_Target(){
				camera = camera_3rd_person,
				raw_image = camera_3rd_person_raw_image,
				rt = null,
				res_scale = raw_image_size / ref_res
			} );

			Resolution_Manager.set_max_resolution( new Vector2Int( 1920, 1080 ) );

			//update_step_check_area();

			prev_body_rot = body_tr.localRotation.eulerAngles;

			prone_to_crouch_timer = new Fixed_Timer( prone_to_crouch_timer_ms );
			crouch_to_prone_timer = new Fixed_Timer( crouch_to_prone_timer_ms );
			landing_timer         = new Fixed_Timer( 0 );
		}
		private void Update()
		{
			bool got_esc =// Keyboard.current.escapeKey.wasPressedThisFrame;
			   Game_Input.instance.merged_keyboards_per_frame.key_front_down( Game_Input.Index.escape );
			//foreach ( Game_Input.Keyboard_Status ks in Game_Input.instance.keyboards_per_frame.Values )
			//{
			//	got_esc = ks.keys_front_down[(int)Game_Input.Index.escape] == 1;
			//	if ( got_esc )
			//	{
			//		break;
			//	}
			//}
			if ( got_esc )
			{
				capturing_mouse = ! capturing_mouse;
				update_lock_cursor();
			}

			contrast_body_rot();

			Quaternion q = Quaternion.FromToRotation( Vector3.up, body_tr.forward );
			walk_dir_arrow.rotation = q;
		}
		private void FixedUpdate()
		{
			//log( $"FixedUpdate {FrameTimeManager.curr_fixu_end_time_ms}" );
			mapping.fixed_update();
			fixed_update_controls();
		}
		#endregion
	}
}
