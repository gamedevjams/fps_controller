﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;
using System;

namespace tg
{
	public class Mouse_Hand_Change_Overlay_GUI : MonoBehaviour
	{
		public TMP_Text title_text;
		public TMP_Text mouse_hand_text;

		public void set_hand( FP_Controller.Mouse_Hand mouse_hand )
		{
			mouse_hand_text.text = $"{mouse_hand}";
		}

		public
		void
		SetActive( bool active )
		{
			gameObject.SetActive( active );
		}

		#region private
		#endregion

		#region Unity
		private void Awake()
		{
		}
		private void Start()
		{
		}
		private void Update()
		{
		}
		#endregion
	}
}
