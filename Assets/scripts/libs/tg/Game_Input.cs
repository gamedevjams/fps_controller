﻿#define USE_PEEK_MESSAGE
//#define USE_GET_MESSAGE // NOTE(theGiallo): for some reason GetMessage returns always 0

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Runtime.InteropServices;

using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.HID;
using UnityEngine.InputSystem.Controls;
using UnityEngine.InputSystem.Utilities;

using EasyButtons;
using Pixeye.Unity;
#if TG_RAW_INPUT
using Linearstar.Windows.RawInput;
using Linearstar.Windows.RawInput.Native;
#endif

using static tg.tg;
using System.Threading;
using System.ComponentModel;

namespace tg
{
	public class Game_Input : MonoBehaviour
	{
		public static Game_Input instance;


		#if TG_RAW_INPUT
		#region WM_INPUT
		[DllImport( "user32.dll", SetLastError = true, EntryPoint = "PeekMessageW" )]
		private static extern int GetMessage( IntPtr lpMsg, // LPMSG
		                                      IntPtr hWnd,  // HWND
		                                      uint wMsgFilterMin,
		                                      uint wMsgFilterMax );


		[DllImport( "user32.dll", SetLastError = true, EntryPoint = "PeekMessageW" )]
		private static extern int PeekMessage( IntPtr lpMsg, // LPMSG
		                                       IntPtr hWnd,  // HWND
		                                       uint wMsgFilterMin,
		                                       uint wMsgFilterMax,
		                                       uint wRemoveMsg );

		[DllImport( "kernel32.dll", SetLastError = true, EntryPoint = "GetModuleHandleW" )]
		private static extern IntPtr GetModuleHandle( IntPtr lpModuleName );

		[DllImport( "user32.dll", SetLastError = true, EntryPoint = "CreateWindowExW", CharSet=CharSet.Ansi )]
		private static extern IntPtr CreateWindowEx( long   dwExStyle,
		                                             [MarshalAs(UnmanagedType.LPWStr)]
		                                             string lpClassName,  // LPCWSTR
		                                             [MarshalAs(UnmanagedType.LPWStr)]
		                                             string lpWindowName, // LPCWSTR
		                                             long   dwStyle,
		                                             int    X,
		                                             int    Y,
		                                             int    nWidth,
		                                             int    nHeight,
		                                             IntPtr hWndParent,   // HWND
		                                             IntPtr hMenu,        // HMENU
		                                             IntPtr hInstance,    // HINSTANCE
		                                             IntPtr lpParam       // LPVOID
		);

		[DllImport( "user32.dll", SetLastError = true, EntryPoint = "RegisterClassExW" )]
		private static extern IntPtr RegisterClassEx( IntPtr Arg1 );

		[DllImport( "user32.dll", SetLastError = true, EntryPoint = "UnregisterClassW" )]
		private static extern int UnregisterClass( [MarshalAs(UnmanagedType.LPWStr)]
		                                          string  lpClassName,  // LPCWSTR
		                                          IntPtr   hInstance ); // HINSTANCE

		[DllImport( "user32.dll", SetLastError = true, EntryPoint = "DefWindowProcW" )]
		private static extern IntPtr DefWindowProc( IntPtr hWnd,
		                                            uint   Msg,
		                                            IntPtr wParam,
		                                            IntPtr lParam);

		[DllImport( "user32.dll", SetLastError = true, EntryPoint = "CloseWindow" )]
		private static extern bool CloseWindow( IntPtr hWnd );

		[DllImport( "user32.dll", SetLastError = true, EntryPoint = "DestroyWindow" )]
		private static extern bool DestroyWindow( IntPtr hWnd );

		[DllImport( "user32.dll", SetLastError = true, EntryPoint = "TranslateMessage" )]
		private static extern bool TranslateMessage( IntPtr lpMsg );

		[DllImport( "user32.dll", SetLastError = true, EntryPoint = "DispatchMessage" )]
		private static extern IntPtr DispatchMessage( IntPtr lpMsg );

		// NOTE(theGiallo): https://social.msdn.microsoft.com/Forums/vstudio/en-US/b6c1cacc-3c84-4f84-82d0-d3f7558c1c0d/how-to-use-quotsetwindowlongquot-using-c?forum=csharpgeneral

		[DllImport( "user32.dll", SetLastError = true, EntryPoint = "SetWindowLongW" )]
		private static extern IntPtr SetWindowLongPtr32( IntPtr hWnd, int nIndex, IntPtr dwNewLong );

		[DllImport( "user32.dll", SetLastError = true, EntryPoint = "SetWindowLongPtrW" )]
		private static extern IntPtr SetWindowLongPtr64( IntPtr hWnd, int nIndex, IntPtr dwNewLong );

		[DllImport( "user32.dll", SetLastError = true, EntryPoint = "GetWindowLongW" )]
		private static extern IntPtr GetWindowLongPtr32( IntPtr hWnd, int nIndex );

		[DllImport( "user32.dll", SetLastError = true, EntryPoint = "GetWindowLongPtrW" )]
		private static extern IntPtr GetWindowLongPtr64( IntPtr hWnd, int nIndex );

		[DllImport( "kernel32.dll", SetLastError = true )]
		private static extern void SetLastError( int error );

		[DllImport( "kernel32.dll", SetLastError = true, EntryPoint = "ShowWindow" )]
		private static extern bool ShowWindow( IntPtr hWnd, int nCmdShow );

		[DllImport( "kernel32.dll", SetLastError = true, EntryPoint = "UpdateWindow" )]
		private static extern bool UpdateWindow( IntPtr hWnd );

		// Minimizes a window, even if the thread that owns the window is not responding. This flag should only be used when minimizing windows from a different thread.
		public static readonly int SW_FORCEMINIMIZE = 11;

		// Hides the window and activates another window.
		public static readonly int SW_HIDE = 0;

		// Maximizes the specified window.
		public static readonly int SW_MAXIMIZE = 3;

		// Minimizes the specified window and activates the next top-level window in the Z order.
		public static readonly int SW_MINIMIZE = 6;

		// Activates and displays the window. If the window is minimized or maximized, the system restores it to its original size and position. An application should specify this flag when restoring a minimized window.
		public static readonly int SW_RESTORE = 9;

		// Activates the window and displays it in its current size and position.
		public static readonly int SW_SHOW = 5;

		// Sets the show state based on the SW_ value specified in the STARTUPINFO structure passed to the CreateProcess function by the program that started the application.
		public static readonly int SW_SHOWDEFAULT = 10;

		// Activates the window and displays it as a maximized window.
		public static readonly int SW_SHOWMAXIMIZED = 3;

		// Activates the window and displays it as a minimized window.
		public static readonly int SW_SHOWMINIMIZED = 2;

		// Displays the window as a minimized window. This value is similar to SW_SHOWMINIMIZED, except the window is not activated.
		public static readonly int SW_SHOWMINNOACTIVE = 7;

		// Displays the window in its current size and position. This value is similar to SW_SHOW, except that the window is not activated.
		public static readonly int SW_SHOWNA = 8;

		// Displays a window in its most recent size and position. This value is similar to SW_SHOWNORMAL, except that the window is not activated.
		public static readonly int SW_SHOWNOACTIVATE = 4;

		// Activates and displays a window. If the window is minimized or maximized, the system restores it to its original size and position. An application should specify this flag when displaying the window for the first time.
		public static readonly int SW_SHOWNORMAL = 1;


		public static readonly uint PM_NOREMOVE = 0x0000; // Messages are not removed from the queue after processing by PeekMessage.
		public static readonly uint PM_REMOVE   = 0x0001; // Messages are removed from the queue after processing by PeekMessage.
		public static readonly uint PM_NOYIELD  = 0x0002; // Prevents the system from releasing any thread that is waiting for the caller to go idle (see WaitForInputIdle).  Combine this value with either PM_NOREMOVE or PM_REMOVE.u
		public static readonly uint QS_KEY            = 0x0001;
		public static readonly uint QS_MOUSEMOVE      = 0x0002;
		public static readonly uint QS_MOUSEBUTTON    = 0x0004;
		public static readonly uint QS_POSTMESSAGE    = 0x0008;
		public static readonly uint QS_TIMER          = 0x0010;
		public static readonly uint QS_PAINT          = 0x0020;
		public static readonly uint QS_SENDMESSAGE    = 0x0040;
		public static readonly uint QS_HOTKEY         = 0x0080;
		public static readonly uint QS_ALLPOSTMESSAGE = 0x0100;
		public static readonly uint QS_MOUSE          = QS_MOUSEMOVE | QS_MOUSEBUTTON;
		public static readonly uint QS_INPUT          = QS_MOUSE | QS_KEY;
		public static readonly uint QS_ALLEVENTS      = QS_INPUT | QS_POSTMESSAGE | QS_TIMER | QS_PAINT | QS_HOTKEY;
		public static readonly uint QS_ALLINPUT       = QS_INPUT | QS_POSTMESSAGE | QS_TIMER | QS_PAINT | QS_HOTKEY | QS_SENDMESSAGE;

		public static readonly uint PM_QS_INPUT       = QS_INPUT << 16;

		public static readonly long WS_EX_TRANSPARENT = 0x00000020L;
		public static readonly long WS_EX_LAYERED     = 0x00080000L;
		public static readonly long WS_EX_TOPMOST     = 0x00000008L;
		public static readonly long WS_POPUP          = 0x80000000L;
		public static readonly long WS_BORDER         = 0x00800000L;
		public static readonly long WS_SYSMENU        = 0x00080000L;
		public static readonly long WS_POPUPWINDOW    = (WS_POPUP | WS_BORDER | WS_SYSMENU);

		public static readonly IntPtr HWND_MESSAGE = new IntPtr(-3);

		public static readonly uint CS_HREDRAW = 0x0002;
		public static readonly uint CS_VREDRAW = 0x0001;

		public static readonly uint WM_QUIT = 0x0012;
		// NOTE(theGiallo): https://docs.microsoft.com/en-us/windows/win32/inputdev/wm-input
		public static readonly uint WM_INPUT = 0x00FF;

		// NOTE(theGiallo): https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-setwindowlonga
		// Sets a new address for the window procedure.
		// You cannot change this attribute if the window does not belong to the same process as the calling thread.
		public static readonly int  GWL_WNDPROC = -4;

		//[FieldOffset(0)]
		//[StructLayout( LayoutKind.Sequential, Pack = 1 )]
		[StructLayout(LayoutKind.Explicit, Size = 48)]
		private struct MSG
		{
			[StructLayout(LayoutKind.Explicit)]
			//[StructLayout( LayoutKind.Sequential, Pack = 1 )]
			public struct POINT
			{
				[FieldOffset(0)]
				uint x;
				[FieldOffset(8)]
				uint y;
			}
			[FieldOffset(0)]
			public IntPtr hWnd;
			[FieldOffset(8)]
			public uint   message;
			[FieldOffset(16)] // NOTE(theGiallo): MOTHERFUCKING PADDING
			public IntPtr wParam;
			[FieldOffset(24)]
			public IntPtr lParam;
			[FieldOffset(32)]
			public uint   time;
			[FieldOffset(36)]
			public POINT  pt;
			[FieldOffset(44)]
			public uint   lPrivate;
		}

		[StructLayout(LayoutKind.Sequential, Pack = 1, CharSet=CharSet.Ansi )]
		private struct WNDCLASSEXA
		{
			public uint   cbSize;
			public uint   style;
			public IntPtr lpfnWndProc;
			public int    cbClsExtra;
			public int    cbWndExtra;
			public IntPtr hInstance;
			public IntPtr hIcon;
			public IntPtr hCursor;
			public IntPtr hbrBackground;
			[MarshalAs(UnmanagedType.LPWStr)]
			public string lpszMenuName;  // LPCWSTR
			[MarshalAs(UnmanagedType.LPWStr)]
			public string lpszClassName; // LPCWSTR
			public IntPtr hIconSm;
		}
		[StructLayout(LayoutKind.Sequential, Pack = 1, CharSet=CharSet.Ansi )]
		private struct WNDCLASSEXW
		{
			public uint   cbSize;
			public uint   style;
			public IntPtr lpfnWndProc;
			public int    cbClsExtra;
			public int    cbWndExtra;
			public IntPtr hInstance;
			public IntPtr hIcon;
			public IntPtr hCursor;
			public IntPtr hbrBackground;
			[MarshalAs(UnmanagedType.LPWStr)]
			public string lpszMenuName;  // LPCWSTR
			[MarshalAs(UnmanagedType.LPWStr)]
			public string lpszClassName; // LPCWSTR
			public IntPtr hIconSm;
		}

		private
		IntPtr
		wndProc( IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam )
		{
			WindowHandleGrabber.Window_Event we = new WindowHandleGrabber.Window_Event()
			{
				hWnd = hWnd,
				msg = msg,// & 0xffff,
				lParam = lParam,
				wParam = wParam,
				processed = new bool[]{ false }
			};
			window_event_handle( this, we );
			return DefWindowProc( hWnd, msg, wParam, lParam );
		}

		private bool input_thread_run = true;
		private Thread input_messages_handler_thread;

		private IntPtr hWnd;
		unsafe
		private
		void
		input_messages_handler_th()
		{

			WndProcDelegate wnd_proc = new WndProcDelegate( wndProc );
			IntPtr wnd_proc_ptr = Marshal.GetFunctionPointerForDelegate( wnd_proc );

			// https://guidedhacking.com/threads/transparent-window-for-esp.13689/
			string class_name = "TransparentC";
			WNDCLASSEXW wc = default;
			wc.cbSize = (uint)Marshal.SizeOf<WNDCLASSEXW>();
			wc.style = CS_HREDRAW | CS_VREDRAW;
			wc.lpfnWndProc = wnd_proc_ptr;
			wc.cbClsExtra = 0;
			wc.cbWndExtra = 0;
			wc.hInstance = GetModuleHandle( (IntPtr)0 );
			wc.hIcon = (IntPtr)0;
			wc.hCursor = (IntPtr)0;
			wc.hbrBackground = (IntPtr)0;
			wc.lpszClassName = class_name;
			wc.lpszMenuName = "";
			wc.hIconSm = (IntPtr)0;

			log( $"wc.hInstance = {wc.hInstance}" );

			IntPtr wc_ptr = Marshal.AllocHGlobal( Marshal.SizeOf<WNDCLASSEXW>() );
			Marshal.StructureToPtr( wc, wc_ptr, false );
			SetLastError( 0 );
			IntPtr reg_res = RegisterClassEx( wc_ptr );
			if ( (int)reg_res == 0 )
			{
				int err = Marshal.GetLastWin32Error();
				log_err( $"failed to register class: {err} {new Win32Exception(err).Message}" );
				return;
			}
			log( $"reg_res = {reg_res}" );

			//GetWindowRect( gameWindow, &gameRect );

			//int x = gameRect.left;
			//int y = gameRect.top;
			//int width = gameRect.right - gameRect.left;
			//int height = gameRect.bottom - gameRect.top;

			SetLastError( 0 );
			#if false
			int x = 1920 / 2;
			int y = 1080 / 2;
			int width = 800;
			int height = 600;
			hWnd = CreateWindowEx( WS_EX_TRANSPARENT | WS_EX_LAYERED | WS_EX_TOPMOST,
			                       wc.lpszClassName,
			                       "Transparent Window",
			                       WS_POPUP,//WINDOW,
			                       x,
			                       y,
			                       width,
			                       height,
			                       (IntPtr)0,
			                       (IntPtr)0,
			                       wc.hInstance,
			                       (IntPtr)0 );
			#else
			hWnd = CreateWindowEx( 0,
			                       wc.lpszClassName,
			                       "WM_INPUT Unity Window",
			                       0,//WS_POPUP,//WINDOW,
			                       0,
			                       0,
			                       0,
			                       0,
			                       HWND_MESSAGE,//(IntPtr)0,
			                       (IntPtr)0,
			                       (IntPtr)0,//wc.hInstance,
			                       (IntPtr)0 );
			#endif
			if ( (int)hWnd == 0 )
			{
				int err = Marshal.GetLastWin32Error();
				log_err( $"failed to create window: {err} {new Win32Exception(err).Message}" );
				goto class_closure;
			}

			log( $"hWnd = {hWnd}" );

			IntPtr old_wnd_proc_ptr;
			SetLastError( 0 );
			if ( IntPtr.Size == 4 )
			{
				old_wnd_proc_ptr = SetWindowLongPtr32( hWnd, GWL_WNDPROC, wnd_proc_ptr );
			} else
			{
				old_wnd_proc_ptr = SetWindowLongPtr64( hWnd, GWL_WNDPROC, wnd_proc_ptr );
			}
			if ( old_wnd_proc_ptr != wnd_proc_ptr )
			{
				log( "ptr changed" );
			}
			if ( (int)old_wnd_proc_ptr == 0 )
			{
				int err = Marshal.GetLastWin32Error();
				log_err( $"failed to register window procedure {err} {new Win32Exception(err).Message}" );
			}

			register_device_background_input( hWnd );

			//GetWindowRect( hWnd, &overlayRect );

			//SetWindowLong( hWnd, GWL_STYLE, GetWindowLong( hWnd, GWL_STYLE ) & ~( WS_BORDER | WS_THICKFRAME | WS_MAXIMIZE | WS_MINIMIZE ) );
			//SetWindowLong( hWnd, GWL_EXSTYLE, GetWindowLong( hWnd, GWL_EXSTYLE ) );
			//SetWindowPos( hWnd, NULL, 0, 0, 0, 0, SWP_NOACTIVATE | SWP_NOSIZE | SWP_NOMOVE | SWP_NOOWNERZORDER );

			//SetLayeredWindowAttributes( hWnd, RGB( 255, 255, 255 ), 255, 0x01 );

			ShowWindow( hWnd, SW_SHOW );
			UpdateWindow( hWnd );


			MSG msg = default;
			//IntPtr msg_ptr = Marshal.AllocHGlobal( Marshal.SizeOf( msg ) );
			//Marshal.StructureToPtr( msg, msg_ptr, false );
			while ( input_thread_run )
			{
				if ( did_unregister && has_focus )
				{
					log( "REGISTERING" );
					did_unregister = false;
					register_device_background_input( hWnd );
				} else
				if ( ! did_unregister && ! has_focus )
				{
					log( "UNREGISTERING" );
					did_unregister = true;
					RawInputDevice.UnregisterDevice( HidUsageAndPage.Mouse );
					RawInputDevice.UnregisterDevice( HidUsageAndPage.Keyboard );
				}

				#if USE_PEEK_MESSAGE
				MSG * msg_star = &msg;
				IntPtr msg_ptr = (IntPtr)msg_star;
				int res = PeekMessage( msg_ptr,
				                       hWnd,
				                       WM_INPUT,
				                       WM_INPUT,
				                       PM_REMOVE | PM_QS_INPUT );
				                       //PM_NOYIELD | PM_QS_INPUT );
				                       //PM_NOREMOVE | PM_QS_INPUT );
				                       //PM_NOREMOVE | PM_NOYIELD | PM_QS_INPUT );
				#elif USE_GET_MESSAGE
				MSG * msg_star = &msg;
				IntPtr msg_ptr = (IntPtr)msg_star;
				int res = GetMessage( msg_ptr,
				                      //(IntPtr)0,//hWnd,
				                      hWnd,
				                      WM_INPUT,
				                      WM_INPUT );
				#endif
				if ( res < 0 )
				{
					int err = Marshal.GetLastWin32Error();
					log_err( $"{{Peek/Get}}Message failed: {err} {new Win32Exception(err).Message}" );
					break;
				} else
				if ( res == 0 )
				{
					#if USE_PEEK_MESSAGE
					// PEEKMESSAGE
					//log( $"no messages: did_unregister {did_unregister} has_focus {has_focus}" );
					continue;
					#elif USE_GET_MESSAGE
					// GETMESSAGE
					log( "got QUIT message, exiting from input messages handler thread" );
					break;
					#endif
				} else
				{
					//log( $"got message res: {res} msg {msg.message}" );
				}

				if ( msg.message == WM_QUIT )
				{
					break;
				}

				if ( TranslateMessage( (IntPtr)(void*)&msg ) )
				{
					DispatchMessage( (IntPtr)(void*)&msg );
				} else
				{
					int err = Marshal.GetLastWin32Error();
					string error_str = new Win32Exception(err).Message;
					//log_err( $"failed to translate msg {msg.message} {err} {error_str}" );
				}

				WindowHandleGrabber.Window_Event we = new WindowHandleGrabber.Window_Event()
				{
					hWnd = msg.hWnd,
					msg = msg.message,// & 0xffff,
					lParam = msg.lParam,
					wParam = msg.wParam,
					processed = new bool[]{ false }
				};
				window_event_handle( this, we );
				if ( ! we.processed[0] )
				{
					DefWindowProc( hWnd, msg.message, msg.wParam, msg.lParam );
				}
			}

			if ( ! DestroyWindow( hWnd ) )
			{
				int err = Marshal.GetLastWin32Error();
				log_err( $"failed to close window: {err} {new Win32Exception(err).Message}" );
			}
			class_closure:
			bool retry = false;
			do
			{
				retry = false;
				int ires = UnregisterClass( wc.lpszClassName, wc.hInstance );
				if ( ires == 0 )
				{
					int err = Marshal.GetLastWin32Error();
					if ( err == 1412 )
					{
						retry = true;
					}
					log_err( $"failed to unregister class: {err} {new Win32Exception(err).Message}" );
				}
			} while ( retry );
			Marshal.FreeHGlobal( wc_ptr );
			//Marshal.FreeHGlobal( msg_ptr );
		}

		private
		void
		register_device_background_input( IntPtr hWnd )
		{
			if ( (int)hWnd == 0 ) return;

			RawInputDevice.RegisterDevice( new RawInputDeviceRegistration( HidUsageAndPage.Keyboard,
			                                                               //RawInputDeviceFlags.ExInputSink
			                                                               RawInputDeviceFlags.InputSink
			                                                             //| RawInputDeviceFlags.NoLegacy
			                                                               , hWnd ),
			                               new RawInputDeviceRegistration( HidUsageAndPage.Mouse,
			                                                               //RawInputDeviceFlags.ExInputSink
			                                                               RawInputDeviceFlags.InputSink
			                                                             //| RawInputDeviceFlags.NoLegacy
			                                                               , hWnd ) );
		}

		private
		void
		window_event_handle( object caller, WindowHandleGrabber.Window_Event we )
		{
			// NOTE(theGiallo): https://docs.microsoft.com/en-us/windows/win32/inputdev/wm-input
			const uint WM_INPUT = 0x00FF;

			try {
			if ( we.msg == WM_INPUT )
			{

				bool[] p = (bool[])we.processed;
				p[0] = true;

				//log( $"we.hWnd   = {(long)we.hWnd}" );
				//log( $"we.wParam = {(long)we.wParam}" );
				//log( $"we.lParam = {(long)we.lParam}" );
				RawInputData data = RawInputData.FromHandle( we.lParam );

				// You can identify the source device using Header.DeviceHandle or just Device.
				var sourceDeviceHandle = data.Header.DeviceHandle;
				var sourceDevice = data.Device;

				#if false
				// The data will be an instance of either RawInputMouseData, RawInputKeyboardData, or RawInputHidData.
				// They contain the raw input data in their properties.
				switch ( data )
				{
					case RawInputMouseData mouse:
						//log( mouse.Mouse.ToString() );
						break;
					case RawInputKeyboardData keyboard:
						log( keyboard.Keyboard.ToString() );
						break;
					case RawInputHidData hid:
						log( hid.Hid.ToString() );
						break;
				}
				#endif

				if ( data.Device is RawInputKeyboard )
				{
					RawInputKeyboard raw_k = data.Device as RawInputKeyboard;
					byte[] preparsed_data = raw_k.GetPreparsedData();
					RawInputKeyboardData kd = data as RawInputKeyboardData;
					#if false
					log( $"DevicePath: {raw_k.DevicePath}\n"
					   + $"DeviceType: {raw_k.DeviceType}"
					   + $"ManufacturerName: {raw_k.ManufacturerName}\n"
					   + $"ProductName: {raw_k.ProductName}\n"
					   + $"VendorId: {raw_k.VendorId}\n"
					   + $"ProductId: {raw_k.ProductId}\n"
					   + $"IsConnected: {raw_k.IsConnected}\n"
					   + $"FunctionKeyCount: {raw_k.FunctionKeyCount}\n"
					   + $"IndicatorCount: {raw_k.IndicatorCount}\n"
					   + $"TotalKeyCount: {raw_k.TotalKeyCount}\n"
					   + $"KeyboardMode: {raw_k.KeyboardMode}\n"
					   + $"KeyboardType: {raw_k.KeyboardType}\n"
					   + $"KeyboardSubType: {raw_k.KeyboardSubType}\n"
					   + $"preparsed data: {(preparsed_data == null ? "NONE": BitConverter.ToString( preparsed_data ))}"
					   );
					#endif
					//log( $"{kd.Keyboard}" );
					//int index = Keyboard_Status.index_from_int_and_flags( kd.Keyboard.ScanCode, kd.Keyboard.Flags );
					//int sc = Keyboard_Status.scancode_from_index( index, out Opt<RawKeyboardFlags> flags );
					//Scancode scancode = Keyboard_Status.scancode_from_index( index );
					//Scancode orig_scancode = Keyboard_Status.scancode_from_int_and_flags( kd.Keyboard.ScanCode, kd.Keyboard.Flags );
					////log( $"int scancode:{kd.Keyboard.ScanCode} index:{index} sc:{sc} {(Scancode)sc} flags:{flags} orig.scancode:{orig_scancode} scancode:{scancode}" );
					//debug_assert( (int)sc == kd.Keyboard.ScanCode && ( !flags.has_value || has_flags( kd.Keyboard.Flags, flags.value ) ) );

					//bool up   = has_flags( kd.Keyboard.Flags, RawKeyboardFlags.Up );
					//if ( up ) log( "got up" );
					//debug_assert( ! up || has_flags( e.keyboard.Flags, RawKeyboardFlags.Up ) );

					Raw_Input_Event e = new Raw_Input_Event( kd.Keyboard, raw_k.DevicePath, MonotonicTimestamp.Now() );
					input_events.Enqueue( e );
				} else
				if ( data.Device is RawInputMouse )
				{
					RawInputMouseData mouse = (RawInputMouseData) data;
					RawInputMouse raw_mouse = data.Device as RawInputMouse;
					byte[] preparsed_data = raw_mouse.GetPreparsedData();
					RawInputMouseData md = data as RawInputMouseData;
					#if false
					log( $"DevicePath: {raw_mouse.DevicePath}\n"
					   + $"DeviceType: {raw_mouse.DeviceType}"
					   + $"ManufacturerName: {raw_mouse.ManufacturerName}\n"
					   + $"ProductName: {raw_mouse.ProductName}\n"
					   + $"Id: {raw_mouse.Id}\n"
					   + $"VendorId: {raw_mouse.VendorId}\n"
					   + $"ProductId: {raw_mouse.ProductId}\n"
					   + $"SampleRate: {raw_mouse.SampleRate}\n"
					   + $"IsConnected: {raw_mouse.IsConnected}\n"
					   + $"HasHorizontalWheel: {raw_mouse.HasHorizontalWheel}\n"
					   + $"ButtonCount: {raw_mouse.ButtonCount}\n"
					   + $"preparsed data: {(preparsed_data == null ? "NONE": BitConverter.ToString( preparsed_data ))}"
					);
					log( $"{md.Mouse}" );
					#endif

					//log( $"mouse: {raw_mouse.DevicePath} {mouse.Mouse}" );
					Raw_Input_Event e = new Raw_Input_Event( mouse.Mouse, raw_mouse.DevicePath, MonotonicTimestamp.Now() );
					//log( $"e.mouse.LastX = {e.mouse.LastX}  mouse.Mouse.LastX = {mouse.Mouse.LastX}" );
					input_events.Enqueue( e );

					if ( ! devices_infos.ContainsKey( e.device_path ) )
					{
						add_device_infos( data.Device, e.device_path );
					}
				}
			}
			} catch ( Exception e )
			{
				log_err( e.Message );
				we.processed[0] = false;
			}
		}


		#endregion
		#endif

		public enum Axis
		{
			LEFT_STICK,
			RIGHT_STICK,
			LEFT_TRIGGER,
			RIGHT_TRIGGER,
		}
		public enum Gamepad_Button
		{
			A,
			B,
			X,
			Y,
			DPAD_UP,
			DPAD_DOWN,
			DPAD_LEFT,
			DPAD_RIGHT,
			L1,
			R1,
			L2,
			R2,
			L3,
			R3,
			L_STICK_UP,
			L_STICK_DOWN,
			L_STICK_RIGHT,
			L_STICK_LEFT,
			L_STICK_UP_LEFT,
			L_STICK_UP_RIGHT,
			L_STICK_DOWN_RIGHT,
			L_STICK_DOWN_LEFT,
			R_STICK_UP,
			R_STICK_DOWN,
			R_STICK_RIGHT,
			R_STICK_LEFT,
			R_STICK_UP_LEFT,
			R_STICK_UP_RIGHT,
			R_STICK_DOWN_RIGHT,
			R_STICK_DOWN_LEFT,
			START,
			SELECT,
			// ---
			__COUNT,
		}
		public enum Mouse_Button
		{
			LEFT,
			RIGHT,
			MIDDLE,
			BACK,
			FORWARD,
			SCROLL_WHEEL_X_UP,
			SCROLL_WHEEL_X_DOWN,
			SCROLL_WHEEL_Y_UP,
			SCROLL_WHEEL_Y_DOWN,
		}
		[Serializable]
		public struct Input_Source : IEquatable<Input_Source>, IComparable<Input_Source>
		{
			public enum Type
			{
				NONE,
				KEY,
				MOUSE_BUTTON,
				GAMEPAD_BUTTON,
				AXIS,
				COMBINED,
				OPTIONS,
			}
			public enum Input_System
			{
				NONE,
				UNITY,
				RAW
			}

			[Serializable]
			public struct
			Status
			{
				public bool active;
				public bool just_activated;
				public bool just_deactivated;
				public Vector2 value;
			}

			public static Input_Source Key( Key key_code )
			{
				Input_Source ret = new Input_Source();
				ret._type = Type.KEY;
				ret._input_system = Input_System.UNITY;
				ret._key_code = key_code;
				return ret;
			}
			public static Input_Source Mouse_Button( Mouse_Button mouse_button)
			{
				Input_Source ret = new Input_Source();
				ret._type = Type.MOUSE_BUTTON;
				ret._input_system = Input_System.UNITY;
				ret._mouse_button = mouse_button;
				return ret;
			}
			public static Input_Source Gamepad_Button( Gamepad_Button gamepad_button, int gamepad_device_id = -1 )
			{
				Input_Source ret = new Input_Source();
				ret._type = Type.GAMEPAD_BUTTON;
				ret._input_system = Input_System.UNITY;
				ret._gamepad_button    = gamepad_button;
				ret._gamepad_device_id = gamepad_device_id;
				return ret;
			}
			public static Input_Source Axis( Axis axis )
			{
				Input_Source ret = new Input_Source();
				ret._type = Type.AXIS;
				ret._input_system = Input_System.UNITY;
				ret._axis =  axis;
				return ret;
			}
			public static Input_Source Combined( params Input_Source[] combined )
			{
				Input_Source ret = new Input_Source();
				ret._type = Type.COMBINED;
				ret._input_system = Input_System.NONE;
				Array.Sort( combined );
				ret._combined =  combined;
				return ret;
			}
			#if TG_RAW_INPUT
			public static Input_Source Key_Raw( Index scancode_index, Device_Path device_path )
			{
				Input_Source ret    = new Input_Source();
				ret._type           = Type.KEY;
				ret._input_system   = Input_System.RAW;
				ret._device_path    = device_path;
				ret._scancode_index = scancode_index;
				return ret;
			}
			public static Input_Source Mouse_Button_Raw( Mouse_Button mouse_button, Device_Path device_path )
			{
				Input_Source ret = new Input_Source();
				ret._type         = Type.MOUSE_BUTTON;
				ret._input_system = Input_System.RAW;
				ret._device_path  = device_path;
				ret._mouse_button = mouse_button;
				return ret;
			}
			#endif
			public static Input_Source Options( params Input_Source[] options )
			{
				Input_Source ret = new Input_Source();
				ret._type = Type.OPTIONS;
				ret._input_system = Input_System.NONE;
				Array.Sort( options );
				ret._options = options;
				return ret;
			}


			public Type type => _type;

			public Key            key_code           { get { debug_assert( _input_system != Input_System.NONE && ( type == Type.KEY            ) ); return _key_code; } }
			public Mouse_Button   mouse_button       { get { debug_assert( _input_system != Input_System.NONE && ( type == Type.MOUSE_BUTTON   ) ); return _mouse_button; } }
			public Axis           axis               { get { debug_assert( _input_system == Input_System.UNITY && ( type == Type.AXIS           ) ); return _axis; } }
			public Gamepad_Button gamepad_button     { get { debug_assert( _input_system == Input_System.UNITY && ( type == Type.GAMEPAD_BUTTON ) ); return _gamepad_button; } }
			public int            gamepad_device_id  { get { debug_assert( _input_system == Input_System.UNITY && ( type == Type.GAMEPAD_BUTTON || type   == Type.AXIS ) ); return _gamepad_device_id; } }
			public Input_Source[] combined           { get { debug_assert( _input_system == Input_System.NONE  && ( type == Type.COMBINED       ) ); return _combined; } }
			public Input_Source[] options            { get { debug_assert( _input_system == Input_System.NONE  && ( type == Type.OPTIONS       ) ); return _options; } }
			public Input_System   input_system       { get { debug_assert( _input_system == Input_System.UNITY || ( type == Type.KEY || type == Type.MOUSE_BUTTON ) ); return (Input_System)_input_system; } }
			#if TG_RAW_INPUT
			public Index          scancode_index     { get { debug_assert( _input_system == Input_System.RAW && ( type == Type.KEY ) ); return _scancode_index; } }
			#endif
			public Device_Path    device_path        { get { debug_assert( _input_system == Input_System.RAW && ( type == Type.KEY || type == Type.MOUSE_BUTTON ) ); return _device_path; } }

			public
			bool
			collides( Input_Source source )
			{
				bool ret = false;

				if ( type == Type.OPTIONS || source.type == Type.OPTIONS )
				{
					if ( type == Type.OPTIONS )
					{
						for ( int i = 0; i != _options.Length && ! ret; ++i )
						{
							ret = options[i].collides( source );
						}
					} else
					{
						for ( int i = 0; i != source._options.Length && ! ret; ++i )
						{
							ret = collides( source._options[i] );
						}
					}
				} else
				if ( type == source.type
				  && _input_system == source.input_system )
				{
					if ( _input_system == Input_System.UNITY
					  || ( _input_system == Input_System.RAW
					    && (        _device_path.chars_length == 0
					      || source._device_path.chars_length == 0
					      || _device_path == source._device_path ) ) )
					{
						switch ( type )
						{
							case Type.AXIS:
								ret = _axis == source._axis
								   && (        _gamepad_device_id < 0
								     || source._gamepad_device_id < 0
								     || _gamepad_device_id == source._gamepad_device_id );

								break;
							case Type.GAMEPAD_BUTTON:
								ret = _gamepad_button == source._gamepad_button
								   && (        _gamepad_device_id < 0
								     || source._gamepad_device_id < 0
								     || _gamepad_device_id == source._gamepad_device_id );
								break;
							case Type.KEY:
								ret = _input_system == Input_System.UNITY
								    ? _key_code == source._key_code
								#if TG_RAW_INPUT
								    : _scancode_index == source._scancode_index;
								#else
									: false;
								#endif
								break;
							case Type.MOUSE_BUTTON:
								ret = _mouse_button == source._mouse_button;
								break;
							case Type.COMBINED:
								ILLEGAL_PATH();
								break;
						}
					} else
					if ( type == Type.COMBINED && _combined.Length == source._combined.Length )
					{
						ret = true;
						for ( int i = 0; i != _combined.Length && ret; ++i )
						{
							ret = ret && _combined[i].collides( source._combined[i] );
						}
					}
				}

				return ret;
			}

			public override string ToString()
			{
				string ret = $"{type} {_input_system} ";
				switch ( type )
				{
					case Type.NONE:
						break;
					case Type.KEY:
						if ( _input_system == Input_System.UNITY )
						{
							ret += $"key: {_key_code}";
						} else
						{
						#if TG_RAW_INPUT
							ret += $"scancode: {_scancode_index}";
						#endif
						}
						break;
					case Type.MOUSE_BUTTON:
						ret += $"mouse btn: {_mouse_button}";
						break;
					case Type.AXIS:
						ret += $"axis: {axis} {_gamepad_device_id}";
						break;
					case Type.GAMEPAD_BUTTON:
						ret += $"gamepad btn: {_gamepad_button}";
						break;
					case Type.COMBINED:
						for ( int i = 0, l = _combined.Length; i != l; ++i )
						{
							ret += $" [{_combined[i]}]{( i != l - 1 ? " +" : "" )}";
						}
						break;
				}
				if ( type == Type.AXIS || type == Type.GAMEPAD_BUTTON )
				{
					if ( gamepad_device_id >= 0 )
					{
						ret += $" device {gamepad_device_id}";
					}
				}

				if ( _input_system == Input_System.RAW )
				{
					ret += $" device path: {_device_path}";
				}

				return ret;
			}

			[SerializeField]
			private Type _type;
			public  Key            _key_code;
			public  Mouse_Button   _mouse_button;
			public  Axis           _axis;
			public  Gamepad_Button _gamepad_button;
			public  int            _gamepad_device_id;
			public  Input_Source[] _combined;
			public  Input_Source[] _options;
			public  Input_System   _input_system;
			public  Device_Path    _device_path;
			#if TG_RAW_INPUT
			public  Index          _scancode_index;
			#endif

			public
			Status
			get_status()
			{
				Status ret = new Status();

				if ( type == Input_Source.Type.NONE
				  || input_system == Input_Source.Input_System.NONE )
				{
					return ret;
				}
				if ( input_system == Input_Source.Input_System.UNITY )
				{
					switch ( type )
					{
						case Input_Source.Type.MOUSE_BUTTON:
							ButtonControl bc = null;
							switch ( mouse_button )
							{
								case Game_Input.Mouse_Button.LEFT:
									bc = Mouse.current.leftButton;
									break;
								case Game_Input.Mouse_Button.RIGHT:
									bc = Mouse.current.rightButton;
									break;
								case Game_Input.Mouse_Button.MIDDLE:
									bc = Mouse.current.middleButton;
									break;
								case Game_Input.Mouse_Button.FORWARD:
									bc = Mouse.current.forwardButton;
									break;
								case Game_Input.Mouse_Button.BACK:
									bc = Mouse.current.backButton;
									break;
							}
							ret.just_activated   = bc.wasPressedThisFrame && FrameTimeManager.first_fixed_time_update_of_frame;
							ret.active           = bc.isPressed           ;
							ret.just_deactivated = bc.wasReleasedThisFrame && FrameTimeManager.first_fixed_time_update_of_frame;
							break;
						case Input_Source.Type.KEY:
							ret.just_activated   = Keyboard.current[key_code].wasPressedThisFrame  && FrameTimeManager.first_fixed_time_update_of_frame;
							ret.active           = Keyboard.current[key_code].isPressed           ;
							ret.just_deactivated = Keyboard.current[key_code].wasReleasedThisFrame && FrameTimeManager.first_fixed_time_update_of_frame;
							break;
						case Input_Source.Type.AXIS:
						case Input_Source.Type.GAMEPAD_BUTTON:
							Gamepad g = gamepad_device_id < 0 ? Gamepad.current : Gamepad.all[gamepad_device_id];
							if ( Game_Input.instance.gamepads.TryGetValue( gamepad_device_id, out Gamepad_Status gamepad_status ) )
							{
								if ( type == Type.AXIS )
								switch ( axis )
								{
									case Game_Input.Axis.LEFT_STICK:
										ret.value = gamepad_status.sticks_value[0];
										break;
									case Game_Input.Axis.RIGHT_STICK:
										ret.value = gamepad_status.sticks_value[1];
										break;
									case Game_Input.Axis.LEFT_TRIGGER:
										ret.value.x = gamepad_status.triggers_value[0];
										break;
									case Game_Input.Axis.RIGHT_TRIGGER:
										ret.value.x = gamepad_status.triggers_value[1];
										break;
								} else
								if ( type == Type.GAMEPAD_BUTTON )
								{
									ret.just_activated   = gamepad_status.buttons_just_activated  [(int)gamepad_button] == 1 && FrameTimeManager.first_fixed_time_update_of_frame;
									ret.active           = gamepad_status.buttons_active          [(int)gamepad_button] == 1;
									ret.just_deactivated = gamepad_status.buttons_just_deactivated[(int)gamepad_button] == 1 && FrameTimeManager.first_fixed_time_update_of_frame;
								}
							}
							break;
					}
				}
				#if TG_RAW_INPUT
				else
				if ( input_system == Input_System.RAW )
				{
					switch ( type )
					{
						case Input_Source.Type.MOUSE_BUTTON:
						{
							if ( Game_Input.instance.mouses.TryGetValue( device_path, out Mouse_Status value ) )
							{
								ret.active           = value.buttons_status_down[(int)mouse_button] == 1;
								ret.just_activated   = value.buttons_front_down [(int)mouse_button] == 1;
								ret.just_deactivated = value.buttons_front_up   [(int)mouse_button] == 1;
							}
						}
							break;
						case Input_Source.Type.KEY:
						{
							if ( Game_Input.instance.keyboards.TryGetValue( device_path, out Keyboard_Status value ) )
							{
								ret.active           = value.keys_status_down[(int)scancode_index] == 1;
								ret.just_activated   = value.keys_front_down [(int)scancode_index] == 1;
								ret.just_deactivated = value.keys_front_up   [(int)scancode_index] == 1;
							}
						}
							break;
						default:
							ILLEGAL_PATH();
							break;
					}
				}
				#endif
				return ret;
			}

			#region csharp
			public override bool Equals( object obj )
			{
				return obj is Input_Source && Equals( (Input_Source)obj );
			}
			public bool Equals( Input_Source other )
			{
				bool ret = _type == other._type && _input_system == other._input_system;
				if ( ret )
				{
					if ( _input_system == Input_System.RAW )
					{
						ret = ret && other._device_path == _device_path;
					}
					switch ( _type )
					{
						case Type.KEY:
							if ( _input_system == Input_System.UNITY )
							{
								ret = _key_code == other._key_code;
							} else
							{
							#if TG_RAW_INPUT
								ret = scancode_index == other._scancode_index;
							#endif
							}
							break;
						case Type.MOUSE_BUTTON:
							ret = _mouse_button == other._mouse_button;
							break;
						case Type.AXIS:
							ret = _axis == other._axis;
							break;
						case Type.GAMEPAD_BUTTON:
							ret = _gamepad_button == other._gamepad_button && _gamepad_device_id == other._gamepad_device_id;
							break;
						case Type.COMBINED:
						{
							ret = _combined.Length == other._combined.Length;
							for ( int i = 0; i != _combined.Length && ret; ++i )
							{
								ret = ret && _combined[i] == other._combined[i];
							}
						}
						break;
						case Type.OPTIONS:
						{
							ret = _options.Length == other._options.Length;
							for ( int i = 0; i != _options.Length && ret; ++i )
							{
								ret = ret && _options[i] == other._options[i];
							}
						}
							break;
					}
				}
				return ret;
			}
			public override int GetHashCode()
			{
				var hashCode = 703995704;
				hashCode = hashCode * -1521134295 + _type.GetHashCode();
				hashCode = hashCode * -1521134295 + _input_system.GetHashCode();
				switch ( _type )
				{
					case Type.KEY:
						if ( _input_system == Input_System.UNITY )
						{
							hashCode = hashCode * -1521134295 + _key_code.GetHashCode();
						} else
						{
							#if TG_RAW_INPUT
							hashCode = hashCode * -1521134295 + _scancode_index.GetHashCode();
							#endif
						}
						break;
					case Type.MOUSE_BUTTON:
						hashCode = hashCode * -1521134295 + _mouse_button.GetHashCode();
						break;
					case Type.AXIS:
						hashCode = hashCode * -1521134295 + _axis.GetHashCode();
						break;
					case Type.GAMEPAD_BUTTON:
						hashCode = hashCode * -1521134295 + _gamepad_button.GetHashCode();
						hashCode = hashCode * -1521134295 + _gamepad_device_id.GetHashCode();
						break;
					case Type.COMBINED:
						for ( int i = 0; i != _combined.Length; ++i )
						{
							hashCode = hashCode * -1521134295 + _combined[i].GetHashCode();
						}
						break;
					case Type.OPTIONS:
						for ( int i = 0; i != _options.Length; ++i )
						{
							hashCode = hashCode * -1521134295 + _options[i].GetHashCode();
						}
						break;
				}
				if ( _input_system == Input_System.RAW )
				{
					hashCode = hashCode * -1521134295 + _device_path.GetHashCode();
				}
				return hashCode;
			}
			public static bool operator ==( Input_Source source1, Input_Source source2 )
			{
				return source1.Equals( source2 );
			}
			public static bool operator !=( Input_Source source1, Input_Source source2 )
			{
				return !( source1 == source2 );
			}
			public static bool operator <( Input_Source s0, Input_Source s1 )
			{
				bool ret = s0.type < s1.type;
				if ( s0.type == s1.type )
				{
					switch ( s0.type )
					{
						case Type.KEY:
							if ( s0._input_system == s1.input_system )
							{
								if ( s0._input_system == Input_System.UNITY )
								{
									ret = s0._key_code < s1._key_code;
								} else
								{
									#if TG_RAW_INPUT
									if ( s0._scancode_index == s1._scancode_index )
									{
										ret = s0._device_path < s1._device_path;
									} else
									{
										ret = s0._scancode_index < s1._scancode_index;
									}
									#endif
								}
							} else
							{
								ret = s0._input_system == s1.input_system;
							}
							break;
						case Type.MOUSE_BUTTON:
							if ( s0._input_system == s1._input_system )
							{
								if ( s0._input_system == Input_System.UNITY )
								{
									ret = s0._mouse_button < s1._mouse_button;
								} else
								{
									if ( s0._mouse_button == s1._mouse_button )
									{
										ret = s0._device_path < s1._device_path;
									} else
									{
										ret = s0._mouse_button < s1._mouse_button;
									}
								}
							} else
							{
								ret = s0._input_system == s1.input_system;
							}
							break;

						case Type.AXIS:
							ret = s0._axis < s1._axis;
							break;
						case Type.GAMEPAD_BUTTON:
							ret = s0._gamepad_button < s1._gamepad_button || ( s0._gamepad_button == s1._gamepad_button && ( s0._gamepad_device_id < s1._gamepad_device_id || s0._gamepad_device_id == s1._gamepad_device_id ) );
							break;
						case Type.COMBINED:
							break;
						case Type.OPTIONS:
							break;
					}
				}
				return ret;
			}
			public static bool operator >( Input_Source s0, Input_Source s1 )
			{
				bool ret = s0.type > s1.type;
				if ( s0.type == s1.type )
				{
					switch ( s0.type )
					{
						case Type.KEY:
							if ( s0._input_system == s1.input_system )
							{
								if ( s0._input_system == Input_System.UNITY )
								{
									ret = s0._key_code > s1._key_code;
								} else
								{
									#if TG_RAW_INPUT
									if ( s0._scancode_index == s1._scancode_index )
									{
										ret = s0._device_path > s1._device_path;
									} else
									{
										ret = s0._scancode_index > s1._scancode_index;
									}
									#endif
								}
							} else
							{
								ret = s0._input_system == s1.input_system;
							}
							break;
						case Type.MOUSE_BUTTON:
							if ( s0._input_system == s1._input_system )
							{
								if ( s0._input_system == Input_System.UNITY )
								{
									ret = s0._mouse_button > s1._mouse_button;
								} else
								{
									if ( s0._mouse_button == s1._mouse_button )
									{
										ret = s0._device_path > s1._device_path;
									} else
									{
										ret = s0._mouse_button > s1._mouse_button;
									}
								}
							} else
							{
								ret = s0._input_system == s1.input_system;
							}
							break;
						case Type.AXIS:
							ret = s0._axis > s1._axis;
							break;
						case Type.GAMEPAD_BUTTON:
							ret = s0._gamepad_button > s1._gamepad_button || ( s0._gamepad_button == s1._gamepad_button && ( s0._gamepad_device_id > s1._gamepad_device_id || s0._gamepad_device_id == s1._gamepad_device_id ) );
							break;
						case Type.COMBINED:
							break;
						case Type.OPTIONS:
							break;
					}
				}
				return ret;
			}
			public static bool operator <=( Input_Source s0, Input_Source s1 )
			{
				bool ret = s0.type <= s1.type;
				if ( s0.type == s1.type )
				{
					switch ( s0.type )
					{
						case Type.KEY:
							if ( s0._input_system == s1.input_system )
							{
								if ( s0._input_system == Input_System.UNITY )
								{
									ret = s0._key_code <= s1._key_code;
								} else
								{
									#if TG_RAW_INPUT
									if ( s0._scancode_index == s1._scancode_index )
									{
										ret = s0._device_path <= s1._device_path;
									} else
									{
										ret = s0._scancode_index <= s1._scancode_index;
									}
									#endif
								}
							} else
							{
								ret = s0._input_system == s1.input_system;
							}
							break;
						case Type.MOUSE_BUTTON:
							if ( s0._input_system == s1._input_system )
							{
								if ( s0._input_system == Input_System.UNITY )
								{
									ret = s0._mouse_button <= s1._mouse_button;
								} else
								{
									if ( s0._mouse_button == s1._mouse_button )
									{
										ret = s0._device_path <= s1._device_path;
									} else
									{
										ret = s0._mouse_button <= s1._mouse_button;
									}
								}
							} else
							{
								ret = s0._input_system == s1.input_system;
							}
							break;

						case Type.AXIS:
							ret = s0._axis <= s1._axis;
							break;
						case Type.GAMEPAD_BUTTON:
							ret = s0._gamepad_button <= s1._gamepad_button || ( s0._gamepad_button == s1._gamepad_button && ( s0._gamepad_device_id <= s1._gamepad_device_id || s0._gamepad_device_id == s1._gamepad_device_id ) );
							break;
						case Type.COMBINED:
							break;
						case Type.OPTIONS:
							break;
					}
				}
				return ret;
			}
			public static bool operator >=( Input_Source s0, Input_Source s1 )
			{
				bool ret = s0.type >= s1.type;
				if ( s0.type == s1.type )
				{
					switch ( s0.type )
					{
						case Type.KEY:
							if ( s0._input_system == s1.input_system )
							{
								if ( s0._input_system == Input_System.UNITY )
								{
									ret = s0._key_code >= s1._key_code;
								} else
								{
									#if TG_RAW_INPUT
									if ( s0._scancode_index == s1._scancode_index )
									{
										ret = s0._device_path >= s1._device_path;
									} else
									{
										ret = s0._scancode_index >= s1._scancode_index;
									}
									#endif
								}
							} else
							{
								ret = s0._input_system == s1.input_system;
							}
							break;
						case Type.MOUSE_BUTTON:
							if ( s0._input_system == s1._input_system )
							{
								if ( s0._input_system == Input_System.UNITY )
								{
									ret = s0._mouse_button >= s1._mouse_button;
								} else
								{
									if ( s0._mouse_button == s1._mouse_button )
									{
										ret = s0._device_path >= s1._device_path;
									} else
									{
										ret = s0._mouse_button >= s1._mouse_button;
									}
								}
							} else
							{
								ret = s0._input_system == s1.input_system;
							}
							break;
						case Type.AXIS:
							ret = s0._axis >= s1._axis;
							break;
						case Type.GAMEPAD_BUTTON:
							ret = s0._gamepad_button >= s1._gamepad_button || ( s0._gamepad_button == s1._gamepad_button && ( s0._gamepad_device_id >= s1._gamepad_device_id || s0._gamepad_device_id == s1._gamepad_device_id ) );
							break;
						case Type.COMBINED:
							break;
						case Type.OPTIONS:
							break;
					}
				}
				return ret;
			}
			public int CompareTo( object obj )
			{
				return CompareTo( (Input_Source)obj );
			}
			public int CompareTo( Input_Source other )
			{
				int ret = this < other ? -1 : ( this > other ? 1 : 0 );
				return ret;
			}
			#endregion
		}
		#if TG_RAW_INPUT
		//[StructLayout(LayoutKind.Explicit)]
		public struct
		Raw_Input_Event
		{
			public enum Type
			{
				MOUSE,
				KEYBOARD,
			}
			public Type               type        => _type;
			public MonotonicTimestamp timestamp   => _timestamp;
			public Device_Path        device_path => _device_path;
			public RawMouse           mouse       => _mouse;
			public RawKeyboard        keyboard    => _keyboard;

			/*[FieldOffset(0)                                               ]*/ private Type               _type       ;
			/*[FieldOffset(sizeof(Type))                                    ]*/ private MonotonicTimestamp _timestamp  ;
			/*[FieldOffset(sizeof(Type)+sizeof(long))                       ]*/ private Device_Path        _device_path;
			/*[FieldOffset(sizeof(Type)+sizeof(long)+Device_Path.BYTES_SIZE)]*/ private RawMouse           _mouse      ;
			/*[FieldOffset(sizeof(Type)+sizeof(long)+Device_Path.BYTES_SIZE)]*/ private RawKeyboard        _keyboard   ;

			public Raw_Input_Event( RawKeyboard keyboard, string path, MonotonicTimestamp ts )
			{
				_type        = Type.KEYBOARD;
				_timestamp   = ts;
				_device_path = new Device_Path( path );
				_mouse       = default;
				_keyboard    = keyboard;
			}
			public Raw_Input_Event( RawMouse mouse, string path, MonotonicTimestamp ts )
			{
				_type        = Type.MOUSE;
				_timestamp   = ts;
				_device_path = new Device_Path( path );
				_mouse       = mouse;
				_keyboard    = default;
			}
		}
		#endif
		[Serializable]
		unsafe public struct
		Device_Path : IEquatable<Device_Path>, ISerializationCallbackReceiver
		{
			public const int PATH_LENGTH = 256;

			#if true
			public Device_Path( string path )
			{
				__string = null;
				fixed ( char * s = path )
				{
					fixed ( char * d = _device_path )
					{
						chars_length = Math.Min( path.Length, PATH_LENGTH - 1 );
						Buffer.MemoryCopy( s, d, PATH_LENGTH * sizeof( char ), chars_length * sizeof( char ) );
						hash = unchecked( (int)u32_FNV1a( (byte*)d, chars_length * sizeof ( char ) ) );
					}
					_device_path[PATH_LENGTH  - 1] = '\0';
					_device_path[chars_length    ] = '\0';
				}
				//log( $"new device path '{path}' => '{this.ToString()}'" );
			}
			#else
			public Device_Path( string path )
			{
				int i;
				for ( i = 0; i != PATH_LENGTH && i != path.Length; ++i )
				{
					_device_path[i] = path[i];
				}
				for ( ; i != PATH_LENGTH; ++i )
				{
					_device_path[i] = (char)0;
				}
			}
			#endif

			public const int BYTES_SIZE = PATH_LENGTH * sizeof(char) + sizeof(int) * 2;
			[NonSerialized]
			private fixed char _device_path[PATH_LENGTH];
			public int hash { get; }
			[SerializeField]
			public int chars_length { get; }

			public string __string;

			public override string ToString()
			{
				fixed ( char * b = _device_path )
				{
					return new string( b );
				}
			}

			public void OnBeforeSerialize()
			{
				__string = ToString();
			}

			public void OnAfterDeserialize()
			{
				this = new Device_Path( __string );
			}

			public override bool Equals( object obj )
			{
				return obj is Device_Path && Equals( (Device_Path)obj );
			}

			public bool Equals( Device_Path o )
			{
				bool ret = chars_length == o.chars_length && hash == o.hash;
				if ( ret )
				{
					for ( int i = 0; i != chars_length; ++i )
					{
						ret = ret && _device_path[i] == o._device_path[i];
					}
				}
				return ret;
			}

			public override int GetHashCode()
			{
				return hash;
			}

			public static bool operator ==( Device_Path path1, Device_Path path2 )
			{
				return path1.Equals( path2 );
			}

			public static bool operator !=( Device_Path path1, Device_Path path2 )
			{
				return !( path1 == path2 );
			}

			public static bool operator <( Device_Path p1, Device_Path p2 )
			{
				bool ret = true;
				bool equals = true;
				for ( int i = 0; i != p1.chars_length && i != p2.chars_length && equals; ++i )
				{
					equals = equals && p1._device_path[i] == p2._device_path[i];
					ret = ret && p1._device_path[i] < p2._device_path[i];
				}
				if ( equals )
				{
					ret = p1.chars_length < p2.chars_length;
				}
				return ret;
			}
			public static bool operator >( Device_Path p1, Device_Path p2 )
			{
				bool ret = true;
				bool equals = true;
				for ( int i = 0; i != p1.chars_length && i != p2.chars_length && equals; ++i )
				{
					equals = equals && p1._device_path[i] == p2._device_path[i];
					ret = ret && p1._device_path[i] > p2._device_path[i];
				}
				if ( equals )
				{
					ret = p1.chars_length > p2.chars_length;
				}
				return ret;
			}
			public static bool operator <=( Device_Path p1, Device_Path p2 )
			{
				bool ret = true;
				bool equals = true;
				for ( int i = 0; i != p1.chars_length && i != p2.chars_length && equals; ++i )
				{
					equals = equals && p1._device_path[i] == p2._device_path[i];
					ret = ret && p1._device_path[i] <= p2._device_path[i];
				}
				if ( equals )
				{
					ret = p1.chars_length < p2.chars_length;
				}
				return ret;
			}
			public static bool operator >=( Device_Path p1, Device_Path p2 )
			{
				bool ret = true;
				bool equals = true;
				for ( int i = 0; i != p1.chars_length && i != p2.chars_length && equals; ++i )
				{
					equals = equals && p1._device_path[i] == p2._device_path[i];
					ret = ret && p1._device_path[i] >= p2._device_path[i];
				}
				if ( equals )
				{
					ret = p1.chars_length > p2.chars_length;
				}
				return ret;
			}
		}

		#if TG_RAW_INPUT
		public class
		Device_Infos
		{
			public enum Type
			{
				MOUSE,
				KEYBOARD
			}
			public struct Mouse
			{
				public int  sample_rate;
				public bool has_horizontal_wheel;
				public int  buttons_count;
			}
			public struct Keyboard
			{
				public int    function_key_count;
				public int    indicator_count;
				public int    total_key_count;
				//KeyboardMode: {raw_k.KeyboardMode}\n"
				//KeyboardType: {raw_k.KeyboardType}\n"
				//KeyboardSubType: {raw_k.KeyboardSubType}\n"
			}
			public Type        type              { get; private set; }
			public Device_Path device_path       { get; private set; }
			public string      manufacturer_name { get; private set; }
			public string      product_name      { get; private set; }
			public int         vendor_id         { get; private set; }
			public int         product_id        { get; private set; }
			public Mouse       mouse             { get; private set; }
			public Keyboard    keyboard          { get; private set; }

			public Device_Infos( RawInputDevice device, Device_Path device_path )
			{
				this.device_path = device_path;

				manufacturer_name  = device.ManufacturerName;
				product_name       = device.ProductName;
				vendor_id          = device.VendorId;
				product_id         = device.ProductId;

				if ( device.DeviceType == RawInputDeviceType.Keyboard )
				{
					type = Type.KEYBOARD;
					var k = device as RawInputKeyboard;
					Keyboard keyboard = new Keyboard()
					{
						function_key_count = k.FunctionKeyCount,
						indicator_count = k.IndicatorCount,
						total_key_count = k.TotalKeyCount,
					};
					this.keyboard = keyboard;
				} else
				if ( device.DeviceType == RawInputDeviceType.Mouse )
				{
					type = Type.MOUSE;
					var m = device as RawInputMouse;
					Mouse mouse = new Mouse()
					{
						sample_rate = m.SampleRate,
						has_horizontal_wheel = m.HasHorizontalWheel,
						buttons_count = m.ButtonCount,
					};
					this.mouse = mouse;
				} else
				{
					ILLEGAL_PATH( "HIDs not supported" );
				}
			}

			public override
			string
			ToString()
			{
				string ret = $"{type}\n{device_path}\nManufacturer: {manufacturer_name}\nProduct: {product_name}\nVendor ID: {vendor_id}\nProduct ID: {product_id}\n";
				if ( type == Type.KEYBOARD )
				{
					ret += $"Function keys: {keyboard.function_key_count}\nIndicators: {keyboard.indicator_count}\nKeys: {keyboard.total_key_count}";
				} else
				{
					ret += $"Sample Rate: {mouse.sample_rate}\nHorizontal Wheel: {mouse.has_horizontal_wheel}\nButtons: {mouse.buttons_count}";
				}
				return ret;
			}
		}

		/*
		The scancode values come from:
		- http://download.microsoft.com/download/1/6/1/161ba512-40e2-4cc9-843a-923143f3456c/scancode.doc (March 16, 2000).
		- http://www.computer-engineering.org/ps2keyboard/scancodes1.html
		- using MapVirtualKeyEx( VK_*, MAPVK_VK_TO_VSC_EX, 0 ) with the english us keyboard layout
		- reading win32 WM_INPUT keyboard messages.
		*/

		public enum
		Scancode : uint
		{
			escape            = 0x01,
			_1                = 0x02,
			_2                = 0x03,
			_3                = 0x04,
			_4                = 0x05,
			_5                = 0x06,
			_6                = 0x07,
			_7                = 0x08,
			_8                = 0x09,
			_9                = 0x0A,
			_0                = 0x0B,
			minus             = 0x0C,
			equals            = 0x0D,
			backspace         = 0x0E,
			tab               = 0x0F,
			q                 = 0x10,
			w                 = 0x11,
			e                 = 0x12,
			r                 = 0x13,
			t                 = 0x14,
			y                 = 0x15,
			u                 = 0x16,
			i                 = 0x17,
			o                 = 0x18,
			p                 = 0x19,
			bracketLeft       = 0x1A,
			bracketRight      = 0x1B,
			enter             = 0x1C,
			controlLeft       = 0x1D,
			a                 = 0x1E,
			s                 = 0x1F,
			d                 = 0x20,
			f                 = 0x21,
			g                 = 0x22,
			h                 = 0x23,
			j                 = 0x24,
			k                 = 0x25,
			l                 = 0x26,
			semicolon         = 0x27,
			apostrophe        = 0x28,
			grave             = 0x29,
			shiftLeft         = 0x2A,
			backslash         = 0x2B,
			z                 = 0x2C,
			x                 = 0x2D,
			c                 = 0x2E,
			v                 = 0x2F,
			b                 = 0x30,
			n                 = 0x31,
			m                 = 0x32,
			comma             = 0x33,
			preiod            = 0x34,
			slash             = 0x35,
			shiftRight        = 0x36,
			numpad_multiply   = 0x37,
			altLeft           = 0x38,
			space             = 0x39,
			capsLock          = 0x3A,
			f1                = 0x3B,
			f2                = 0x3C,
			f3                = 0x3D,
			f4                = 0x3E,
			f5                = 0x3F,
			f6                = 0x40,
			f7                = 0x41,
			f8                = 0x42,
			f9                = 0x43,
			f10               = 0x44,
			numLock           = 0x45,
			scrollLock        = 0x46,
			numpad_7          = 0x47,
			numpad_8          = 0x48,
			numpad_9          = 0x49,
			numpad_minus      = 0x4A,
			numpad_4          = 0x4B,
			numpad_5          = 0x4C,
			numpad_6          = 0x4D,
			numpad_plus       = 0x4E,
			numpad_1          = 0x4F,
			numpad_2          = 0x50,
			numpad_3          = 0x51,
			numpad_0          = 0x52,
			numpad_period     = 0x53,
			alt_printScreen   = 0x54, /* Alt + print screen. MapVirtualKeyEx( VK_SNAPSHOT, MAPVK_VK_TO_VSC_EX, 0 ) returns scancode 0x54. */
			_unknown_0x55     = 0x55,
			bracketAngle      = 0x56, /* Key between the left shift and Z. */
			f11               = 0x57,
			f12               = 0x58,
			_unknown_0x59     = 0x59,
			oem_1             = 0x5a, /* VK_OEM_WSCTRL */
			oem_2             = 0x5b, /* VK_OEM_FINISH */
			oem_3             = 0x5c, /* VK_OEM_JUMP */
			eraseEOF          = 0x5d,
			oem_4             = 0x5e, /* VK_OEM_BACKTAB */
			oem_5             = 0x5f, /* VK_OEM_AUTO */
			_unknown_0x60     = 0x60,
			_unknown_0x61     = 0x61,
			zoom              = 0x62,
			help              = 0x63,
			f13               = 0x64,
			f14               = 0x65,
			f15               = 0x66,
			f16               = 0x67,
			f17               = 0x68,
			f18               = 0x69,
			f19               = 0x6a,
			f20               = 0x6b,
			f21               = 0x6c,
			f22               = 0x6d,
			f23               = 0x6e,
			oem_6             = 0x6f, /* VK_OEM_PA3 */
			katakana          = 0x70,
			oem_7             = 0x71, /* VK_OEM_RESET */
			_unknown_0x72     = 0x72,
			_unknown_0x73     = 0x73,
			_unknown_0x74     = 0x74,
			_unknown_0x75     = 0x75,
			f24               = 0x76,
			sbcschar          = 0x77,
			convert           = 0x79,
			_unknown_0x7A     = 0x7A,
			nonconvert        = 0x7B, /* VK_OEM_PA1 */

			media_previous    = 0xE010,
			_unknown_0xE011   = 0xE011,
			_unknown_0xE012   = 0xE012,
			_unknown_0xE013   = 0xE013,
			_unknown_0xE014   = 0xE014,
			_unknown_0xE015   = 0xE015,
			_unknown_0xE016   = 0xE016,
			_unknown_0xE017   = 0xE017,
			_unknown_0xE018   = 0xE018,
			media_next        = 0xE019,
			_unknown_0xE01A   = 0xE01A,
			_unknown_0xE01B   = 0xE01B,
			numpad_enter      = 0xE01C,
			controlRight      = 0xE01D,
			_unknown_0xE01E   = 0xE01E,
			_unknown_0xE01F   = 0xE01F,
			volume_mute       = 0xE020,
			launch_app2       = 0xE021,
			media_play        = 0xE022,
			_unknown_0xE023   = 0xE023,
			media_stop        = 0xE024,
			_unknown_0xE025   = 0xE025,
			_unknown_0xE026   = 0xE026,
			_unknown_0xE027   = 0xE027,
			_unknown_0xE028   = 0xE028,
			_unknown_0xE029   = 0xE029,
			printScreenDown   = 0xE02A,
			printScreenUp     = 0xE02A,
			_unknown_0xE02B   = 0xE02B,
			_unknown_0xE02C   = 0xE02C,
			_unknown_0xE02D   = 0xE02D,
			volume_down       = 0xE02E,
			_unknown_0xE02F   = 0xE02F,
			volume_up         = 0xE030,
			_unknown_0xE031   = 0xE031,
			browser_home      = 0xE032,
			_unknown_0xE033   = 0xE033,
			_unknown_0xE034   = 0xE034,
			numpad_divide     = 0xE035,
			_unknown_0xE036   = 0xE036,
			printScreen       = 0xE037,
			/*
			printScreen:
			- make: 0xE02A 0xE037
			- break: 0xE0B7 0xE0AA
			- MapVirtualKeyEx( VK_SNAPSHOT, MAPVK_VK_TO_VSC_EX, 0 ) returns scancode 0x54;
			- There is no VK_KEYDOWN with VK_SNAPSHOT.
			*/
			altRight          = 0xE038,
			_unknown_0xE039   = 0xE039,
			_unknown_0xE03A   = 0xE03A,
			_unknown_0xE03B   = 0xE03B,
			_unknown_0xE03C   = 0xE03C,
			_unknown_0xE03D   = 0xE03D,
			_unknown_0xE03E   = 0xE03E,
			_unknown_0xE03F   = 0xE03F,
			_unknown_0xE040   = 0xE040,
			_unknown_0xE041   = 0xE041,
			_unknown_0xE042   = 0xE042,
			_unknown_0xE043   = 0xE043,
			_unknown_0xE044   = 0xE044,
			_unknown_0xE045   = 0xE045,
			cancel            = 0xE046, /* CTRL + Pause */
			home              = 0xE047,
			arrowUp           = 0xE048,
			pageUp            = 0xE049,
			_unknown_0xE04A   = 0xE04A,
			arrowLeft         = 0xE04B,
			_unknown_0xE04C   = 0xE04C,
			arrowRight        = 0xE04D,
			_unknown_0xE04E   = 0xE04E,
			end               = 0xE04F,
			arrowDown         = 0xE050,
			pageDown          = 0xE051,
			insert            = 0xE052,
			delete            = 0xE053,
			_unknown_0xE054   = 0xE054,
			_unknown_0xE055   = 0xE055,
			_unknown_0xE056   = 0xE056,
			_unknown_0xE057   = 0xE057,
			_unknown_0xE058   = 0xE058,
			_unknown_0xE059   = 0xE059,
			_unknown_0xE05A   = 0xE05A,
			metaLeft          = 0xE05B,
			metaRight         = 0xE05C,
			application       = 0xE05D,
			power             = 0xE05E,
			sleep             = 0xE05F,
			_unknown_0xE060   = 0xE060,
			_unknown_0xE061   = 0xE061,
			_unknown_0xE062   = 0xE062,
			wake              = 0xE063,
			_unknown_0xE064   = 0xE064,
			browser_search    = 0xE065,
			browser_favorites = 0xE066,
			browser_refresh   = 0xE067,
			browser_stop      = 0xE068,
			browser_forward   = 0xE069,
			browser_back      = 0xE06A,
			launch_app1       = 0xE06B,
			launch_email      = 0xE06C,
			launch_media      = 0xE06D,

			pausePartial0     = 0xE11D00,
			pause             = 0xE11D45,
			/*
			pause:
			- make: 0xE11D 45 0xE19D C5
			- make in raw input: 0xE11D 0x45
			- break: none
			- No repeat when you hold the key down
			- There are no break so I don't know how the key down/up is expected to work. Raw input sends "keydown" and "keyup" messages, and it appears that the keyup message is sent directly after the keydown message (you can't hold the key down) so depending on when GetMessage or PeekMessage will return messages, you may get both a keydown and keyup message "at the same time". If you use VK messages most of the time you only get keydown messages, but some times you get keyup messages too.
			- when pressed at the same time as one or both control keys, generates a 0xE046 (cancel) and the string for that scancode is "break".
			*/
		};
		public enum
		Index : uint
		{
			escape            ,
			_1                ,
			_2                ,
			_3                ,
			_4                ,
			_5                ,
			_6                ,
			_7                ,
			_8                ,
			_9                ,
			_0                ,
			minus             ,
			equals            ,
			backspace         ,
			tab               ,
			q                 ,
			w                 ,
			e                 ,
			r                 ,
			t                 ,
			y                 ,
			u                 ,
			i                 ,
			o                 ,
			p                 ,
			bracketLeft       ,
			bracketRight      ,
			enter             ,
			controlLeft       ,
			a                 ,
			s                 ,
			d                 ,
			f                 ,
			g                 ,
			h                 ,
			j                 ,
			k                 ,
			l                 ,
			semicolon         ,
			apostrophe        ,
			grave             ,
			shiftLeft         ,
			backslash         ,
			z                 ,
			x                 ,
			c                 ,
			v                 ,
			b                 ,
			n                 ,
			m                 ,
			comma             ,
			preiod            ,
			slash             ,
			shiftRight        ,
			numpad_multiply   ,
			altLeft           ,
			space             ,
			capsLock          ,
			f1                ,
			f2                ,
			f3                ,
			f4                ,
			f5                ,
			f6                ,
			f7                ,
			f8                ,
			f9                ,
			f10               ,
			numLock           ,
			scrollLock        ,
			numpad_7          ,
			numpad_8          ,
			numpad_9          ,
			numpad_minus      ,
			numpad_4          ,
			numpad_5          ,
			numpad_6          ,
			numpad_plus       ,
			numpad_1          ,
			numpad_2          ,
			numpad_3          ,
			numpad_0          ,
			numpad_period     ,
			alt_printScreen   , /* Alt + print screen. MapVirtualKeyEx( VK_SNAPSHOT, MAPVK_VK_TO_VSC_EX, 0 ) returns scancode 0x54. */
			_unknown_0x55     ,
			bracketAngle      , /* Key between the left shift and Z. */
			f11               ,
			f12               ,
			_unknown_0x59     ,
			oem_1             , /* VK_OEM_WSCTRL */
			oem_2             , /* VK_OEM_FINISH */
			oem_3             , /* VK_OEM_JUMP */
			eraseEOF          ,
			oem_4             , /* VK_OEM_BACKTAB */
			oem_5             , /* VK_OEM_AUTO */
			_unknown_0x60     ,
			_unknown_0x61     ,
			zoom              ,
			help              ,
			f13               ,
			f14               ,
			f15               ,
			f16               ,
			f17               ,
			f18               ,
			f19               ,
			f20               ,
			f21               ,
			f22               ,
			f23               ,
			oem_6             , /* VK_OEM_PA3 */
			katakana          ,
			oem_7             , /* VK_OEM_RESET */
			_unknown_0x72     ,
			_unknown_0x73     ,
			_unknown_0x74     ,
			_unknown_0x75     ,
			f24               ,
			sbcschar          ,
			convert           ,
			_unknown_0x7A     ,
			nonconvert        , /* VK_OEM_PA1 */

			media_previous    ,
			_unknown_0xE011   ,
			_unknown_0xE012   ,
			_unknown_0xE013   ,
			_unknown_0xE014   ,
			_unknown_0xE015   ,
			_unknown_0xE016   ,
			_unknown_0xE017   ,
			_unknown_0xE018   ,
			media_next        ,
			_unknown_0xE01A   ,
			_unknown_0xE01B   ,
			numpad_enter      ,
			controlRight      ,
			_unknown_0xE01E   ,
			_unknown_0xE01F   ,
			volume_mute       ,
			launch_app2       ,
			media_play        ,
			_unknown_0xE023   ,
			media_stop        ,
			_unknown_0xE025   ,
			_unknown_0xE026   ,
			_unknown_0xE027   ,
			_unknown_0xE028   ,
			_unknown_0xE029   ,
			printScreenDown   ,
			printScreenUp     ,
			_unknown_0xE02B   ,
			_unknown_0xE02C   ,
			_unknown_0xE02D   ,
			volume_down       ,
			_unknown_0xE02F   ,
			volume_up         ,
			_unknown_0xE031   ,
			browser_home      ,
			_unknown_0xE033   ,
			_unknown_0xE034   ,
			numpad_divide     ,
			_unknown_0xE036   ,
			printScreen       ,
			/*
			printScreen:
			- make: 0xE02A 0xE037
			- break: 0xE0B7 0xE0AA
			- MapVirtualKeyEx( VK_SNAPSHOT, MAPVK_VK_TO_VSC_EX, 0 ) returns scancode 0x54;
			- There is no VK_KEYDOWN with VK_SNAPSHOT.
			*/
			altRight          ,
			_unknown_0xE039   ,
			_unknown_0xE03A   ,
			_unknown_0xE03B   ,
			_unknown_0xE03C   ,
			_unknown_0xE03D   ,
			_unknown_0xE03E   ,
			_unknown_0xE03F   ,
			_unknown_0xE040   ,
			_unknown_0xE041   ,
			_unknown_0xE042   ,
			_unknown_0xE043   ,
			_unknown_0xE044   ,
			_unknown_0xE045   ,
			cancel            , /* CTRL + Pause */
			home              ,
			arrowUp           ,
			pageUp            ,
			_unknown_0xE04A   ,
			arrowLeft         ,
			_unknown_0xE04C   ,
			arrowRight        ,
			_unknown_0xE04E   ,
			end               ,
			arrowDown         ,
			pageDown          ,
			insert            ,
			delete            ,
			_unknown_0xE054   ,
			_unknown_0xE055   ,
			_unknown_0xE056   ,
			_unknown_0xE057   ,
			_unknown_0xE058   ,
			_unknown_0xE059   ,
			_unknown_0xE05A   ,
			metaLeft          ,
			metaRight         ,
			application       ,
			power             ,
			sleep             ,
			_unknown_0xE060   ,
			_unknown_0xE061   ,
			_unknown_0xE062   ,
			wake              ,
			_unknown_0xE064   ,
			browser_search    ,
			browser_favorites ,
			browser_refresh   ,
			browser_stop      ,
			browser_forward   ,
			browser_back      ,
			launch_app1       ,
			launch_email      ,
			launch_media      ,

			pausePartial0     ,
			pause             ,
			/*
			pause:
			- make: 0xE11D 45 0xE19D C5
			- make in raw input: 0xE11D 0x45
			- break: none
			- No repeat when you hold the key down
			- There are no break so I don't know how the key down/up is expected to work. Raw input sends "keydown" and "keyup" messages, and it appears that the keyup message is sent directly after the keydown message (you can't hold the key down) so depending on when GetMessage or PeekMessage will return messages, you may get both a keydown and keyup message "at the same time". If you use VK messages most of the time you only get keydown messages, but some times you get keyup messages too.
			- when pressed at the same time as one or both control keys, generates a 0xE046 (cancel) and the string for that scancode is "break".
			*/
		};
		public class
		Keyboard_Status
		{
			public const int KEYS_MAX_COUNT = 256;
			private Bit_Vector _keys_status_down;
			private Bit_Vector _keys_prev_status_down;
			private Bit_Vector _keys_front_up;
			private Bit_Vector _keys_front_down;
			private Bit_Vector _keys_prev_front_up;
			private Bit_Vector _keys_prev_front_down;
			public Bit_Vector keys_status_down      => _keys_status_down;
			public Bit_Vector keys_prev_status_down => _keys_prev_status_down;
			public Bit_Vector keys_front_up         => _keys_front_up;
			public Bit_Vector keys_front_down       => _keys_front_down;
			public Bit_Vector keys_prev_front_up    => _keys_prev_front_up;
			public Bit_Vector keys_prev_front_down  => _keys_prev_front_down;

			#region scancode management
			private const int group_0_start = (int)Scancode.escape;
			private const int group_0_end   = (int)Scancode.nonconvert;
			private const int group_0_count = group_0_end - group_0_start + 1;
			private const int group_1_start = (int)Scancode.media_previous;
			private const int group_1_end   = (int)Scancode.launch_media;
			private const int group_1_count = group_1_end - group_1_start + 1;
			private const int group_2_start = (int)Scancode.pausePartial0;
			public static int scancode_from_index( int index, out Opt<RawKeyboardFlags> flags )
			{
				int ret;// = ( index > 127 ? index - 128 + 0xe010 : index );
				flags = Opt<RawKeyboardFlags>.NO_VALUE;


				int ig2_start = group_0_count + group_1_count;
				int ig1_start = group_0_count;
				if ( index >= ig2_start )
				{
					ret = ( index - ig2_start + group_2_start );
				} else
				if ( index >= ig1_start )
				{
					ret = ( index - ig1_start + group_1_start );
				} else
				{
					debug_assert( index <= group_0_end );
					ret = group_0_start + index;
				}
				#if false
				switch ( ret )
				{
					case Scancode.altRight:
					case Scancode.controlRight:
					case Scancode.metaLeft: // NOTE(theGiallo): this library has something wrong...
					case Scancode.metaRight:
						ret = (Scancode)( (int)ret & ~0xe000 );
						// NOTE(theGiallo): this should be "Right" but this library gives Left...
						flags = RawKeyboardFlags.LeftKey;
						break;
				}
				#else
				if ( ((int)ret & 0xe10000 ) == 0xe10000 )
				{
					ret = ( (int)ret & ~0xe10000 ) >> 8;
					flags = RawKeyboardFlags.RightKey;
				} else
				if ( ((int)ret & 0xe000 ) == 0xe000 )
				{
					ret = (int)ret & ~0xe000;
					flags = RawKeyboardFlags.LeftKey;
				}
				#endif

				return ret;
			}
			public static Scancode scancode_from_index( int index )
			{
				int s = scancode_from_index( index, out Opt<RawKeyboardFlags> flags );
				Scancode ret = (Scancode)s;
				if ( flags.has_value )
				{
					ret = scancode_from_int_and_flags( s, flags.value );
				}
				return ret;
			}
			public static Scancode scancode_from_int_and_flags( int scancode, RawKeyboardFlags flags )
			{
				if ( has_flags( flags, RawKeyboardFlags.RightKey ) )
				{
					scancode = ( scancode << 8 ) | 0xe10000;
				}
				if ( has_flags( flags, RawKeyboardFlags.LeftKey ) )
				{
					scancode = scancode | 0xe000;
				}
				Scancode ret = (Scancode)scancode;
				return ret;
			}
			public static int index_from_int_and_flags( int i_scancode, RawKeyboardFlags flags )
			{
				Scancode scancode = scancode_from_int_and_flags( i_scancode, flags );
				return index_from_scancode( scancode );
			}
			public static int index_from_scancode( Scancode scancode )
			{
				int i_scancode = (int)scancode;

				int ret           = i_scancode;

				if ( i_scancode >= group_2_start )
				{
					#if false
					if ( i_scancode > group_2_start )
					{
						i_scancode = group_2_start;
					}
					ret = group_0_count + group_1_count + ( i_scancode - group_2_start );
					#else
					ret = group_0_count + group_1_count;
					#endif
				} else if ( i_scancode >= group_1_start )
				{
					ret = group_0_count + ( i_scancode - group_1_start );
				} else
				{
					ret = i_scancode - group_0_start;
				}

				debug_assert( ret <= 0xff );

				return ret;
			}
			#endregion

			public Keyboard_Status()
			{
				_keys_status_down      = new Bit_Vector( KEYS_MAX_COUNT );
				_keys_prev_status_down = new Bit_Vector( KEYS_MAX_COUNT );
				_keys_front_down       = new Bit_Vector( KEYS_MAX_COUNT );
				_keys_front_up         = new Bit_Vector( KEYS_MAX_COUNT );
			}

			public bool key_status_down( Index index )
			{
				bool ret = _keys_status_down[(int)index] == 1;
				return ret;
			}
			public bool key_prev_status_down( Index index )
			{
				bool ret = _keys_prev_status_down[(int)index] == 1;
				return ret;
			}
			public bool key_front_up( Index index )
			{
				bool ret = _keys_front_up[(int)index] == 1;
				return ret;
			}
			public bool key_front_down( Index index )
			{
				bool ret = _keys_front_down[(int)index] == 1;
				return ret;
			}

			private Scancode last_scancode_applied;
			public void start_update()
			{
				_keys_prev_status_down.copy_from( ref _keys_status_down );
				_keys_prev_front_up   .copy_from( ref _keys_front_up );
				_keys_prev_front_down .copy_from( ref _keys_front_down );
				_keys_front_down.clear();
				_keys_front_up  .clear();
			}
			public void apply_event( Raw_Input_Event e )
			{
				debug_assert( e.type == Raw_Input_Event.Type.KEYBOARD );

				bool dont_apply = false;
				bool up   = has_flags( e.keyboard.Flags, RawKeyboardFlags.Up );
				bool down = ! up;
				//if ( up ) log( "up" );
				//if ( down ) log( "down" );
				//log( e.keyboard.Flags.ToString() );
				int index = index_from_int_and_flags( e.keyboard.ScanCode, e.keyboard.Flags );
				Scancode scancode = scancode_from_int_and_flags( e.keyboard.ScanCode, e.keyboard.Flags );
				if ( scancode == Scancode.printScreen )
				{
					if ( last_scancode_applied == Scancode.printScreenUp )
					{
						up = true;
					} else
					{
						dont_apply = true;
					}
				} else
				if ( scancode == Scancode.printScreenDown )
				{
					if ( last_scancode_applied == Scancode.printScreen )
					{
						down = true;
					} else
					{
						dont_apply = true;
					}
				} else
				if ( scancode == Scancode.pausePartial0 )
				{
					dont_apply = true;
				} else
				if ( scancode == Scancode.numLock && last_scancode_applied == Scancode.pausePartial0 )
				{
					scancode = Scancode.printScreen;
				}

				last_scancode_applied = scancode;

				if ( dont_apply )
				{
					return;
				}

				if ( down )
				{
					//log( $"down {(Index)index}" );
					_keys_status_down[index] = 1;
					_keys_front_down[index] |= 1;
				} else
				if ( up )
				{
					//log( $"up {(Index)index}" );
					_keys_status_down[index] = 0;
					_keys_front_up[index] |= 1;
				}
			}
			public void merge_status( Keyboard_Status k )
			{
				keys_status_down.copy_from( ref k._keys_status_down );
				_keys_front_up   |= k.keys_front_up;
				_keys_front_down |= k.keys_front_down;
			}
		}
		public class
		Mouse_Status
		{
			public const int BUTTONS_MAX_COUNT = 8;

			private Bit_Vector _buttons_status_down;
			public Bit_Vector _buttons_prev_status_down;
			public Bit_Vector _buttons_front_up;
			public Bit_Vector _buttons_front_down;
			public Bit_Vector buttons_status_down      => _buttons_status_down;
			public Bit_Vector buttons_prev_status_down => _buttons_prev_status_down;
			public Bit_Vector buttons_front_up         => _buttons_front_up;
			public Bit_Vector buttons_front_down       => _buttons_front_down;
			public long       wheel_h                   { get; private set; }
			public long       last_delta_wheel_h        { get; private set; }
			public long       wheel_v                   { get; private set; }
			public long       last_delta_wheel_v        { get; private set; }
			public long       prev_wheel_h              { get; private set; }
			public long       prev_last_delta_wheel_h   { get; private set; }
			public long       prev_wheel_v              { get; private set; }
			public long       prev_last_delta_wheel_v   { get; private set; }
			public long       prev_pos_x                { get; private set; }
			public long       prev_pos_y                { get; private set; }
			public long       pos_x                     { get; private set; }
			public long       pos_y                     { get; private set; }
			public long       last_delta_pos_x          { get; private set; }
			public long       last_delta_pos_y          { get; private set; }
			public long       prev_delta_pos_x          { get; private set; }
			public long       prev_delta_pos_y          { get; private set; }
			public int        step_events_counter       { get; private set; }
			public int        prev_step_events_counter  { get; private set; }
			public int        max_step_events_counter   { get; private set; }

			public Mouse_Status()
			{
				_buttons_status_down      = new Bit_Vector( BUTTONS_MAX_COUNT );
				_buttons_prev_status_down = new Bit_Vector( BUTTONS_MAX_COUNT );
				_buttons_front_up         = new Bit_Vector( BUTTONS_MAX_COUNT );
				_buttons_front_down       = new Bit_Vector( BUTTONS_MAX_COUNT );

				prev_pos_x = 0;
				prev_pos_y = 0;
				pos_x = 0;
				pos_y = 0;
				last_delta_pos_x = 0;
				last_delta_pos_y = 0;
				prev_delta_pos_x = 0;
				prev_delta_pos_y = 0;
			}

			public void apply_event( Raw_Input_Event e )
			{
				debug_assert( e.type == Raw_Input_Event.Type.MOUSE );

				++step_events_counter;

				if ( false )
				{
					log( "- - - - - - - - - -" );
					Bit_Vector bv = new Bit_Vector( 32 );
					uint u =  unchecked((uint)e.mouse.ButtonData);
					bv.copy_whole( u );
					log( $"{u} => {bv}" );

					u =  unchecked((uint)e.mouse.RawButtons);
					bv.copy_whole( u );
					log( $"{u} => {bv}" );

					u = (uint)e.mouse.Buttons;
					bv.copy_whole( u );
					log( $"{e.mouse.Buttons} => {bv}" );

					u = (uint)e.mouse.ExtraInformation;
					bv.copy_whole( u );
					log( $"{u} => {bv}" );
					log( "- - - - - - - - - -" );
				}

				for ( int i = 0, f = 1; i != 5; ++i, f <<= 2 )
				{
					if ( has_flags( e.mouse.Buttons, (RawMouseButtonFlags)f ) )
					{
						_buttons_status_down[i] = 1;
						_buttons_front_down [i] = 1;
					}
					if ( has_flags( e.mouse.Buttons, (RawMouseButtonFlags)(f<<1) ) )
					{
						_buttons_status_down[i] = 0;
						_buttons_front_up   [i] = 1;
					}
				}

				if ( has_flags( e.mouse.Buttons, RawMouseButtonFlags.MouseHorizontalWheel ) )
				{
					int v = unchecked((int)e.mouse.ButtonData);
					wheel_h            += v;
					last_delta_wheel_h += v;

					int i_dir          = (int)( v > 0 ? Mouse_Button.SCROLL_WHEEL_X_UP : Mouse_Button.SCROLL_WHEEL_X_DOWN );
					_buttons_front_down [i_dir] = 1;
					_buttons_front_up   [i_dir] = 0;
					_buttons_status_down[i_dir] = 1;
				}
				if ( has_flags( e.mouse.Buttons, RawMouseButtonFlags.MouseWheel ) )
				{
					int v = unchecked((int)e.mouse.ButtonData);
					wheel_v            += v;
					last_delta_wheel_v += v;

					int i_dir          = (int)( v > 0 ? Mouse_Button.SCROLL_WHEEL_Y_UP : Mouse_Button.SCROLL_WHEEL_Y_DOWN );
					_buttons_front_down [i_dir] = 1;
					_buttons_front_up   [i_dir] = 0;
					_buttons_status_down[i_dir] = 1;
					//int i_dir_opposite = (int)( v > 0 ? Mouse_Button.SCROLL_WHEEL_Y_DOWN : Mouse_Button.SCROLL_WHEEL_Y_UP );
					//_buttons_status_down[i_dir_opposite] = 0;
				}

				pos_x            += e.mouse.LastX;
				pos_y            -= e.mouse.LastY;
				last_delta_pos_x += e.mouse.LastX;
				last_delta_pos_y -= e.mouse.LastY;
			}
			public void merge_status( Mouse_Status m )
			{
				buttons_status_down.copy_from( ref m._buttons_status_down );
				_buttons_front_up   |= m.buttons_front_up;
				_buttons_front_down |= m.buttons_front_down;

				wheel_h = m.wheel_h;
				wheel_v = m.wheel_v;
				last_delta_wheel_h += m.last_delta_wheel_h;
				last_delta_wheel_v += m.last_delta_wheel_v;

				pos_x            = m.pos_x;
				pos_y            = m.pos_y;
				last_delta_pos_x += m.last_delta_pos_x;
				last_delta_pos_y += m.last_delta_pos_y;

				max_step_events_counter = Math.Max( max_step_events_counter, m.max_step_events_counter );
				step_events_counter += m.step_events_counter;
			}

			public void start_update()
			{
				prev_step_events_counter = step_events_counter;
				max_step_events_counter = Math.Max( prev_step_events_counter, max_step_events_counter );
				step_events_counter = 0;

				buttons_front_up  .clear();
				buttons_front_down.clear();
				_buttons_front_up[(int)Mouse_Button.SCROLL_WHEEL_Y_DOWN] = 1;
				_buttons_front_up[(int)Mouse_Button.SCROLL_WHEEL_X_DOWN] = 1;
				_buttons_front_up[(int)Mouse_Button.SCROLL_WHEEL_Y_UP  ] = 1;
				_buttons_front_up[(int)Mouse_Button.SCROLL_WHEEL_X_UP  ] = 1;
				_buttons_status_down[(int)Mouse_Button.SCROLL_WHEEL_Y_DOWN] = 0;
				_buttons_status_down[(int)Mouse_Button.SCROLL_WHEEL_X_DOWN] = 0;
				_buttons_status_down[(int)Mouse_Button.SCROLL_WHEEL_Y_UP  ] = 0;
				_buttons_status_down[(int)Mouse_Button.SCROLL_WHEEL_X_UP  ] = 0;
				buttons_prev_status_down.copy_from( ref _buttons_status_down );

				prev_wheel_h = wheel_h;
				prev_wheel_v = wheel_v;

				prev_last_delta_wheel_h = last_delta_wheel_h;
				prev_last_delta_wheel_v = last_delta_wheel_v;
				last_delta_wheel_h = 0;
				last_delta_wheel_v = 0;

				prev_pos_x = pos_x;
				prev_pos_y = pos_y;

				prev_delta_pos_x = last_delta_pos_x;
				prev_delta_pos_y = last_delta_pos_y;
				last_delta_pos_x = 0;
				last_delta_pos_y = 0;
			}

			public static
			Mouse_Button
			mouse_button_from_index( int ki )
			{
				Mouse_Button ret = (Mouse_Button)ki;
				return ret;
			}
		}
		#endif
		public class
		Gamepad_Status
		{
			public const int BUTTONS_MAX_COUNT = (int)Gamepad_Button.__COUNT;

			public int        gamepad_device_id;

			public Bit_Vector prev_buttons_active      = new Bit_Vector( BUTTONS_MAX_COUNT );
			public Bit_Vector buttons_active           = new Bit_Vector( BUTTONS_MAX_COUNT );
			public Bit_Vector buttons_just_activated   = new Bit_Vector( BUTTONS_MAX_COUNT );
			public Bit_Vector buttons_just_deactivated = new Bit_Vector( BUTTONS_MAX_COUNT );
			public Vector2[]  sticks_value          = new Vector2[2];
			public float[]    triggers_value        = new float[2];
			public Vector2[]  prev_sticks_value     = new Vector2[2];
			public float[]    prev_triggers_value   = new float[2];
			public Vector2[]  delta_sticks_value    = new Vector2[2];
			public float[]    delta_triggers_value  = new float[2];

			public bool connected;
			public double last_fixu_seen_ms;

			public
			void
			fixed_update()
			{
				Gamepad g;
				if ( Gamepad.all.Count <= gamepad_device_id )
				{
					g = new Gamepad();
				} else
				{
					g = gamepad_device_id < 0 ? Gamepad.current : Gamepad.all[gamepad_device_id];
				}

				prev_buttons_active.copy_from( ref buttons_active );
				for ( int i = 0; i != sticks_value.Length; ++i )
				{
					prev_sticks_value[i] = sticks_value[i];
				}
				for ( int i = 0; i != triggers_value.Length; ++i )
				{
					prev_triggers_value[i] = triggers_value[i];
				}

				buttons_active          [(int)Gamepad_Button.A] = g.aButton.isPressed ? 1 : 0;
				//buttons_just_activated  [(int)Gamepad_Button.A] = g.aButton.wasPressedThisFrame ? 1 : 0;
				//buttons_just_deactivated[(int)Gamepad_Button.A] = g.aButton.wasReleasedThisFrame ? 1 : 0;

				buttons_active          [(int)Gamepad_Button.B] = g.bButton.isPressed ? 1 : 0;
				//buttons_just_activated  [(int)Gamepad_Button.B] = g.bButton.wasPressedThisFrame ? 1 : 0;
				//buttons_just_deactivated[(int)Gamepad_Button.B] = g.bButton.wasReleasedThisFrame ? 1 : 0;

				buttons_active          [(int)Gamepad_Button.X] = g.xButton.isPressed ? 1 : 0;
				//buttons_just_activated  [(int)Gamepad_Button.X] = g.xButton.wasPressedThisFrame ? 1 : 0;
				//buttons_just_deactivated[(int)Gamepad_Button.X] = g.xButton.wasReleasedThisFrame ? 1 : 0;

				buttons_active          [(int)Gamepad_Button.Y] = g.yButton.isPressed ? 1 : 0;
				//buttons_just_activated  [(int)Gamepad_Button.Y] = g.yButton.wasPressedThisFrame ? 1 : 0;
				//buttons_just_deactivated[(int)Gamepad_Button.Y] = g.yButton.wasReleasedThisFrame ? 1 : 0;

				buttons_active          [(int)Gamepad_Button.DPAD_UP] = g.dpad.up.isPressed ? 1 : 0;
				//buttons_just_activated  [(int)Gamepad_Button.DPAD_UP] = g.dpad.up.wasPressedThisFrame ? 1 : 0;
				//buttons_just_deactivated[(int)Gamepad_Button.DPAD_UP] = g.dpad.up.wasReleasedThisFrame ? 1 : 0;

				buttons_active          [(int)Gamepad_Button.DPAD_DOWN] = g.dpad.down.isPressed ? 1 : 0;
				//buttons_just_activated  [(int)Gamepad_Button.DPAD_DOWN] = g.dpad.down.wasPressedThisFrame ? 1 : 0;
				//buttons_just_deactivated[(int)Gamepad_Button.DPAD_DOWN] = g.dpad.down.wasReleasedThisFrame ? 1 : 0;

				buttons_active          [(int)Gamepad_Button.DPAD_LEFT] = g.dpad.left.isPressed ? 1 : 0;
				//buttons_just_activated  [(int)Gamepad_Button.DPAD_LEFT] = g.dpad.left.wasPressedThisFrame ? 1 : 0;
				//buttons_just_deactivated[(int)Gamepad_Button.DPAD_LEFT] = g.dpad.left.wasReleasedThisFrame ? 1 : 0;

				buttons_active          [(int)Gamepad_Button.DPAD_RIGHT] = g.dpad.right.isPressed ? 1 : 0;
				//buttons_just_activated  [(int)Gamepad_Button.DPAD_RIGHT] = g.dpad.right.wasPressedThisFrame ? 1 : 0;
				//buttons_just_deactivated[(int)Gamepad_Button.DPAD_RIGHT] = g.dpad.right.wasReleasedThisFrame ? 1 : 0;

				buttons_active          [(int)Gamepad_Button.L1] = g.leftShoulder.isPressed ? 1 : 0;
				//buttons_just_activated  [(int)Gamepad_Button.L1] = g.leftShoulder.wasPressedThisFrame ? 1 : 0;
				//buttons_just_deactivated[(int)Gamepad_Button.L1] = g.leftShoulder.wasReleasedThisFrame ? 1 : 0;

				buttons_active          [(int)Gamepad_Button.R1] = g.rightShoulder.isPressed ? 1 : 0;
				//buttons_just_activated  [(int)Gamepad_Button.R1] = g.rightShoulder.wasPressedThisFrame ? 1 : 0;
				//buttons_just_deactivated[(int)Gamepad_Button.R1] = g.rightShoulder.wasReleasedThisFrame ? 1 : 0;

				buttons_active          [(int)Gamepad_Button.L2] = g.leftTrigger.isPressed ? 1 : 0;
				//buttons_just_activated  [(int)Gamepad_Button.L2] = g.leftTrigger.wasPressedThisFrame ? 1 : 0;
				//buttons_just_deactivated[(int)Gamepad_Button.L2] = g.leftTrigger.wasReleasedThisFrame ? 1 : 0;

				buttons_active          [(int)Gamepad_Button.R2] = g.rightTrigger.isPressed ? 1 : 0;
				//buttons_just_activated  [(int)Gamepad_Button.R2] = g.rightTrigger.wasPressedThisFrame ? 1 : 0;
				//buttons_just_deactivated[(int)Gamepad_Button.R2] = g.rightTrigger.wasReleasedThisFrame ? 1 : 0;

				gamepad_digitize_sticks( g, out int[] sticks_buttons_active );
				for ( int lr = 0; lr != 2; ++lr )
				{
					int gb = sticks_buttons_active[lr];
					log( gb.ToString() );

					if ( gb >= 0 )
					{
						//if ( this.buttons_active[gb] == 0 )
						//{
						//	buttons_just_activated[gb] = 1;
						//}
						this.buttons_active[gb] = 1;
						//buttons_just_deactivated[gb] = 0;
					} else
					{
						//if ( this.buttons_active[gb] == 1 )
						//{
						//	buttons_just_deactivated[gb] = 1;
						//}
						this.buttons_active[gb] = 0;
						//buttons_just_activated[gb] = 0;
					}
				}

				triggers_value[0] = g.leftTrigger .ReadValue();
				triggers_value[1] = g.rightTrigger.ReadValue();
				sticks_value[0] = g.leftStick .ReadValue();
				sticks_value[1] = g.rightStick.ReadValue();

				for ( int i = 0; i != sticks_value.Length; ++i )
				{
					delta_sticks_value[i] = sticks_value[i] - prev_sticks_value[i];
				}
				for ( int i = 0; i != triggers_value.Length; ++i )
				{
					delta_triggers_value[i] = triggers_value[i] - prev_triggers_value[i];
				}

				//for ( int i = 0; i != buttons_active.bits_count; ++i )
				//{
				//	calc_fronts( i );
				//}
				buttons_just_activated = ~prev_buttons_active & buttons_active;
				buttons_just_deactivated = prev_buttons_active & ~buttons_active;


				bool now_connected =  last_fixu_seen_ms == FrameTimeManager.curr_fixu_end_time_ms;
				just_connected = ! connected && now_connected;
				just_disconnected = connected && ! now_connected;
				connected = now_connected;
			}

			#region private
			private Vector2 last_value;
			internal bool just_disconnected;
			internal bool just_connected;
			internal double connected_fixu_seen_ms;

			//private void calc_fronts( int b )
			//{
			//	buttons_just_activated  [(int)b] = prev_buttons_active[(int)b] == 0 && buttons_active[(int)b] == 1 ? 1 : 0;
			//	buttons_just_deactivated[(int)b] = prev_buttons_active[(int)b] == 1 && buttons_active[(int)b] == 0 ? 1 : 0;
			//}
			#endregion
		}

		[Serializable]
		public class
		Mapping
		{
			[Serializable]
			public class
			Action
			{
				public Input_Capture_Flags flags;
				public int                 index;
			}
			[Serializable]
			public class
			Mapped_Action
			{
				public Action       action;
				public Input_Source source;
			}

			// NOTE(theGiallo): dear Unity management, where the fuck do you spend your money?! We still have to do this?!
			// And cannot write a custom serializer?! In 2020?!
			[Serializable]
			public class Serialized_Chunked_Array_Input_Source : Chunked_Array<Input_Source> { }

			[NonSerialized]  public Chunked_Array<Input_Source.Status>    statuses = new Chunked_Array<Input_Source.Status>();
			[NonSerialized]  public Chunked_Array<Action>                 actions  = new Chunked_Array<Action>();
			[SerializeField] public Serialized_Chunked_Array_Input_Source sources  = new Serialized_Chunked_Array_Input_Source();
			[NonSerialized]  public Bit_Vector actions_active          = new Bit_Vector();
			[NonSerialized]  public Bit_Vector actions_just_activated  = new Bit_Vector();
			[NonSerialized]  public Bit_Vector actions_just_deactivated = new Bit_Vector();

			public
			void
			copy_from( Mapping m )
			{
				actions .copy_from( m.actions );
				sources .copy_from( m.sources );
				statuses.copy_from( m.statuses );

				if ( actions_active.bits_count != m.actions_active.bits_count )
				{
					actions_active.copy_from( ref m.actions_active );
				}
				if ( actions_just_activated.bits_count != m.actions_just_activated.bits_count )
				{
					actions_just_activated.copy_from( ref m.actions_just_activated );
				}
				if ( actions_just_deactivated.bits_count != m.actions_just_deactivated.bits_count )
				{
					actions_just_deactivated.copy_from( ref m.actions_just_deactivated );
				}
			}

			public Mapping( Chunked_Array<Action> actions, Chunked_Array<Input_Source> sources )
			{
				this.actions.add( actions );
				this.sources.add( sources );
				this.statuses.add( new Input_Source.Status() );
				init_actions_statuses();
			}
			public Mapping( List<Action> actions, List<Input_Source> sources )
			{
				this.actions.add( actions );
				this.sources.add( sources );
				this.statuses.add( new Input_Source.Status() );
				init_actions_statuses();
			}
			public Mapping( params Mapped_Action[] mapped_actions )
			{
				for ( int i = 0; i != mapped_actions.Length; ++i )
				{
					actions.add( mapped_actions[i].action );
					sources.add( mapped_actions[i].source );
					statuses.add( new Input_Source.Status() );
				}
				init_actions_statuses();
			}
			public Mapping( params Action[] actions )
			{
				debug_assert( actions != null );
				this.actions = new Chunked_Array<Action>();
				for ( int i = 0; i != actions.Length; ++i )
				{
					this.actions.add( new Action() );
					this.sources.add( new Input_Source() );
					this.statuses.add( new Input_Source.Status() );
				}
				for ( int i = 0; i != actions.Length; ++i )
				{
					this.actions[actions[i].index] = actions[i];
				}
				init_actions_statuses();
			}
			public
			bool
			on_after_deserialized( params Action[] actions )
			{
				bool ret = false;
				if ( sources == null || actions.Length != sources.occupancy )
				{
					return ret;
				}
				this.actions = new Chunked_Array<Action>();
				this.actions.add( actions );
				for ( int i = 0; i != actions.Length; ++i )
				{
					this.actions[actions[i].index] = actions[i];
				}
				this.statuses = new Chunked_Array<Input_Source.Status>();
				for ( int i = 0; i != this.actions.occupancy; ++i )
				{
					this.statuses.add( new Input_Source.Status() );
				}

				init_actions_statuses();

				ret = true;
				return ret;
			}

			public
			void
			remove_collisions( int action, Input_Source source, List<int> colliding_actions )
			{
				for ( int i = 0; i != colliding_actions.Count; ++i )
				{
					int a = colliding_actions[i];
					ref Input_Source s = ref sources[(int)a];
					if ( s.type == Input_Source.Type.OPTIONS )
					{
						Bit_Vector removed = new Bit_Vector( s.options.Length );
						int removed_count = 0;
						for ( int o = 0; 0 != removed.bits_count; ++o )
						{
							if ( s.options[o].collides( source ) )
							{
								removed[o] = 1;
								++removed_count;
							}
						}
						if ( removed_count == removed.bits_count )
						{
							s = new Input_Source();
						} else
						{
							Input_Source[] new_options = new Input_Source[removed.bits_count - removed_count];
							for ( int o = 0, n = 0; o != removed.bits_count; ++o )
							{
								if ( removed[o] == 0 )
								{
									new_options[n++] = s.options[o];
								}
							}
							s._options = new_options;
						}
					} else
					{
						s = new Game_Input.Input_Source();
					}
				}
			}

			public
			bool
			already_mapped( int action, Input_Source source, out List<int> colliding_actions )
			{
				bool ret = false;

				colliding_actions = new List<int>();

				for ( int a = 0; a != sources.occupancy; ++a )
				{
					ref var s = ref sources[a];
					if ( s.collides( source ) )
					{
						if ( ! colliding_actions.Contains( a ) )
						{
							colliding_actions.Add( a );
						}
						ret = true;
					}
				}

				return ret;
			}

			public
			void
			fixed_update()
			{
				for ( int i = 0; i != actions.occupancy; ++i )
				{
					ref Input_Source s = ref sources[i];
					statuses[i] = s.get_status();
					actions_active          [i] = statuses[i].active           ? 1 : 0;
					actions_just_activated  [i] = statuses[i].just_activated   ? 1 : 0;
					actions_just_deactivated[i] = statuses[i].just_deactivated ? 1 : 0;
				}
			}

			#region private
			private
			void
			init_actions_statuses()
			{
				actions_active           = new Bit_Vector( actions.occupancy );
				actions_just_activated   = new Bit_Vector( actions.occupancy );
				actions_just_deactivated = new Bit_Vector( actions.occupancy );
			}
			#endregion
		}

		[Flags]
		public enum
		Input_Capture_Flags
		{
			ALLOW_COMBINED     = 1 << 0,
			NO_MOUSES          = 1 << 1,
			NO_KEYBOARDS       = 1 << 2,
			NO_GAMEPADS        = 1 << 3,
			DIGITIZED_STICKS   = 1 << 4,
			DIGITIZED_TRIGGERS = 1 << 5,
			DIGITIZED_WHEELS   = 1 << 6,
			INPUT_UNITY        = 1 << 7,
			INPUT_RAW          = 1 << 8,
			DEVICE_PATHS       = 1 << 9,
			ABORT_ON_ESCAPE    = 1 << 10,
			FULL               =   ALLOW_COMBINED
			                     | DIGITIZED_STICKS
			                     | DIGITIZED_TRIGGERS
			                     | DIGITIZED_WHEELS
			                     | INPUT_UNITY
			                     | INPUT_RAW
			                     | DEVICE_PATHS
			                     | ABORT_ON_ESCAPE
		}
		public class
		Capture_Advanced_Options
		{
			public Device_Path[] ignored_mouses;
			public Device_Path[] ignored_keyboards;
			public Device_Path[] just_these_mouses;
			public Device_Path[] just_these_keyboards;

			public
			bool
			can_record( Input_Source.Type type, Device_Path path )
			{
				bool ret = true;
				if ( type == Input_Source.Type.MOUSE_BUTTON )
				{
					ret = ( ignored_mouses == null || ! Array.Exists( ignored_mouses, p=>p==path ) )
					  && ( just_these_mouses == null || Array.Exists( just_these_mouses, p=>p==path ) );
				} else
				if ( type == Input_Source.Type.KEY )
				{
					ret = ( ignored_keyboards == null || ! Array.Exists( ignored_keyboards, p=>p==path ) )
					   && ( just_these_keyboards == null || Array.Exists( just_these_keyboards, p=>p==path ) );
				}
				return ret;
			}
		}

		[Button]
		public
		void
		capture_input_source_test()
		{
			Input_Source test = default;
			log( test.ToString() );
			StartCoroutine( capture_input_source_co( Input_Capture_Flags.FULL, r => log( r.ToString() ) ) );
		}
		public
		void
		capture_input_source( Input_Capture_Flags flags, Action<Opt<Input_Source>> result )
		{
			StartCoroutine( capture_input_source_co( flags, result ) );
		}
		public
		IEnumerator
		capture_input_source_co( Input_Capture_Flags flags, Action<Opt<Input_Source>> result, Capture_Advanced_Options adv_opts = null )
		{
			yield return null;

			HashSet<Input_Source> history = new HashSet<Input_Source>();
			Opt<Input_Source> ret = Opt<Input_Source>.NO_VALUE;

			bool allow_combined = has_flags( flags, Input_Capture_Flags.ALLOW_COMBINED );
			bool abort_on_escape = has_flags( flags, Input_Capture_Flags.ABORT_ON_ESCAPE );

			bool started = false;
			for (;;)
			{
				HashSet<Input_Source> all = get_all_pressed_inputs( flags, adv_opts );
				if ( all.Count > 0 )
				{
					if ( abort_on_escape )
					{
						bool escape_detected = false;
						foreach ( Input_Source e in all )
						{
							if ( e.type == Input_Source.Type.KEY
							  && ( ( e.input_system == Input_Source.Input_System.UNITY && e.key_code == Key.Escape )
							#if TG_RAW_INPUT
							    || ( e.input_system == Input_Source.Input_System.RAW   && e.scancode_index == Index.escape ) ) )
							#else
							    ) )
							#endif
							{
								escape_detected = true;
								break;
							}
						}
						if ( escape_detected )
						{
							//log( "escape detected" );
							history.Clear();
							break;
						}
					}
					history.Add( all );
					started = true;
					if ( ! allow_combined )
					{
						if ( history.Count > 1 )
						{
							history.Clear();
							started = false;
						} else
						{
							break;
						}
					}
				} else
				if ( started )
				{
					break;
				}
				yield return new WaitForEndOfFrame();
			}

			if ( history.Count > 1 && allow_combined )
			{
				ret = Input_Source.Combined( history.ToArray() );
			} else
			if ( history.Count > 0 )
			{
				ret = history.First();
			}

			result( ret );
		}
		public
		HashSet<Input_Source>
		get_all_pressed_inputs( Input_Capture_Flags flags, Capture_Advanced_Options adv_opts = null )
		{
			HashSet<Input_Source> ret = new HashSet<Input_Source>();

			if ( adv_opts == null )
			{
				adv_opts = new Capture_Advanced_Options();
			}

			if ( has_flags( flags, Input_Capture_Flags.INPUT_UNITY ) )
			{
				ReadOnlyArray<InputDevice> devices = InputSystem.devices;
				for ( int id = 0; id != devices.Count; ++id )
				{
					InputDevice d = devices[id];
					if ( d is Keyboard && ! has_flags( flags, Input_Capture_Flags.NO_KEYBOARDS ) )
					{
						Keyboard s = d as Keyboard;
						ReadOnlyArray<KeyControl> ak = s.allKeys;
						for ( int ik = 0; ik != ak.Count; ++ik )
						{
							if ( ak[ik].isPressed )
							{
								Input_Source ins = Input_Source.Key( ak[ik].keyCode );
								log( $"{ak[ik].keyCode} => {ins}" );
								ret.Add( ins );
							}
						}
					} else
					if ( d is Mouse && ! has_flags(	 flags, Input_Capture_Flags.NO_MOUSES ) )
					{
						Mouse m = d as Mouse;

						if ( m.leftButton.isPressed )
						{
							Input_Source ins = Input_Source.Mouse_Button( Mouse_Button.LEFT );
							ret.Add( ins );
						}
						if ( m.rightButton.isPressed )
						{
							Input_Source ins = Input_Source.Mouse_Button( Mouse_Button.RIGHT );
							ret.Add( ins );
						}
						if ( m.middleButton.isPressed )
						{
							Input_Source ins = Input_Source.Mouse_Button( Mouse_Button.MIDDLE );
							ret.Add( ins );
						}
						if ( m.backButton.isPressed )
						{
							Input_Source ins = Input_Source.Mouse_Button( Mouse_Button.BACK );
							ret.Add( ins );
						}
						if ( m.forwardButton.isPressed )
						{
							Input_Source ins = Input_Source.Mouse_Button( Mouse_Button.FORWARD );
							ret.Add( ins );
						}
						if ( has_flags( flags, Input_Capture_Flags.DIGITIZED_WHEELS ) )
						{
							Vector2 scroll = m.scroll.ReadValue();
							if ( scroll.x > 0 )
							{
								Input_Source ins = Input_Source.Mouse_Button( Mouse_Button.SCROLL_WHEEL_X_UP );
								ret.Add( ins );
							}
							if ( scroll.x < 0 )
							{
								Input_Source ins = Input_Source.Mouse_Button( Mouse_Button.SCROLL_WHEEL_X_DOWN );
								ret.Add( ins );
							}
							if ( scroll.y > 0 )
							{
								Input_Source ins = Input_Source.Mouse_Button( Mouse_Button.SCROLL_WHEEL_Y_UP );
								ret.Add( ins );
							}
							if ( scroll.y < 0 )
							{
								Input_Source ins = Input_Source.Mouse_Button( Mouse_Button.SCROLL_WHEEL_Y_DOWN );
								ret.Add( ins );
							}
						}

						#if false
						ReadOnlyArray<InputControl> inputs = m.allControls;
						for ( int ii = 0; ii != inputs.Count; ++ii )
						{
							if ( inputs[ii] is ButtonControl )
							{
								ButtonControl b = inputs[ii] as ButtonControl;
								if ( b.isPressed )
								{
									log( $"{b.name} {b.displayName} {b.shortDisplayName}" );
									int index = -1;
									// TODO(theGiallo): how do we get the index?
								}
							}
						}
						#endif
					} else
					if ( d is Pointer )
					{
						// TODO(theGiallo): IMPLEMENT
						Pointer s = d as Pointer;
					} else
					if ( d is Gamepad && ! has_flags( flags, Input_Capture_Flags.NO_GAMEPADS ) )
					{
						Gamepad g = d as Gamepad;

						int gamepad_dev_id = g.deviceId;

						if ( g.aButton.isPressed )
						{
							Input_Source ins = Input_Source.Gamepad_Button( Gamepad_Button.A, gamepad_dev_id );
							ret.Add( ins );
						}
						if ( g.bButton.isPressed )
						{
							Input_Source ins = Input_Source.Gamepad_Button( Gamepad_Button.B, gamepad_dev_id );
							ret.Add( ins );
						}
						if ( g.xButton.isPressed )
						{
							Input_Source ins = Input_Source.Gamepad_Button( Gamepad_Button.X, gamepad_dev_id );
							ret.Add( ins );
						}
						if ( g.yButton.isPressed )
						{
							Input_Source ins = Input_Source.Gamepad_Button( Gamepad_Button.Y, gamepad_dev_id );
							ret.Add( ins );
						}
						if ( g.dpad.up.isPressed )
						{
							Input_Source ins = Input_Source.Gamepad_Button( Gamepad_Button.DPAD_UP, gamepad_dev_id );
							ret.Add( ins );
						}
						if ( g.dpad.down.isPressed )
						{
							Input_Source ins = Input_Source.Gamepad_Button( Gamepad_Button.DPAD_DOWN, gamepad_dev_id );
							ret.Add( ins );
						}
						if ( g.dpad.left.isPressed )
						{
							Input_Source ins = Input_Source.Gamepad_Button( Gamepad_Button.DPAD_LEFT, gamepad_dev_id );
							ret.Add( ins );
						}
						if ( g.dpad.right.isPressed )
						{
							Input_Source ins = Input_Source.Gamepad_Button( Gamepad_Button.DPAD_RIGHT, gamepad_dev_id );
							ret.Add( ins );
						}
						if ( g.leftShoulder.isPressed )
						{
							Input_Source ins = Input_Source.Gamepad_Button( Gamepad_Button.L1, gamepad_dev_id );
							ret.Add( ins );
						}
						if ( g.rightShoulder.isPressed )
						{
							Input_Source ins = Input_Source.Gamepad_Button( Gamepad_Button.R1, gamepad_dev_id );
							ret.Add( ins );
						}
						if ( has_flags( flags, Input_Capture_Flags.DIGITIZED_TRIGGERS ) )
						{
							if ( g.leftTrigger.isPressed )
							{
								Input_Source ins = Input_Source.Gamepad_Button( Gamepad_Button.L2, gamepad_dev_id );
								ret.Add( ins );
							}
							if ( g.rightTrigger.isPressed )
							{
								Input_Source ins = Input_Source.Gamepad_Button( Gamepad_Button.R2, gamepad_dev_id );
								ret.Add( ins );
							}
						}
						if ( g.leftStickButton.isPressed )
						{
							Input_Source ins = Input_Source.Gamepad_Button( Gamepad_Button.L3, gamepad_dev_id );
							ret.Add( ins );
						}
						if ( g.rightStickButton.isPressed )
						{
							Input_Source ins = Input_Source.Gamepad_Button( Gamepad_Button.R3, gamepad_dev_id );
							ret.Add( ins );
						}
						if ( g.selectButton.isPressed )
						{
							Input_Source ins = Input_Source.Gamepad_Button( Gamepad_Button.SELECT, gamepad_dev_id );
							ret.Add( ins );
						}
						if ( has_flags( flags, Input_Capture_Flags.DIGITIZED_STICKS ) )
						{
							gamepad_digitize_sticks( g, out int[] buttons_active );

							for ( int lr = 0; lr != 2; ++lr )
							{
								int gb = buttons_active[lr];
								log( gb.ToString() );

								if ( gb >= 0 )
								{
									Input_Source ins = Input_Source.Gamepad_Button( (Gamepad_Button)gb, gamepad_dev_id );
									ret.Add( ins );
								}
							}
						}
					}
				}
			}

			#if TG_RAW_INPUT
			if ( has_flags( flags, Input_Capture_Flags.INPUT_RAW ) )
			{
				bool device_paths = has_flags( flags, Input_Capture_Flags.DEVICE_PATHS );
				if ( ! has_flags( flags, Input_Capture_Flags.NO_KEYBOARDS ) )
				{
					foreach ( KeyValuePair<Device_Path,Keyboard_Status> kv in keyboards_per_frame )
					{
						if ( ! adv_opts.can_record( Input_Source.Type.KEY, kv.Key ) )
						{
							continue;
						}
						for ( int ki = 0; ki != kv.Value.keys_status_down.bits_count; ++ki )
						{
							if ( kv.Value.keys_status_down[ki] == 1 )
							{
								Input_Source ins = Input_Source.Key_Raw( (Index)ki, device_paths ? kv.Key : new Device_Path() );
								ret.Add( ins );
							}
						}
					}
				}
				if ( ! has_flags( flags, Input_Capture_Flags.NO_MOUSES ) )
				{
					foreach ( KeyValuePair<Device_Path,Mouse_Status> kv in mouses_per_frame )
					{
						if ( ! adv_opts.can_record( Input_Source.Type.MOUSE_BUTTON, kv.Key ) )
						{
							continue;
						}
						for ( int ki = 0; ki != kv.Value.buttons_status_down.bits_count; ++ki )
						{
							if ( kv.Value.buttons_status_down[ki] == 1 )
							{
								Input_Source ins = Input_Source.Mouse_Button_Raw( Mouse_Status.mouse_button_from_index( ki ), device_paths ? kv.Key : new Device_Path() );
								ret.Add( ins );
							}
						}
						if ( has_flags( flags, Input_Capture_Flags.DIGITIZED_WHEELS ) )
						{
							if ( kv.Value.last_delta_wheel_h > 0 )
							{
								Input_Source ins = Input_Source.Mouse_Button_Raw( Mouse_Button.SCROLL_WHEEL_X_UP, device_paths ? kv.Key : new Device_Path() );
								ret.Add( ins );
							} else
							if ( kv.Value.last_delta_wheel_h < 0 )
							{
								Input_Source ins = Input_Source.Mouse_Button_Raw( Mouse_Button.SCROLL_WHEEL_X_DOWN, device_paths ? kv.Key : new Device_Path() );
								ret.Add( ins );
							}
							if ( kv.Value.last_delta_wheel_v > 0 )
							{
								Input_Source ins = Input_Source.Mouse_Button_Raw( Mouse_Button.SCROLL_WHEEL_Y_UP, device_paths ? kv.Key : new Device_Path() );
								ret.Add( ins );
							} else
							if ( kv.Value.last_delta_wheel_v < 0 )
							{
								Input_Source ins = Input_Source.Mouse_Button_Raw( Mouse_Button.SCROLL_WHEEL_Y_DOWN, device_paths ? kv.Key : new Device_Path() );
								ret.Add( ins );
							}
						}
					}
				}
			}
			#endif

			return ret;
		}

		public static void gamepad_digitize_sticks( Gamepad g, out int[] buttons_active )
		{
			buttons_active = new int[2]{ -1, -1 };

			for ( int lr = 0; lr != 2; ++lr )
			{
				Vector2 v = lr == 0 ? g.leftStick.ReadValue() : g.rightStick.ReadValue();

				//log( $"{(lr==0?"left":"right")} stick | {v}" );

				if ( lr == 0 ? v.magnitude > gamepad_threshold_out_left_stick( g.deviceId )
				             : v.magnitude > gamepad_threshold_out_right_stick( g.deviceId ) )
				{
					float deg_up = lr == 0 ? angle_deg_ccw_up_left_stick( g.deviceId ) : angle_deg_ccw_up_right_stick( g.deviceId );
					float rad_up = Mathf.Deg2Rad * deg_up;
					float a_deg = deg_up == 0 ? Vector2.SignedAngle( new Vector2( 0, 1 ), v )
					                          : Vector2.SignedAngle( new Vector2( -Mathf.Sin( rad_up ), Mathf.Cos( rad_up ) ), v );

					if ( a_deg < 0 )
					{
						a_deg += 360;
					}
					float hh = 22.5f;
					Gamepad_Button gb;
					if ( a_deg <= hh || a_deg > 15 * hh )
					{
						gb = Gamepad_Button.L_STICK_UP;
					} else
					if ( a_deg <= 3 * hh && a_deg > hh * 1 )
					{
						gb = Gamepad_Button.L_STICK_UP_LEFT;
					} else
					if ( a_deg <= 5 * hh && a_deg > hh * 3 )
					{
						gb = Gamepad_Button.L_STICK_LEFT;
					} else
					if ( a_deg <= 7 * hh && a_deg > hh * 5 )
					{
						gb = Gamepad_Button.L_STICK_DOWN_LEFT;
					} else
					if ( a_deg <= 9 * hh && a_deg > hh * 7 )
					{
						gb = Gamepad_Button.L_STICK_DOWN;
					} else
					if ( a_deg <= 11 * hh && a_deg > hh * 9 )
					{
						gb = Gamepad_Button.L_STICK_DOWN_RIGHT;
					} else
					if ( a_deg <= 13 * hh && a_deg > hh * 11 )
					{
						gb = Gamepad_Button.L_STICK_RIGHT;
					} else
					//if ( a <= 15 * hh && a > hh * 13 )
					{
						gb = Gamepad_Button.L_STICK_UP_RIGHT;
					}

					gb = (Gamepad_Button)( (int)gb + lr * 8 );

					buttons_active[lr] = (int)gb;
				}
			}
		}

		public EventHandler<Device_Path> on_new_keyboard;
		public EventHandler<Device_Path> on_new_mouse;
		public EventHandler<int> on_new_gamepad;
		public EventHandler<int> on_disconnected_gamepad;

		public static float gamepad_threshold_out_left_stick( int device_id )
		{
			// TODO(theGiallo): IMPLEMENT
			return 1;
		}
		public static float gamepad_threshold_out_right_stick( int device_id )
		{
			// TODO(theGiallo): IMPLEMENT
			return 1;
		}
		public static float angle_deg_ccw_up_left_stick( int device_id )
		{
			// TODO(theGiallo): IMPLEMENT
			return 0;
		}
		public static float angle_deg_ccw_up_right_stick( int de_vice_id )
		{
			// TODO(theGiallo): IMPLEMENT
			return 0;
		}

		#if TG_RAW_INPUT
		public Keyboard_Status merged_keyboards           { get; private set; } = new Keyboard_Status();
		public Keyboard_Status merged_keyboards_per_frame { get; private set; } = new Keyboard_Status();
		public Dictionary<Device_Path,Keyboard_Status > keyboards           { get; private set; } = new Dictionary<Device_Path, Keyboard_Status>();
		public Dictionary<Device_Path,Mouse_Status    > mouses              { get; private set; } = new Dictionary<Device_Path, Mouse_Status>();
		public Dictionary<Device_Path,Keyboard_Status > keyboards_per_frame { get; private set; } = new Dictionary<Device_Path, Keyboard_Status>();
		public Dictionary<Device_Path,Mouse_Status    > mouses_per_frame    { get; private set; } = new Dictionary<Device_Path, Mouse_Status>();
		public ConcurrentDictionary<Device_Path,Device_Infos> devices_infos { get; private set; } = new ConcurrentDictionary<Device_Path, Device_Infos>();
		#endif

		public Dictionary<int,Gamepad_Status> gamepads { get; private set; } = new Dictionary<int,Gamepad_Status>();
		// TODO(theGiallo): IMPLEMENT gamepads per frame

		#region private
		#if TG_RAW_INPUT
		private ConcurrentQueue<Raw_Input_Event> input_events = new ConcurrentQueue<Raw_Input_Event>();

		private Keyboard_Status add_keyboard( Device_Path path )
		{
			Keyboard_Status ret = new Keyboard_Status();
			Keyboard_Status per_frame = new Keyboard_Status();
			keyboards_per_frame.Add( path, per_frame );
			keyboards.Add( path, ret );
			debug_assert( keyboards.ContainsKey( path ) );
			on_new_keyboard?.Invoke( this, path );
			return ret;
		}
		private Mouse_Status add_mouse( Device_Path path )
		{
			Mouse_Status ret = new Mouse_Status();
			Mouse_Status per_frame = new Mouse_Status();
			mouses_per_frame.Add( path, per_frame );
			mouses.Add( path, ret );
			debug_assert( mouses.ContainsKey( path ) );
			on_new_mouse?.Invoke( this, path );
			return ret;
		}
		private void add_device_infos( RawInputDevice raw_dev, Device_Path device_path )
		{
			debug_assert( ! devices_infos.ContainsKey( device_path ) );
			Device_Infos di = new Device_Infos( raw_dev, device_path );
			devices_infos[device_path] = di;
		}
		#endif

		private ReadOnlyArray<T> all<T>() where T : InputDevice
		{
			List<T> l = new List<T>();
			ReadOnlyArray<InputDevice> all_devices = InputSystem.devices;
			for ( int i = 0; i != all_devices.Count; ++i )
			{
				InputDevice d = all_devices[i];
				if ( d is T )
				{
					l.Add( d as T );
				}
			}
			ReadOnlyArray<T> ret = new ReadOnlyArray<T>( l.ToArray() );
			return ret;
		}
		[Button]
		private void explore_log_mouses()
		{
			ReadOnlyArray<Mouse> ms = all<Mouse>();
			string s = $"{ms.Count} mouses\n";
			for ( int i = 0; i != ms.Count; ++i )
			{
				Mouse m = ms[i];
				s += $"\n[{i,2}] ----\n";
				s += $"    added: {m.added}\n"; // bool
				s += $"    valueType: {m.valueType}\n"; // Type
				s += $"    allControls: {m.allControls}\n"; // ReadOnlyArray<InputControl>
				s += $"    wasUpdatedThisFrame: {m.wasUpdatedThisFrame}\n"; // bool
				s += $"    lastUpdateTime: {m.lastUpdateTime}\n"; // double
				s += $"    deviceId: {m.deviceId}\n"; // int
				s += $"    description: {m.description}\n"; // string
				s += $"    displayName: {m.displayName}\n"; // string
				s += $"    updateBeforeRender: {m.updateBeforeRender}\n"; // bool
				s += $"    native: {m.native}\n"; // bool
				s += $"    path: {m.path}\n"; // string
				s += $"    remote: {m.remote}\n"; // bool
				s += $"    enabled: {m.enabled}\n"; // bool
				s += $"    valueSizeInBytes: {m.valueSizeInBytes}\n"; // int
				s += $"    description: {m.description}\n"; // InputDeviceDescription
				s += $"    canRunInBacmground: {m.canRunInBackground}\n"; // bool
				s += $"    layout: {m.layout}\n";
			}
			log( s );
		}
		[Button]
		private void explore_log_keyboards()
		{
			ReadOnlyArray<Keyboard> ks = all<Keyboard>();
			string s = $"{ks.Count} keyboards\n";
			for ( int i = 0; i != ks.Count; ++i )
			{
				Keyboard k = ks[i];
				s += $"\n[{i,2}] ----\n";
				s += $"    added: {k.added}\n"; // bool
				s += $"    valueType: {k.valueType}\n"; // Type
				s += $"    allControls: {k.allControls}\n"; // ReadOnlyArray<InputControl>
				s += $"    wasUpdatedThisFrame: {k.wasUpdatedThisFrame}\n"; // bool
				s += $"    lastUpdateTime: {k.lastUpdateTime}\n"; // double
				s += $"    deviceId: {k.deviceId}\n"; // int
				s += $"    description: {k.description}\n"; // string
				s += $"    displayName: {k.displayName}\n"; // string
				s += $"    updateBeforeRender: {k.updateBeforeRender}\n"; // bool
				s += $"    native: {k.native}\n"; // bool
				s += $"    path: {k.path}\n"; // string
				s += $"    remote: {k.remote}\n"; // bool
				s += $"    enabled: {k.enabled}\n"; // bool
				s += $"    valueSizeInBytes: {k.valueSizeInBytes}\n"; // int
				s += $"    description: {k.description}\n"; // InputDeviceDescription
				s += $"    canRunInBackground: {k.canRunInBackground}\n"; // bool
				s += $"    layout: {k.layout}\n";
				s += $"    keyboardLayout: {k.keyboardLayout}\n";
			}
			log( s );
		}
		[Button]
		private void explore_log_input_devices()
		{
			var keyboards = InputSystem.devices;
			string s = $"{keyboards.Count} keyboards";
			for ( int i = 0; i != keyboards.Count; ++i )
			{
				InputDevice k = keyboards[i];
				s += $"[{i,2}] ----\n";
				s += $"    added: {k.added}\n"; // bool
				s += $"    valueType: {k.valueType}\n"; // Type
				s += $"    allControls: {k.allControls}\n"; // ReadOnlyArray<InputControl>
				s += $"    wasUpdatedThisFrame: {k.wasUpdatedThisFrame}\n"; // bool
				s += $"    lastUpdateTime: {k.lastUpdateTime}\n"; // double
				s += $"    deviceId: {k.deviceId}\n"; // int
				s += $"    description: {k.description}\n"; // string
				s += $"    displayName: {k.displayName}\n"; // string
				s += $"    updateBeforeRender: {k.updateBeforeRender}\n"; // bool
				s += $"    native: {k.native}\n"; // bool
				s += $"    path: {k.path}\n"; // string
				s += $"    remote: {k.remote}\n"; // bool
				s += $"    enabled: {k.enabled}\n"; // bool
				s += $"    valueSizeInBytes: {k.valueSizeInBytes}\n"; // int
				s += $"    description: {k.description}\n"; // InputDeviceDescription
				s += $"    canRunInBackground: {k.canRunInBackground}\n"; // bool
				s += $"    usages: {k.usages}\n"; // bool
				s += $"    layout: {k.layout}\n"; // bool
				s += $"    keyboardLayout: {(k as Keyboard)?.keyboardLayout}\n"; // bool
				s += $"    is Keyboard:             {k is Keyboard}\n";
				s += $"    is Mouse:                {k is Mouse}\n";
				s += $"    is Pointer:              {k is Pointer}\n";
				s += $"    is Gameoad:              {k is Gamepad}\n";
				s += $"    is Joystick:             {k is Joystick}\n";
				s += $"    is Pen:                  {k is Pen}\n";
				s += $"    is Touchscreen:          {k is Touchscreen}\n";
				s += $"    is DualShock:            {k is UnityEngine.InputSystem.DualShock.DualShockGamepad}\n";
				s += $"    is DualShock3GamepadHID: {k is UnityEngine.InputSystem.DualShock.DualShock3GamepadHID}\n";
				s += $"    is DualShock4GamepadHID: {k is UnityEngine.InputSystem.DualShock.DualShock4GamepadHID}\n";
				s += $"    is Sensor:               {k is Sensor}\n";
				s += $"    is HID:                  {k is HID}\n";
			}
			log( s );

			do
			{
				Keyboard k = Keyboard.current;
				s = "current keyboard:\n";
				if ( k == null )
				{
					break;
				}
				s += $"    keyboardLayout: {k.keyboardLayout}\n";
				s += $"    layout: {k.layout}\n";
				s += $"    added: {k.added}\n"; // bool
				s += $"    valueType: {k.valueType}\n"; // Type
				s += $"    allControls: {k.allControls}\n"; // ReadOnlyArray<InputControl>
				s += $"    wasUpdatedThisFrame: {k.wasUpdatedThisFrame}\n"; // bool
				s += $"    lastUpdateTime: {k.lastUpdateTime}\n"; // double
				s += $"    deviceId: {k.deviceId}\n"; // int
				s += $"    updateBeforeRender: {k.updateBeforeRender}\n"; // bool
				s += $"    native: {k.native}\n"; // bool
				s += $"    remote: {k.remote}\n"; // bool
				s += $"    enabled: {k.enabled}\n"; // bool
				s += $"    valueSizeInBytes: {k.valueSizeInBytes}\n"; // int
				s += $"    description: {k.description}\n"; // InputDeviceDescription
				s += $"    canRunInBackground: {k.canRunInBackground}\n"; // bool
			} while ( false );
			log( s );
		}
		#endregion

		#region Unity
		private void Awake()
		{
			if ( instance != null )
			{
				Destroy( gameObject );
				return;
			}
			DontDestroyOnLoad( gameObject );
			debug_assert( instance == null, "multiple instances of GameInput" );
			instance = this;

			#if false
			Input_Source test = default;
			log( test.ToString() );

			explore_log_input_devices();
			ButtonControl b;
			if ( Gamepad.current != null )
			{
				b = Gamepad.current.aButton;
				b = Gamepad.current.bButton;
				b = Gamepad.current.xButton;
				b = Gamepad.current.yButton;
				b = Gamepad.current.buttonNorth;
				b = Gamepad.current.buttonSouth;
				b = Gamepad.current.buttonEast;
				b = Gamepad.current.buttonWest;
				b = Gamepad.current.crossButton;
				b = Gamepad.current.triangleButton;
				b = Gamepad.current.squareButton;
				b = Gamepad.current.circleButton;
				b = Gamepad.current.startButton;
				b = Gamepad.current.selectButton;
				b = Gamepad.current.leftStickButton;
				b = Gamepad.current.rightStickButton;
				b = Gamepad.current.leftShoulder;
				b = Gamepad.current.rightShoulder;
			}

			capture_input_source_test();
			#endif
		}
		private void Start()
		{
		#if TG_RAW_INPUT
			// Register the HidUsageAndPage to watch any device.
			#if USE_WND_PROC
			RawInputDevice.RegisterDevice( new RawInputDeviceRegistration( HidUsageAndPage.Keyboard, RawInputDeviceFlags.ExInputSink /*| RawInputDeviceFlags.NoLegacy*/, WindowHandleGrabber.window_handle ),
			                               new RawInputDeviceRegistration( HidUsageAndPage.Mouse,    RawInputDeviceFlags.ExInputSink /*| RawInputDeviceFlags.NoLegacy*/, WindowHandleGrabber.window_handle )
			   );
			#endif
			//RawInputDevice.RegisterDevice( HidUsageAndPage.Keyboard, RawInputDeviceFlags.ExInputSink | RawInputDeviceFlags.NoLegacy, WindowHandleGrabber.window_handle );
			//RawInputDevice.RegisterDevice( HidUsageAndPage.Mouse,    RawInputDeviceFlags.ExInputSink | RawInputDeviceFlags.NoLegacy, WindowHandleGrabber.window_handle );

			#if USE_WND_PROC
			WindowHandleGrabber.window_event_handler += window_event_handle;
			#else
			input_messages_handler_thread = new Thread( new ThreadStart( input_messages_handler_th ) );
			input_messages_handler_thread.Start();
			#endif
		#endif
		}
		public void OnDestroy()
		{
			//input_messages_handler_thread?.Abort();

			#if TG_RAW_INPUT
			input_thread_run = false;
			#endif
		}
		private bool has_focus = false;
		#if TG_RAW_INPUT
		private bool did_unregister = false;
		#endif
		private void OnApplicationFocus( bool focus )
		{
			has_focus = focus;
			if ( !focus )
			{
				//log_warn( "lost focus" );

				//#if ! USE_WND_PROC
				//did_unregister = true;
				//RawInputDevice.UnregisterDevice( HidUsageAndPage.Mouse );
				//RawInputDevice.UnregisterDevice( HidUsageAndPage.Keyboard );
				//#endif

				return;
			}
			//log_warn( "got focus" );
			#if USE_WND_PROC
			RawInputDevice.RegisterDevice( new RawInputDeviceRegistration( HidUsageAndPage.Keyboard, RawInputDeviceFlags.ExInputSink /*| RawInputDeviceFlags.NoLegacy*/, WindowHandleGrabber.window_handle ),
			                               new RawInputDeviceRegistration( HidUsageAndPage.Mouse,    RawInputDeviceFlags.ExInputSink /*| RawInputDeviceFlags.NoLegacy*/, WindowHandleGrabber.window_handle )
			   );
			#else
			//register_device_background_input( hWnd );
			#endif
		}
		#if false
		private void OnTick( params Component components[] )
		{
			debug_assert( comonents.Length == on_tick_types.Length );

			Transform   t0 = components[0] as Transform;
			RigidBody   rb = components[1] as RigidBody;
			Transform   t1 = components[2] as Transform;
			MyComponent m0 = components[3] as MyComponent;

			do_the_thing( m0, t0 );
			do_the_thing2( rb, t1 );
		}
		#endif

		#if TG_RAW_INPUT
		private bool first_fixed_update_done = false;
		#endif
		private void FixedUpdate()
		{
			#if TG_RAW_INPUT
			if ( ! first_fixed_update_done )
			{
				first_fixed_update_done = true;

				merged_keyboards_per_frame.start_update();
				foreach ( Keyboard_Status k in keyboards_per_frame.Values )
				{
					k.start_update();
				}
				foreach ( Mouse_Status m in mouses_per_frame.Values )
				{
					m.start_update();
				}
			}
			foreach ( Keyboard_Status k in keyboards.Values )
			{
				k.start_update();
			}
			foreach ( Mouse_Status m in mouses.Values )
			{
				m.start_update();
			}
			//log( $"Time.fixedUnscaledDeltaTime = {Time.fixedUnscaledDeltaTime} Time.fixedDeltaTime = {Time.fixedDeltaTime} Time.timeScale = {Time.timeScale}" );
			//Time.timeScale *= 0.99f;
			do
			{
				bool there_is_one = input_events.TryPeek( out Raw_Input_Event peek_e );
				if ( there_is_one )
				{
					//log( $" {( peek_e.timestamp - FrameTimeManager.root_timestamp ).TotalMilliseconds}ms - {FrameTimeManager.curr_fixu_end_time_ms}ms = {( peek_e.timestamp - FrameTimeManager.root_timestamp ).TotalMilliseconds - FrameTimeManager.curr_fixu_end_time_ms}ms" );
				}
				if ( there_is_one && ( peek_e.timestamp - FrameTimeManager.root_timestamp ).TotalMilliseconds <= FrameTimeManager.curr_fixu_end_time_ms )
				{
					there_is_one = input_events.TryDequeue( out Raw_Input_Event e );
					debug_assert( there_is_one );
					if ( e.type == Raw_Input_Event.Type.KEYBOARD )
					{
						debug_assert( peek_e.keyboard.Flags == e.keyboard.Flags );
						bool k_found = keyboards.TryGetValue( e.device_path, out Keyboard_Status k );
						if ( ! k_found )
						{
							k = add_keyboard( e.device_path );
						}
						k.apply_event( e );
					} else
					if ( e.type == Raw_Input_Event.Type.MOUSE )
					{
						bool m_found = mouses.TryGetValue( e.device_path, out Mouse_Status m );
						if ( ! m_found )
						{
							m = add_mouse( e.device_path );
						}
						m.apply_event( e );
					}
				} else
				{
					//log( $"stop dequeueing with {input_events.Count} events in queue" );
					//if ( there_is_one )
					//{
					//	log( $"( peek_e.timestamp - FrameTimeManager.root_timestamp ).TotalMilliseconds = {( peek_e.timestamp - FrameTimeManager.root_timestamp ).TotalMilliseconds } peek_e.timestamp = {peek_e.timestamp}" );
					//}
					break;
				}
			} while ( true );

			merged_keyboards.start_update();
			foreach ( Keyboard_Status k in keyboards.Values )
			{
				merged_keyboards.merge_status( k );
			}
			foreach ( KeyValuePair<Device_Path,Keyboard_Status> kv in keyboards_per_frame )
			{
				kv.Value.merge_status( keyboards[kv.Key] );
				merged_keyboards_per_frame.merge_status( kv.Value );
			}
			foreach ( KeyValuePair<Device_Path,Mouse_Status> kv in mouses_per_frame )
			{
				kv.Value.merge_status( mouses[kv.Key] );
			}
			#endif

			for ( int i = 0, ic = Gamepad.all.Count; i != ic; ++i )
			{
				Gamepad g = Gamepad.all[i];
				if ( ! gamepads.ContainsKey( g.deviceId ) )
				{
					Gamepad_Status gs = new Gamepad_Status(){ gamepad_device_id = g.deviceId };
					gamepads.Add( g.deviceId, gs );
					gs.connected_fixu_seen_ms = gs.last_fixu_seen_ms = FrameTimeManager.curr_fixu_end_time_ms;
					gs.just_connected = true;
					on_new_gamepad?.Invoke( this, gs.gamepad_device_id );
				}
			}

			foreach( KeyValuePair<int,Gamepad_Status> kv in gamepads )
			{
				Gamepad_Status gs = kv.Value;
				gs.fixed_update();
				if ( gs.just_disconnected )
				{
					on_disconnected_gamepad?.Invoke( this, gs.gamepad_device_id );
				}
			}
		}

		private void add_device_infos( object infos, Device_Path device_path )
		{
			throw new NotImplementedException();
		}

		private void Update()
		{
		#if TG_RAW_INPUT
			first_fixed_update_done = false;
		#endif
		}
		private void OnDisable()
		{
		#if TG_RAW_INPUT
			RawInputDevice.UnregisterDevice( HidUsageAndPage.Mouse );
			RawInputDevice.UnregisterDevice( HidUsageAndPage.Keyboard );
		#endif
		}
		#endregion
	}
}
