﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;

namespace tg
{
	public class Plot_FP_Mouses_Events_Count : MonoBehaviour
	{
		public FP_Controller fp_controller;
		public Plot_Mouse_Events_Count[] plotters = new Plot_Mouse_Events_Count[2];
		#region private
		#endregion

		#region Unity
		private void Awake()
		{
		}
		private void Start()
		{
		}
		private void Update()
		{
			for ( int i = 0; i != 2; ++i )
			{
				plotters[i].device_path = fp_controller.mapping.mouses_paths[i];
			}
		}
		#endregion
	}
}
