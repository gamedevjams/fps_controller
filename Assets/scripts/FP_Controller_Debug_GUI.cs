﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;

namespace tg
{
	public class FP_Controller_Debug_GUI : MonoBehaviour
	{
		public FP_Controller fp_controller;
		public TMP_Text mouse_delta_l;
		public TMP_Text mouse_delta_r;
		public TMP_Text walk_velocity;
		public TMP_Text head_yaw;
		public TMP_Text head_pitch;
		public TMP_Text rb_abs_vel;
		public TMP_Text abs_vel;
		public TMP_Text curr_vel_fw;
		public TMP_Text vel_diff;
		public TMP_Text contrasting_vel;
		public TMP_Text grounded;
		public TMP_Text could_step;

		#region private
		#endregion

		#region Unity
		private void Awake()
		{
		}
		private void Start()
		{
		}
		private void Update()
		{
			mouse_delta_l.text = fp_controller.mapping.mouses_deltas[(int)FP_Controller.Mouse_Hand.LEFT ].ToString();
			mouse_delta_r.text = fp_controller.mapping.mouses_deltas[(int)FP_Controller.Mouse_Hand.RIGHT].ToString();
			walk_velocity.text = fp_controller.movement_speed.ToString();
			head_yaw.text      = fp_controller.head_yaw_deg.ToString();
			head_pitch.text    = fp_controller.head_pitch_deg.ToString();
			rb_abs_vel.text    = fp_controller.body_rb.velocity.magnitude.ToString();
			abs_vel.text       = fp_controller.abs_vel.ToString();
			curr_vel_fw.text   = fp_controller.curr_vel_fw.ToString();
			vel_diff.text      = fp_controller.movement_velocity.ToString();
			contrasting_vel.text = fp_controller.contrasting_vel.ToString();
			grounded.text      = fp_controller.is_grounded.ToString();
			could_step.text    = fp_controller.step_checker.is_free.ToString();
		}
		#endregion
	}
}
