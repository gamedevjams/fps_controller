﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;
using System;

namespace tg
{
	public class Mouse_Hand_Entry_GUI : MonoBehaviour
	{
		public Mapping_GUI mapping_gui;
		public FP_Controller.Mouse_Hand mouse_hand;
		public TMP_Text title_text;
		public TMP_Text mouse_infos_text;
		public Button   change_button;
		public TMP_Text change_button_text;
		public string left_title_string = "Left Mouse";
		public string right_title_string = "Right Mouse";
		public string assign_string = "Assign";
		public string change_string = "Change";
		public Sensibility_Slider_GUI[] sensibility_slider_gui = new Sensibility_Slider_GUI[2];

		public void set_mapping_gui( Mapping_GUI mapping_gui )
		{
			this.mapping_gui = mapping_gui;
			for ( int i = 0; i != sensibility_slider_gui.Length; ++i )
			{
				sensibility_slider_gui[i].mapping_gui = mapping_gui;
			}
		}

		public
		void
		set_data( FP_Controller.Mouse_Hand mouse_hand )
		{
			for ( int i = 0; i != sensibility_slider_gui.Length; ++i )
			{
				sensibility_slider_gui[i].hand = mouse_hand;
			}
			this.mouse_hand = mouse_hand;
			this.mouse_infos_text.text = $"{mouse_hand}";

			refresh();
		}

		public
		void
		refresh()
		{
			refresh_in_update = false;
			title_text.text = mouse_hand == FP_Controller.Mouse_Hand.LEFT ? left_title_string : right_title_string;

			Game_Input.Device_Path path = mapping_gui.mapping.mouses_paths[(int)mouse_hand];
			if ( path.chars_length == 0 )
			{
				mouse_infos_text.text = "";
				change_button_text.text = assign_string;
			} else
			{
				if ( Game_Input.instance.devices_infos.TryGetValue( path, out Game_Input.Device_Infos di ) )
				{
					mouse_infos_text.text = di.ToString();
					change_button_text.text = change_string;
				} else
				{
					mouse_infos_text.text = $"{path}\n\n\nmouse not yet detected, please plug it in and move it";
					change_button_text.text = change_string;
					refresh_in_update = true;
				}
			}

			//content_size_fitter_infos.verticalFit = ContentSizeFitter.FitMode.MinSize;
			//content_size_fitter_infos.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
			//content_size_fitter_infos.verticalFit = ContentSizeFitter.FitMode.Unconstrained;

			vertical_layout?.SetLayoutVertical();
			//content_size_fitter.verticalFit = ContentSizeFitter.FitMode.MinSize;
			//content_size_fitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
			//content_size_fitter.verticalFit = ContentSizeFitter.FitMode.Unconstrained;

			horizontal_layout?.SetLayoutHorizontal();
			//content_size_fitter_parent.verticalFit = ContentSizeFitter.FitMode.MinSize;
			//content_size_fitter_parent.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
			//content_size_fitter_parent.verticalFit = ContentSizeFitter.FitMode.Unconstrained;

		}

		#region private
		private VerticalLayoutGroup vertical_layout;
		//private ContentSizeFitter   content_size_fitter_infos;
		//private ContentSizeFitter   content_size_fitter;
		//private ContentSizeFitter   content_size_fitter_parent;
		private HorizontalLayoutGroup horizontal_layout;

		private bool refresh_in_update = false;

		private
		void
		button_click_handler()
		{
			mapping_gui.request_mouse_hand_change( mouse_hand );
		}
		#endregion

		#region Unity
		private void Awake()
		{
			horizontal_layout = transform.parent.GetComponent<HorizontalLayoutGroup>();
			//content_size_fitter_parent = transform.parent.GetComponent<ContentSizeFitter>();
			vertical_layout = GetComponent<VerticalLayoutGroup>();
			//content_size_fitter = GetComponent<ContentSizeFitter>();
			//content_size_fitter_infos = mouse_infos_text.GetComponent<ContentSizeFitter>();

			for ( int i = 0; i != sensibility_slider_gui.Length; ++i )
			{
				sensibility_slider_gui[i].axis = i;
			}
		}
		private void Start()
		{
			change_button.onClick.AddListener( button_click_handler );
		}
		private void Update()
		{
			if ( refresh_in_update )
			{
				refresh();
			}
		}
		#endregion
	}
}
